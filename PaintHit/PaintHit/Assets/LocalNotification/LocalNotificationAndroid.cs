﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace LocalNotification
{
	#if UNITY_ANDROID
	public class LocalNotificationAndroid : ILocalNotificationService 
	{
		protected static LocalNotificationAndroidHelper _ACTION_COLLECTOR = new GameObject("LocalNotificationHelper").AddComponent<LocalNotificationAndroidHelper>();
		
		#region ILocalNotificationService implementation
		public void RegisterLocalPush ()
		{
		}

		public int Schedule (string id, string content, System.DateTime scheduleTime)
		{
			var timeSpan = scheduleTime - System.DateTime.Now;

            return LocalNotificationNative.SendNotification(timeSpan, "Paint Hit Man!", content,
                                                            Color.white, true, true, true, "app_icon", null, "default");
		}

		public List<string> QueryForReceivedNotification ()
		{
			var list = LocalNotificationNative.ListNotified ();
			if (list != null) {
				return list.ToList ();	
			}

			return new List<string> ();
		}

		public void ClearReceivedNotification ()
		{
			LocalNotificationNative.ClearReceivedNotified ();
		}

		public void ClearScheduledPush ( )
		{
			LocalNotificationNative.ClearNotifications ();

		}

		public void CancelScheduledPush ( List<int> scheduledIds )
		{
			foreach (var id in scheduledIds) {
				LocalNotificationNative.CancelNotification (id);
			}
		}
		#endregion
	}
	#endif

	public class LocalNotificationAndroidHelper : MonoBehaviour
	{
		[HideInInspector]
		public List<string> CollectedNotify = new List<string> ();

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public void OnAction( string id )
		{
			Debug.LogError ("On Action : " + id);
			CollectedNotify.Add (id);
		}
	}
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LocalNotification
{
	public class ScheduleNotifyData
	{
		public ScheduleNotifyData( )
		{
		}
		
		public ScheduleNotifyData(string id , long fireDate)
		{
			this.scheduleId = id;
			this.fireDate = fireDate;
		}
		
		public string scheduleId;
		public long fireDate;
	}
	
	public class LocalNotificationUserData
	{
		public List<ScheduleNotifyData> scheduledList = new List<ScheduleNotifyData>();

		public List<int> androidScheduledIds = new List<int>();
		public List<string> receivedList = new List<string>();
	}
}
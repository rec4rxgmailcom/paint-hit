﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Zenject;
using Assets.Scripts.Core;

namespace LocalNotification
{
	public interface ILocalNotificationService
	{
		void RegisterLocalPush ( );
		int Schedule ( string id, string content, DateTime scheduleTime );
		List<string> QueryForReceivedNotification ( );
		void ClearReceivedNotification ( );
		void ClearScheduledPush ( );
	}
	
	public class LocalNotificationManager : GameBaseSystem
	{
		[Inject] DataManager _dataManager;

		protected ILocalNotificationService _services;

		protected LocalNotificationUserData _userData = null;

        protected bool _hasInitialized = false;

		void Start(){
			#if UNITY_IOS
			_services = new LocalNotificationIOS ();
			#elif UNITY_ANDROID
			_services = new LocalNotificationAndroid();
			#endif

			InitData();

			// Save save = _dataManager.LoadGame();
			// if(save.LocalNotificationData.LastDateTimeSaved != 0)
			// {

			// }

			if (_services != null) {
				_services.RegisterLocalPush ();
			}
		}

		void InitData()
		{
			_userData = new LocalNotificationUserData();
			_userData.scheduledList.Add(new ScheduleNotifyData("0", 1));
			_userData.scheduledList.Add(new ScheduleNotifyData("1", 2));
			_userData.scheduledList.Add(new ScheduleNotifyData("2", 3));
		}

		void OnApplicationPause(bool pauseStatus)
		{   
			// if (pauseStatus) {
            //     // ScheduleForSuspensed ();
			// 	if (_services != null) {
			// 		if (_userData != null) {
			// 			foreach (var item in _userData.scheduledList) {
			// 				var data = GetNotificationData (item.scheduleId);
			// 				if (data != null) {
			// 					var timer = DateTime.Now.Add(new TimeSpan((int) item.fireDate, 0, 0));
			// 					_services.Schedule("0", "Fuck yeah!", timer);
			// 				}
			// 			}
			// 		}
			// 	}
			// } 
			// else {
				
			// }
		}

		void OnApplicationQuit()
		{
			if (_services != null) {
				if (_userData != null) {
					foreach (var item in _userData.scheduledList) {
						var data = GetNotificationData (item.scheduleId);
						if (data != null) {
							var timer = DateTime.Now.Add(new TimeSpan((int) item.fireDate, 0, 0));
							_services.Schedule("0", "Fuck yeah!", timer);
						}
					}
				}
			}
		}

        protected void ScheduleForSuspensed( )
		{
			if (_services != null) {
                // var suspenseNotify = _gameData.GetGlobalData<LocalNotificationConfig>().GetNotification(NotificationInvokeType.Appsuspensed);
                // foreach (var item in suspenseNotify) {
                //     ScheduleWithData(item);
				// }
			}
		}

		public LocalNotificationData GetNotificationData (string id)
		{
            if(id.Equals("0"))
			{
				var data = new LocalNotificationData();
				data.id = "0";
				data.MaximumScheduleTimeInDay = 1;
				data.MaximumScheduleTimeInDay = 0;
				data.Probability = 1;
				return data;
			}

			return null;
		}

		public string GetCalendarTimeById ( string calendarId )
		{
			// var calendar = _gameData.GetGlobalData<CalendarConfig> ();
			// foreach (var item in calendar.calendar) {
			// 	if (item.calendarId == calendarId) {
			// 		return item.dateTime;
			// 	}
			// }

			return "";
		}

		protected bool HasScheduled ( string id )
		{
			if (_userData != null) {
				foreach (var item in _userData.scheduledList) {
					if (item.scheduleId == id) {
						return true;
					}
				}
			}

			return false;
		}
	}
}
﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace YorkStreet.GameKit.LocalNotification
{
	#if UNITY_IOS
	public class LocalNotificationIOS : ILocalNotificationService 
	{
        protected static LocalNotificationAndroidHelper _ACTION_COLLECTOR = new GameObject("LocalNotificationHelper").AddComponent<LocalNotificationAndroidHelper>();

		#region ILocalNotificationService implementation

		public void RegisterLocalPush ()
		{
            UnityEngine.iOS.NotificationServices.RegisterForNotifications(
                UnityEngine.iOS.NotificationType.Alert |
                UnityEngine.iOS.NotificationType.Badge |
                UnityEngine.iOS.NotificationType.Sound);
		}

		public int Schedule (string id, string content, System.DateTime scheduleTime)
		{
            var timeSpan = scheduleTime - System.DateTime.Now;
            LocalNotificationNative.Action act = new LocalNotificationNative.Action(id, "Open", _ACTION_COLLECTOR);

            return LocalNotificationNative.SendNotification(timeSpan, "Paint Hit Man!", content,
                Color.white, true, true, true, "app_icon", null, "default", act);
		}

		public List<string> QueryForReceivedNotification ()
		{
            //List<string> receives = new List<string> ();
            //foreach (var item in UnityEngine.iOS.NotificationServices.localNotifications) {
            //             Debug.LogError("[RECEIVED NOTIFY - IOS NATIVE 1] - " + item.fireDate.ToLocalTime().ToString() + "-" + item.timeZone);
            //             Debug.LogError(System.DateTime.Now.ToLocalTime());

            //             Debug.LogError("[RECEIVED NOTIFY - IOS NATIVE 2] - " + item.fireDate.ToUniversalTime().ToString() + "-" + item.timeZone);
            //             Debug.LogError(System.DateTime.Now.ToUniversalTime());
            //	if (item.userInfo.Contains ("id")) {
            //		receives.Add((string)item.userInfo ["id"]);
            //	}
            //}

            var list = _ACTION_COLLECTOR.CollectedNotify;
            if (list != null)
            {
                return list.ToList();
            }

            return new List<string>();
		}

		public void ClearReceivedNotification ()
		{
            //UnityEngine.iOS.NotificationServices.ClearLocalNotifications ();
            LocalNotificationNative.ClearReceivedNotified();
		}

		public void ClearScheduledPush ( )
		{
            //if (UnityEngine.iOS.NotificationServices.scheduledLocalNotifications != null) {
            //	foreach (var item in UnityEngine.iOS.NotificationServices.scheduledLocalNotifications) {
            //                 Debug.LogError("[RECEIVED NOTIFY - IOS NATIVE 1] - " + item.fireDate.ToLocalTime().ToString() + "-" + item.timeZone);
            //                 Debug.LogError(System.DateTime.Now.ToLocalTime());

            //                 Debug.LogError("[RECEIVED NOTIFY - IOS NATIVE 2] - " + item.fireDate.ToUniversalTime().ToString() + "-" + item.timeZone);
            //                 Debug.LogError(System.DateTime.Now.ToUniversalTime());
            //	}
            //}

            LocalNotificationNative.ClearNotifications();
			//UnityEngine.iOS.NotificationServices.CancelAllLocalNotifications ();
		}
		#endregion
	}
	#endif
}
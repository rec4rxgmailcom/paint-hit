﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CalendarData
{
	public string calendarId;
	public string dateTime;
}

public class CalendarConfig  
{
	public List<CalendarData> calendar = new List<CalendarData>();
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

namespace LocalNotification
{
	public enum NotificationAction
	{
        None,
		SeedSpecialChest,
		AwardChestOnEntry,
		AwardItemOnEntry
	}

    public enum NotificationClockType
    {
        /// <summary>
        /// This clock type will be exactly time that define in calendar
        /// </summary>
        ExactTime,
        /// <summary>
        /// This clock type will be an increasing of current time, the increase amount is specific in calendar
        /// </summary>
        Increasing,
        /// <summary>
        /// This clock type will be the current time which will ignore the calendar timer
        /// </summary>
        CurrentTime
    }

    public enum NotificationInvokeType
    {
        Appsuspensed = 0,
        DailyReward_Receive,
        DailyReward_Forget_Receive,
    }

	public class LocalNotificationData
	{
		public string id;
		public int MinimumScheduleTimeInDay;
		public int MaximumScheduleTimeInDay;
		public float Probability;
        public bool EnableDoNotDisturbMode;

		public bool AvailableForSchedule( )
		{
			return UnityEngine.Random.value < Probability;
		}

        public DateTime GetScheduledNotificationTime(string specificTime)
        {
            var day = UnityEngine.Random.Range(MinimumScheduleTimeInDay, MaximumScheduleTimeInDay + 1);

            var scheduledDay = DateTime.Now.AddDays(day);

            var time = Convert(specificTime);
                        return new DateTime(scheduledDay.Year, scheduledDay.Month, scheduledDay.Day,
                                        time.Hour, time.Minute, time.Second, DateTimeKind.Local).ToLocalTime();
        }

        public static DateTime Convert (string datetime)
		{
			IFormatProvider culture = new System.Globalization.CultureInfo ("fr-FR", true); 
			if (!string.IsNullOrEmpty (datetime)) {
				try {
					return DateTime.Parse (datetime, culture);
				} catch (Exception e) {
					Debug.LogError (e.ToString ());
				}
			}

			Debug.LogError (string.Format ("Unable to convert '{0}' to a date.", datetime));
			return DateTime.MinValue;
		}
	}
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using UnityEngine;
using Zenject;

public class RewardManager : GameBaseSystem 
{
	[Inject] DataManager _dataManager;

	public const long SECONDS_PER_DAY = 86400;

	public DailyReward DailyRewardData;

	public Action OnUpdateGetNewReward;

	void Start()
	{
		if(_dataManager.IsSaveFileExist())
		{
			Save save = _dataManager.LoadGame();
			DailyRewardData = save.DailyRewardData;
			DateTime CurrentDate = Convert.ToDateTime(DateTime.Now.ToString("dd-MMM-yyyy"));
			long curTime = GetCurrentTimeStamp();

			// If user dosen't login more than 2 days, data will be reset
			if(curTime - DailyRewardData.NearestDateTimeSaved > SECONDS_PER_DAY * 2)
			{
				CreateNewRewardData();
			}
			else
			{
				DailyRewardData.NearestDateTimeSaved = curTime;
				long count = curTime - DailyRewardData.LastDateTimeSaved;
				if(count > (SECONDS_PER_DAY * DailyRewardData.CurrentDayCount) 
					&& count < SECONDS_PER_DAY * (DailyRewardData.CurrentDayCount + 1))
				{
					DailyRewardData.Rewards[DailyRewardData.CurrentDayCount - 1] = 1;
					DailyRewardData.CurrentDayCount++;
				}

				save.DailyRewardData = DailyRewardData;
				_dataManager.SaveGame(save);
			}

			if(DailyRewardData.CurrentDayCount == 7)
			{
				CreateNewRewardData();
			}

			return;
		}

		CreateNewRewardData();
	}

	void Update()
	{
		long curTime = GetCurrentTimeStamp();
		long count = curTime - DailyRewardData.LastDateTimeSaved;
		if(count > (SECONDS_PER_DAY * DailyRewardData.CurrentDayCount) 
			&& count < SECONDS_PER_DAY * (DailyRewardData.CurrentDayCount + 1))
		{
			DailyRewardData.Rewards[DailyRewardData.CurrentDayCount - 1] = 1;
			DailyRewardData.CurrentDayCount++;
			
			Save save = _dataManager.LoadGame();
			save.DailyRewardData = DailyRewardData;
			_dataManager.SaveGame(save);

			if(OnUpdateGetNewReward != null)
				OnUpdateGetNewReward();
		}
	}

	void CreateNewRewardData()
	{
		DailyRewardData = CreateNewDailyRewardData();
		Save save = _dataManager.LoadGame();
		save.DailyRewardData = DailyRewardData;
		_dataManager.SaveGame(save);
	}

	DailyReward CreateNewDailyRewardData()
	{
		DailyReward reward = new DailyReward();
		reward.CurrentDayCount = 1;
		reward.LastDateTimeSaved = GetCurrentTimeStamp();
		reward.NearestDateTimeSaved = reward.LastDateTimeSaved;
		reward.Rewards = new int[7];
		reward.Claimpeds = new bool[7];
		for (int i = 0; i < reward.Rewards.Length; i++)
		{
			reward.Rewards[i] = 0;
			reward.Claimpeds[i] = false;
		}

		return reward;
	}

	public long GetCurrentTimeStamp()
	{
		return GetTimeStamp(DateTime.Now);
	}

	long GetTimeStamp(DateTime dateTime)
	{
		DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, 0).ToLocalTime();
		TimeSpan span = (dateTime.ToLocalTime() - epoch);
		return (long) span.TotalSeconds; 
	}

	public DateTime UnixTimeStampToDateTime(double unixTimeStamp)
	{
		// Unix timestamp is seconds past epoch
		System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
		dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToLocalTime();
		return dtDateTime;
	}

	public void Save()
	{
		Save save = _dataManager.LoadGame();
		save.DailyRewardData = DailyRewardData;
		_dataManager.SaveGame(save);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using UnityEngine;

namespace Assets.Scripts.InGame.Systems
{
	public class NotificationController : GameBaseSystem
	{

		private IListenerNotification _listenerNotification = null;

		public void LoadListener(IListenerNotification listener)
		{
			_listenerNotification = listener;
		}

		public void UnloadListener()
		{
			_listenerNotification = null;
		}

		public void Notify(string eventId, object data = null, Component sender = null)
		{
			_listenerNotification.OnNotification(eventId, data, sender);
		}
	}

	public interface IListenerNotification
	{
		void OnNotification (string eventId, object data, Component sender);
	}
}



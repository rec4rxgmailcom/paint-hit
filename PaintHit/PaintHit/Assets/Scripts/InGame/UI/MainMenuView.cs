﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Core.UI;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InGame.UI
{
	public class MainMenuView : UIView 
	{
		public void ChangeScene()
		{
			NotifyToController(ControllerEvent.CHANGE_CONTEXT, GameHub.ContextName.MainGameContext);
		}
	}

}


﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Core.UI;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InGame.UI
{
	public class PanelTest : UIView 
	{
		[Inject] GUISystem guiSystem;
		
		public void HideThisView()
		{
			guiSystem.Hide(GUISystem.GUINames.PanelTest, true);
		}

		public void ChangeScene()
		{
			NotifyToController(ControllerEvent.CHANGE_CONTEXT, GameHub.ContextName.MainGameContext);
		}
		
	}
}



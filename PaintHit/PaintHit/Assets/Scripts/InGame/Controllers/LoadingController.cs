﻿
using Assets.Scripts.Core;
using Assets.Scripts.Core.EventsManager;
using Assets.Scripts.Core.UI;
using Assets.Scripts.InGame.UI;
using UnityEngine;
using Zenject;


namespace Assets.Scripts.InGame.Controllers
{
	public class LoadingController : Controller 
	{
		[Inject] GameHub gameHub;
		[Inject] GUISystem guiSystem;
		[Inject] EventDispatcher eventDispatcher;

		override protected void Loaded()
		{
			//Debug.Log("-- CONTROLLER LOADED --");
//			LoadingView loadingView = guiSystem.Show(GUISystem.GUINames.LoadingView) as LoadingView;
		}

		public void onViewLoadComplete()
		{
			Debug.Log("loadingView show complete");
		}

		override protected void Unloaded()
		{
			//Debug.Log("Unload");
			// Hide all views in current context.
			// except loading views.
			guiSystem.HideAll();
		}

		private void ChangeContext(object contextName)
		{
			gameHub.LoadContext((string) contextName);
		}

		override protected void OnNotification(string eventId, object data, Component sender)
		{
			if(eventId.Equals(ControllerEvent.CHANGE_CONTEXT))
			{
				ChangeContext(data);
			}
		}
	}
}



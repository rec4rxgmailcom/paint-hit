﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core;
using Assets.Scripts.Core.UI;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InGame.Controllers
{
	public class BootingController : Controller 
	{

		[Inject] GUISystem guiSystem;

		[Inject] GameHub gameHub;
		override protected void Loaded()
		{
			//guiSystem.sho
			guiSystem.Show(GUISystem.GUINames.PlashView);
			StartCoroutine(StartLoadSlpash());
		}

		override protected void Unloaded()
		{
			//Debug.Log("Unload");
			// Hide all views in current context.
			// except loading views.
			guiSystem.HideAll();
		}

		IEnumerator StartLoadSlpash()
		{
			yield return new WaitForSeconds(2);

			gameHub.LoadContext(GameHub.ContextName.MainMenuContext);
		}
	}

}


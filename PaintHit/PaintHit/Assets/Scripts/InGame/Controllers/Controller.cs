﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.InGame.Controllers
{
	public class Controller : MonoBehaviour
	{
		public void Load()
		{
			Loaded();
		}

		public void Unload()
		{
			Unloaded();
		}

		public void Destroy()
		{
			Destroyed();
		}

		virtual protected void Loaded()
		{
			
		}

		virtual protected void Unloaded()
		{
			
		}

		virtual protected void Destroyed()
		{
			
		}

		virtual protected void OnNotification(string eventId, object data, Component sender)
		{

		} 
    }

}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Core.SystemContext
{
	[CreateAssetMenu ( fileName = "ScreenData", menuName = "Contexts/Create Applicaiton Screen Data")]
	public class ContextData : SceneMetaData
	{

		[SerializeField] string _contextName;
		[SerializeField] string _description;

		public string ContextName 
		{
			get{ 
				return _contextName;
			}
		}

		public string Description
		{
			get { 
				return _description;
			}
		}
	}
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Core.SystemContext
{
    public class Context : MonoBehaviour, IInitializable
    {
		[Inject] readonly GameHub gameHub;
		[Inject] readonly BaseContextDelegate _contextDelegate;

		[Inject]
		void IInitializable.Initialize()
		{
		    gameHub.InitContext(this);
		}

		public void OnContextInitialize(Action onInitalizeContextComplete)
		{
			if(onInitalizeContextComplete != null)
			{
				onInitalizeContextComplete();
			}
		}

		public void LoadContext()
		{
			_contextDelegate.OnLoadContext (this);
		}

		public void UnloadContext()
		{
			_contextDelegate.OnUnLoadContext (this);
		}

		public void DestroyContext()
		{
			_contextDelegate.OnDestroyContext (this);	
		}
    }

}


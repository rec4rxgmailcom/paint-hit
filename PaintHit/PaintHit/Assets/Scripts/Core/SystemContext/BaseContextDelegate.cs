﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core.UI;
using Assets.Scripts.InGame.Controllers;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Core.SystemContext
{
	/// <summary>
	/// Handle callbacks from Context.
	/// </summary>
	public abstract class BaseContextDelegate : MonoBehaviour 
	{
		[SerializeField] protected Controller Controller;

		public abstract void OnLoadContext(Context context);

		public abstract void OnUnLoadContext(Context context);

		public abstract void OnDestroyContext(Context context);

		public virtual void OnInitializationContext(Context context, ILoadingUIContext loadingUI, Action onContextLoaded)
		{
			if (loadingUI != null) {
				//context.
				// TODO:
				//if(loadingUI.) 
			}

			if(onContextLoaded != null){
				onContextLoaded();
			}
		}
	}

}
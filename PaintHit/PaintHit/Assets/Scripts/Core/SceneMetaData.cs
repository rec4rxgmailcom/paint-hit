﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Core
{
    public class SceneMetaData : ScriptableObject, ISerializationCallbackReceiver 
	{

		#if UNITY_EDITOR

		[SerializeField]
		UnityEditor.SceneAsset _scene;

		public UnityEditor.SceneAsset SceneAsset
		{
			get { return _scene; }
			set { 
				_scene = value;
				_sceneName = _scene.name;
				ResolveScenePath ();
			}
		}

		#endif

		public string SceneName
		{
			get { 
				return _sceneName;
			}
		}

		public string ScenePath
		{
			get { 
				ResolveScenePath ();
				return _scenePath;
			}
		}

		public Scene Scene
		{
			get
			{
				return SceneManager.GetSceneByName (_sceneName);
			}
		}

		[SerializeField] private string _sceneName;
		private string _scenePath;


		public void ResolveScenePath()
		{
			#if UNITY_EDITOR
			_scenePath =  UnityEditor.AssetDatabase.GetAssetPath(_scene);
			#endif
		}

		public Scene GetScene(string sceneName)
		{
			return SceneManager.GetSceneByName (sceneName);
		}

		#region ISerializationCallbackReceiver implementation

		void ISerializationCallbackReceiver.OnBeforeSerialize ()
		{
			ResolveScenePath ();
		}

		void ISerializationCallbackReceiver.OnAfterDeserialize ()
		{
		}

		#endregion
	}
}



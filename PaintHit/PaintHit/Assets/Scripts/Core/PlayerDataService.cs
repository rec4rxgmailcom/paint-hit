﻿
using UnityEditor;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
#region Player Data Model

[Serializable]
public class ScoreData
{
    public int lastScore;
    public int totalScore;
    public int averageScore;
    public int bestScore;
    public int level;

    public ScoreData()
    {
        this.lastScore = 0;
        this.totalScore = 0;
        this.averageScore = 0;
        this.bestScore = 0;
        this.level = 0;
    }
}

[Serializable]
public class ChallengeData
{
    public int currentChallengeId;
    public int currentChallengeValue;
    public int currentProcessChallengeCount;
    public int currentTargetValue;
    public ChallengeData()
    {
        this.currentChallengeId = 0;
        this.currentProcessChallengeCount = 0;
        this.currentChallengeValue = 0;
        this.currentTargetValue = 0;
    }
}

[Serializable]
public class DailyRewardData
{
    public int lastDayGetReward;
    public long lastTimeGetReward;
    public DailyRewardData()
    {
        this.lastDayGetReward = 0;
        this.lastTimeGetReward = 0;
    }
}

[Serializable]
public class ColorInfo
{
    public int IdColor;
    public Color colors;
}

[Serializable]
public enum TypeReward
{
    Diamond,
    Circle,
    Color,
    SlowMotion
}

[Serializable]
public class ItemQuestInfo
{
    public int LevelQuest;
    public int Status;
    public ItemQuestInfo()
    {
        this.LevelQuest = 0;
        this.Status = 0;
    }
}

/// <summary>
/// User Player Data
/// </summary>

[Serializable]
public class PlayerDataModel
{
    public string deviceId;
    public int level;

    public ItemQuestInfo ItemQuestInfo;

    public int exp;
    public int diamond;
    public bool isPremiumGame;

    public int totalPlayGame;

    public int idCircleSelect;

    public int IdColorCurrent;

    public bool IsPlay;

    public bool IsSlowMotion;

    public DateTime TimeBeginReward;

    public int TimeActiveChallenge;

    public bool isChallengeReward;

    public ScoreData scoreData;
 
    public DailyRewardData dailyRewardData;

    public Dictionary<int, bool> circleUnlock;

    public Dictionary<int, bool> colorUnlock;

    public Dictionary<int, ChallengeData> challengeList;

    public Dictionary<int, List<ColorInfo>> colorDictionary;

    public Dictionary<int, bool> stageUnClockDictionary;

    public List<GameObject> ListQuest;

    public PlayerDataModel()
    {
        this.IsPlay = false;
        //this.deviceId = GetDeviceId();
        this.level = 1;

        this.ItemQuestInfo = new ItemQuestInfo();

        this.exp = 0;
        this.diamond = 0;
        this.totalPlayGame = 0;
        this.idCircleSelect = 0;
        this.IdColorCurrent = 0;
        this.TimeActiveChallenge = 0;

        this.TimeBeginReward = new DateTime();
        this.IsSlowMotion = false;
        this.isPremiumGame = false;
        this.isChallengeReward = false;
        this.scoreData = new ScoreData();
        this.dailyRewardData = new DailyRewardData();
        this.circleUnlock = new Dictionary<int, bool>();
        this.colorUnlock = new Dictionary<int, bool>();
        this.challengeList = new Dictionary<int, ChallengeData>();
        this.colorDictionary = new Dictionary<int, List<ColorInfo>>();
        this.stageUnClockDictionary = new Dictionary<int, bool>();
        this.ListQuest = new List<GameObject>();

        for (var i = 0; i < 10;i++)
        {
            this.circleUnlock.Add(i, true);
            this.colorUnlock.Add(i, true);

        }

        for (var j = 0; j < 300;j++)
        {
            this.stageUnClockDictionary.Add(j, false);
        }

        this.colorUnlock[0] = true;
        this.circleUnlock[0] = true;
        this.stageUnClockDictionary[0] = true;

        InitColor();

    }

    private void InitColor()
    {
        for (var i = 0; i < 10;i++)
        {
            this.colorDictionary.Add(i, GetColor(i));
        }
    }

    private List<ColorInfo> GetColor(int index)
    {
        List<ColorInfo> listColor = new List<ColorInfo>();

        switch(index){
            case 0:

                listColor.Add(SetColor(0, HexToColor("65faff")));
                listColor.Add(SetColor(1, HexToColor("5ae6ff")));
                listColor.Add(SetColor(2, HexToColor("5fc6f5")));
                listColor.Add(SetColor(3, HexToColor("77b4f2")));
                listColor.Add(SetColor(4, HexToColor("669ae8")));
                listColor.Add(SetColor(5, HexToColor("6886de")));
                listColor.Add(SetColor(6, HexToColor("7971ff")));
                listColor.Add(SetColor(7, HexToColor("885af3")));
                listColor.Add(SetColor(8, HexToColor("c52cfa")));
                listColor.Add(SetColor(9, HexToColor("f60cff")));

                break;

            case 1:

                listColor.Add(SetColor(0, HexToColor("31a0bc")));
                listColor.Add(SetColor(1, HexToColor("22afa5")));
                listColor.Add(SetColor(2, HexToColor("11c189")));
                listColor.Add(SetColor(3, HexToColor("0cd36f")));
                listColor.Add(SetColor(4, HexToColor("0ce05f")));
                listColor.Add(SetColor(5, HexToColor("1be75b")));
                listColor.Add(SetColor(6, HexToColor("4aeb61")));
                listColor.Add(SetColor(7, HexToColor("88ed6f")));
                listColor.Add(SetColor(8, HexToColor("b9ed7b")));
                listColor.Add(SetColor(9, HexToColor("eacd87")));
                break;

            case 2:
                listColor.Add(SetColor(0, HexToColor("370f68")));
                listColor.Add(SetColor(1, HexToColor("540f66")));
                listColor.Add(SetColor(2, HexToColor("7d0f63")));
                listColor.Add(SetColor(3, HexToColor("ae0f5f")));
                listColor.Add(SetColor(4, HexToColor("d70f5c")));
                listColor.Add(SetColor(5, HexToColor("f4145a")));
                listColor.Add(SetColor(6, HexToColor("ff305e")));
                listColor.Add(SetColor(7, HexToColor("f4535c")));
                listColor.Add(SetColor(8, HexToColor("e67c5f")));
                listColor.Add(SetColor(9, HexToColor("f1a168")));
                break;


            case 3:
                listColor.Add(SetColor(0, HexToColor("dc8e82")));
                listColor.Add(SetColor(1, HexToColor("d98677")));
                listColor.Add(SetColor(2, HexToColor("bd8e7d")));
                listColor.Add(SetColor(3, HexToColor("a6a394")));
                listColor.Add(SetColor(4, HexToColor("82ab96")));
                listColor.Add(SetColor(5, HexToColor("77baa6")));
                listColor.Add(SetColor(6, HexToColor("6ac1c4")));
                listColor.Add(SetColor(7, HexToColor("58babe")));
                listColor.Add(SetColor(8, HexToColor("43c3c8")));
                listColor.Add(SetColor(9, HexToColor("40b3c8")));
                break;

            case 4:
                listColor.Add(SetColor(0, HexToColor("d628e9")));
                listColor.Add(SetColor(1, HexToColor("dc32c1")));
                listColor.Add(SetColor(2, HexToColor("e03a90")));
                listColor.Add(SetColor(3, HexToColor("e64878")));
                listColor.Add(SetColor(4, HexToColor("ed4e59")));
                listColor.Add(SetColor(5, HexToColor("ed684b")));
                listColor.Add(SetColor(6, HexToColor("ef7e41")));
                listColor.Add(SetColor(7, HexToColor("f09a39")));
                listColor.Add(SetColor(8, HexToColor("f2b633")));
                listColor.Add(SetColor(9, HexToColor("f3cb2e")));
                break;

            case 5:
                listColor.Add(SetColor(0, HexToColor("4c718e")));
                listColor.Add(SetColor(1, HexToColor("4a6581")));
                listColor.Add(SetColor(2, HexToColor("495671")));
                listColor.Add(SetColor(3, HexToColor("464762")));
                listColor.Add(SetColor(4, HexToColor("413d58")));
                listColor.Add(SetColor(5, HexToColor("393551")));
                listColor.Add(SetColor(6, HexToColor("2f304d")));
                listColor.Add(SetColor(7, HexToColor("242b48")));
                listColor.Add(SetColor(8, HexToColor("1a2846")));
                listColor.Add(SetColor(9, HexToColor("11223d")));
                break;
            case 6:
                listColor.Add(SetColor(0, HexToColor("eb8f68")));
                listColor.Add(SetColor(1, HexToColor("dca0cf")));
                listColor.Add(SetColor(2, HexToColor("cdb1e7")));
                listColor.Add(SetColor(3, HexToColor("c1c3fa")));
                listColor.Add(SetColor(4, HexToColor("bdd4fe")));
                listColor.Add(SetColor(5, HexToColor("c4deef")));
                listColor.Add(SetColor(6, HexToColor("c8e8cd")));
                listColor.Add(SetColor(7, HexToColor("e0efb4")));
                listColor.Add(SetColor(8, HexToColor("eff793")));
                listColor.Add(SetColor(9, HexToColor("f0f257")));
                break;
            case 7:
                listColor.Add(SetColor(0, HexToColor("cec283")));
                listColor.Add(SetColor(1, HexToColor("b33d9d")));
                listColor.Add(SetColor(2, HexToColor("9954b6")));
                listColor.Add(SetColor(3, HexToColor("8569cb")));
                listColor.Add(SetColor(4, HexToColor("7d7dda")));
                listColor.Add(SetColor(5, HexToColor("8593e4")));
                listColor.Add(SetColor(6, HexToColor("95a8e9")));
                listColor.Add(SetColor(7, HexToColor("acbeec")));
                listColor.Add(SetColor(8, HexToColor("bfceec")));
                listColor.Add(SetColor(9, HexToColor("d7e6fa")));
                break;
            case 8:
                listColor.Add(SetColor(0, HexToColor("3d3790")));
                listColor.Add(SetColor(1, HexToColor("5f4d9f")));
                listColor.Add(SetColor(2, HexToColor("765caa")));
                listColor.Add(SetColor(3, HexToColor("8f6db4")));
                listColor.Add(SetColor(4, HexToColor("a57dbb")));
                listColor.Add(SetColor(5, HexToColor("b68cbb")));
                listColor.Add(SetColor(6, HexToColor("c69abb")));
                listColor.Add(SetColor(7, HexToColor("d5a9ba")));
                listColor.Add(SetColor(8, HexToColor("e1b4b8")));
                listColor.Add(SetColor(9, HexToColor("f0bcb1")));
                break;
            case 9:
                listColor.Add(SetColor(0, HexToColor("4b3d4a")));
                listColor.Add(SetColor(1, HexToColor("574a56")));
                listColor.Add(SetColor(2, HexToColor("645662")));
                listColor.Add(SetColor(3, HexToColor("6f636d")));
                listColor.Add(SetColor(4, HexToColor("7b707a")));
                listColor.Add(SetColor(5, HexToColor("868086")));
                listColor.Add(SetColor(6, HexToColor("949398")));
                listColor.Add(SetColor(7, HexToColor("a2a7a8")));
                listColor.Add(SetColor(8, HexToColor("aeb9b6")));
                listColor.Add(SetColor(9, HexToColor("c0d1cc")));
                break;

        }

        return listColor;
    }

    private ColorInfo SetColor(int index , Color color)
    {
        ColorInfo colorInfo = new ColorInfo();
        colorInfo.IdColor = index;
        colorInfo.colors = color;
        return colorInfo;
    }
    public static Color HexToColor(string hex)
    {
        hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
        hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
        byte a = 255;//assume fully visible unless specified in hex
        byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
        byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
        byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
        //Only use alpha if the string has enough characters
        if (hex.Length == 8)
        {
            a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
        }
        return new Color32(r, g, b, a);
    }

    private string GetDeviceId()
    {
        string systemID = string.Empty;
#if UNITY_IPHONE
        systemID = "IOS_"+SystemInfo.deviceUniqueIdentifier+System.DateTime.Now.Ticks;
#elif UNITY_ANDROID
        systemID = "ANR_" + SystemInfo.deviceUniqueIdentifier + System.DateTime.Now.Ticks;
#endif
        return systemID;
    }

}

public class PlayerDataService : MonoBehaviour
{

    public const string USER_DATA_PROFILE = "ProfileService";

    private string gameDataProjectFilePath = "/PlayerData.dat";

    private PlayerDataModel playerData;

    public delegate void EventUpdateDiamond();

    public event EventUpdateDiamond OnUpdateDiamond;

    string _filePath;


    public PlayerDataModel GetPlayerData()
    {
        if (this.playerData == null)
        {
            Debug.LogError("GET PLAYER DATA RETURN NULL - NEED INIT");
        }
        return this.playerData;
    }

    public void UpdateDiamond(int diamond)
    {
        this.playerData.diamond += diamond;

        if(this.OnUpdateDiamond != null)
        {
            this.OnUpdateDiamond();
        }
    }

    public void Start()
    {
        #if UNITY_EDITOR
            _filePath = Application.dataPath + gameDataProjectFilePath;
        #else
            _filePath = Application.persistentDataPath + gameDataProjectFilePath;
        #endif
        PlayerDataModel playerDataModel = LoadGame();
        this.playerData = playerDataModel;
    }
    public PlayerDataModel CreateNewSave()
    {
        PlayerDataModel save = new PlayerDataModel();
        return save;
    }

    void CreateNewData()
    {
        PlayerDataModel data = GetPlayerData();
        PlayerDataModel save = CreateNewSave();
        save = data;
        SaveGame(save);
    }

    public void SaveGame(PlayerDataModel save)
    {
        // Save save = CreateSaveGameObject();
        BinaryFormatter bf = new BinaryFormatter();
        // FileStream file = File.Create(_filePath);
        
        string json = JsonUtility.ToJson(save);
        int totalByte = Encoding.UTF8.GetByteCount(json);
        byte[] bytes = Encoding.UTF8.GetBytes(json);
        using (FileStream fs = File.Create(_filePath))
        {
            fs.Write(bytes, 0, totalByte);
            fs.Flush();
            fs.Close();
        }
    }

    public PlayerDataModel GetSave()
    {
        //Save save = CreateSaveGameObject();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(_filePath, FileMode.Open);
        return (PlayerDataModel)bf.Deserialize(file);
    }

    public bool IsSaveFileExist()
    {
        return File.Exists(_filePath);
    }

    [ContextMenu("Load Data Test")]
    public PlayerDataModel LoadGame()
    {
        PlayerDataModel save = null;
        if (File.Exists(_filePath))
        {
            byte[] bytes = File.ReadAllBytes (_filePath);
            string data = Encoding.UTF8.GetString(bytes);
            PlayerDataModel PlayerData = JsonUtility.FromJson<PlayerDataModel>(data);
            return PlayerData;
            //  BinaryFormatter bf = new BinaryFormatter();
            //  FileStream file = File.Open(_filePath, FileMode.Open);
            // save = (PlayerDataModel)bf.Deserialize(file);
            // file.Close();
            // if (save != null)
            // {
            //     return save;
            // }
            // else
            // {
            //     return new PlayerDataModel();
            // }
        }
        else
        {
            Debug.Log("No game saved!");
            // Create a new save file for the game.
            var playerData = new PlayerDataModel();
            SaveGame(playerData);
            return playerData;
        }
    }


}
#endregion

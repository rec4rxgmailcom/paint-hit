﻿using UnityEngine;
using DG.Tweening;

namespace Assets.Scripts.Core.UI
{
    public class AnimationViewConfig : MonoBehaviour
	{
		public enum AnimationType
		{
			NONE,
			TWEEN,
			CUSTOM,
			FADE
		}

		public enum AnimationDirection
		{
			EAST,
			WEST,
			NORTH,
			SOUTH
		}

		public delegate void AnimationEvent ();

		private AnimationEvent _animateInCompleteCallback;
		private AnimationEvent animateOutCompleteCallback;

		[HideInInspector]
		[SerializeField]
		private string m_PresentCompleteEvent;

		[HideInInspector]
		[SerializeField]
		private string m_RemoveCompleteEvent;

		[SerializeField]
		private AnimationType m_AnimationType = AnimationType.NONE;

		[HideInInspector]
		[SerializeField]
		private AnimationDirection m_AnimationDirection = AnimationDirection.SOUTH;

		[HideInInspector]
		[SerializeField]
		private float m_AnimationTime = 0.5f;

		[HideInInspector]
		[SerializeField]
		private float m_FadeInValue = 1f;

		[HideInInspector]
		[SerializeField]
		private float m_FadeOutValue = 0.3f;

		bool m_FirstAnimation = true;
		Vector2 m_CorrectPosition;
		Vector2 m_OffscreenPosition;

		public AnimationType animationType {
			get {
				return m_AnimationType;
			}
		}

		public AnimationDirection animationDirection {
			get {
				return m_AnimationDirection;
			}
			set {
				m_AnimationDirection = value;
			}
		}

		public float animationTime {
			get {
				return m_AnimationTime;
			}
			set {
				m_AnimationTime = value;
			}
		}

		public string presentCompleteEvent {
			get {
				return m_PresentCompleteEvent;
			}
			set {
				m_PresentCompleteEvent = value;
			}
		}

		public string removeCompleteEvent {
			get {
				return m_RemoveCompleteEvent;
			}
			set {
				m_RemoveCompleteEvent = value;
			}
		}

		public float fadeInValue {
			get {
				return m_FadeInValue;
			}
			set {
				m_FadeInValue = value;
			}
		}

		public float fadeOutValue {
			get {
				return m_FadeOutValue;
			}
			set {
				m_FadeOutValue = value;
			}
		}

        public AnimationEvent AnimateInCompleteCallback
        {
            get
            {
                return _animateInCompleteCallback;
            }

            set
            {
                _animateInCompleteCallback = value;
            }
        }

        private void SetupPositions ()
		{
			m_CorrectPosition = gameObject.GetComponent<RectTransform> ().anchoredPosition;

			if (m_AnimationDirection == AnimationDirection.SOUTH) {
				m_OffscreenPosition = new Vector2 (m_CorrectPosition.x, m_CorrectPosition.y - gameObject.transform.parent.GetComponent<RectTransform> ().rect.height);
			} else if (m_AnimationDirection == AnimationDirection.NORTH) {
				m_OffscreenPosition = new Vector2 (m_CorrectPosition.x, m_CorrectPosition.y + gameObject.transform.parent.GetComponent<RectTransform> ().rect.height);
			} else if (m_AnimationDirection == AnimationDirection.WEST) {
				m_OffscreenPosition = new Vector2 (m_CorrectPosition.x - gameObject.transform.parent.GetComponent<RectTransform> ().rect.width, m_CorrectPosition.y);
			} else if (m_AnimationDirection == AnimationDirection.EAST) {
				m_OffscreenPosition = new Vector2 (m_CorrectPosition.x + gameObject.transform.parent.GetComponent<RectTransform> ().rect.width, m_CorrectPosition.y);
			}
		}

		internal void AnimateIn (AnimationEvent callback)
		{
			_animateInCompleteCallback = callback;

			switch (m_AnimationType) {
			case AnimationType.NONE:
				AnimateInCompleted ();
				break;
			case AnimationType.FADE:
				{
					var canvasGroup = GetComponent<CanvasGroup> ();
					if (canvasGroup == null) {
						canvasGroup = this.gameObject.AddComponent<CanvasGroup> ();
					}
					canvasGroup.alpha = 0;
					// canvasGroup.DOFade (m_FadeInValue, m_AnimationTime).OnComplete (() => {
					// 	AnimateInCompleted ();
					// });
				}
				break;
			case AnimationType.TWEEN:
				if (m_FirstAnimation) {
					SetupPositions ();
					m_FirstAnimation = false;
				}

				gameObject.GetComponent<RectTransform> ().anchoredPosition = m_OffscreenPosition;

				// iTween.ValueTo (gameObject, iTween.Hash (
				// 	"from", gameObject.GetComponent<RectTransform> ().anchoredPosition,
				// 	"to", m_CorrectPosition,
				// 	"time", m_AnimationTime,
				// 	"oncomplete", "AnimateInCompleted",
				// 	"onupdate", "MoveGuiElement"
				// ));
				break;
			}
		}

		internal void AnimateOut (AnimationEvent callback)
		{
			if (m_FirstAnimation) {
				SetupPositions ();
				m_FirstAnimation = false;
			}

			animateOutCompleteCallback = callback;

			switch (m_AnimationType) {
			case AnimationType.NONE:
				AnimateOutCompleted ();
				break;
			case AnimationType.FADE:
				{
					var canvasGroup = GetComponent<CanvasGroup> ();
					if (canvasGroup == null) {
						canvasGroup = this.gameObject.AddComponent<CanvasGroup> ();
					}
					canvasGroup.alpha = 1f;
					// canvasGroup.DOFade (m_FadeOutValue, m_AnimationTime).OnComplete (() => {
					// 	AnimateOutCompleted ();
					// });
				}
				break;
			case AnimationType.TWEEN:
				// iTween.ValueTo (gameObject, iTween.Hash (
				// 	"from", m_CorrectPosition,
				// 	"to", m_OffscreenPosition,
				// 	"time", m_AnimationTime,
				// 	"oncomplete", "AnimateOutCompleted",
				// 	"onupdate", "MoveGuiElement"
				// ));

				// transform.DOMove()

				break;
			}
		}

		void AnimateInCompleted ()
		{
			if (_animateInCompleteCallback != null) {
				_animateInCompleteCallback ();
			}
		}

		void AnimateOutCompleted ()
		{
			if (animateOutCompleteCallback != null) {
				animateOutCompleteCallback ();
			}
		}

		void MoveGuiElement (Vector2 position)
		{
			gameObject.GetComponent<RectTransform> ().anchoredPosition = position;
		}
	}
}

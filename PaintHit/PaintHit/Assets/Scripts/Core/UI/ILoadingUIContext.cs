﻿using System;

namespace Assets.Scripts.Core.UI
{
	public interface ILoadingUIContext
	{
		event Action<object> OnStartLoading;
		event Action<object> OnCompleteLoading;
	}
}



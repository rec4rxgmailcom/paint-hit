﻿using UnityEngine;
using System;
using Assets.Scripts.InGame.Systems;
using Zenject;


namespace Assets.Scripts.Core.UI
{

    [RequireComponent(typeof(CanvasGroup))]
    public abstract class UIView : MonoBehaviour, IInitializable
    {
        public string UIName { get; set; }

        public bool IsLoadingView { get; set; }

        public Action OnHideComplete;

        CanvasGroup _canvasGroup;

        bool _isShow = false;

        [Inject] NotificationController _notificationController;

        [Inject] 
        void IInitializable.Initialize()
        {
            _canvasGroup = GetComponent<CanvasGroup>();
        }

        public virtual void Present()
        {
            EnableView(true);
            AnimationViewConfig animConfig = GetComponent<AnimationViewConfig>();
            if(animConfig != null)
            {
                animConfig.AnimateIn(null);
            }
            else{
                //OnPresentComplete();    
            }
        }

        public virtual void Hide(bool isDestroy)
        {
            AnimationViewConfig animConfig = GetComponent<AnimationViewConfig>();
            if(animConfig != null && _isShow)
            {
                animConfig.AnimateOut(() => {
                    OnHideViewComplete(isDestroy);
                });
            }
            else{
                OnHideViewComplete(isDestroy);
            }
        }
        
        public void DestroyThisView()
        {
            Destroy(gameObject);
        }

        void OnHideViewComplete(bool isDestroy)
        {
            if(OnHideComplete != null)
            {
                OnHideComplete();
            }

            EnableView(false);
            if(isDestroy){
                transform.parent = null;
                DestroyThisView();
            }
        }

        void EnableView(bool isEnable)
        {
            _canvasGroup.alpha = isEnable ? 1 : 0;
            _canvasGroup.interactable = isEnable;
            _canvasGroup.blocksRaycasts = isEnable;
            _isShow = isEnable;
        }

        protected void NotifyToController(string eventId, object data = null, Component sender = null)
        {
            _notificationController.Notify(eventId, data, sender);
        }

        public class Factory : PrefabFactory<UIView>
        {
        }
    }
}



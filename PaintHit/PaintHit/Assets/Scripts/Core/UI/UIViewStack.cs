﻿using UnityEngine;

namespace Assets.Scripts.Core.UI
{
    public class UIViewStack : MonoBehaviour
    {
        Transform _transform;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        void Awake()
        {
            _transform = transform;
        }

        public void PopView(UIView uiView, bool isDestroy = false)
        {   
            // for(int i = _transform.childCount - 1; i >= 0; i++)
            // {
            //     GameObject childViewObj = _transform.GetChild(i).gameObject;
            //     if(childViewObj == uiView.gameObject)
            //     {
            //         UIView childView = childViewObj.GetComponent<UIView>();
            //         childView.Hide(isDestroy);
            //     }
            // }
            uiView.Hide(isDestroy);
        }

        public void PushView(UIView uiView)
        {
            uiView.Present();
            if(uiView.transform.parent != _transform)
            {
                uiView.transform.SetParent(_transform, false);        
            }
            
            uiView.transform.SetAsLastSibling();
        }
    }
}


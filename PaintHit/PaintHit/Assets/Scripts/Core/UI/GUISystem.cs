﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Text;

namespace Assets.Scripts.Core.UI
{
    public class GUISystem : GameBaseSystem
    {
		[SerializeField] private UIViewStack _uiViewStack;

        [SerializeField] private UIViewStack _uiViewLoadingStack;

        [Inject] private readonly GameSystemManager gameSystemmanager;

        [Inject] private readonly UIView.Factory _viewFactory;
        
        DiContainer _diContainer;

        private Dictionary<int, string> _dictGUIName = new Dictionary<int, string>();

        private List<UIView> _viewList = new List<UIView>();
        private List<UIView> _viewLoadingList = new List<UIView>();

        public static class GUINames
        {
            public const int LoadingView = 1;
            public const int PanelTest = 2;
            public const int PlashView = 3;
            public const int PanelMenu = 4;
        }

        override protected void Init()
        {
            //gameSystemmanager.RegisterGameSystem(this);
            _dictGUIName.Add(GUISystem.GUINames.LoadingView, "LoadingView");
            _dictGUIName.Add(GUISystem.GUINames.PanelTest, "Panel");
            _dictGUIName.Add(GUISystem.GUINames.PlashView, "PlashView");
            _dictGUIName.Add(GUISystem.GUINames.PanelMenu, "PanelMenu");
        }

        public UIView Show(int viewKey, bool isLoadingView = false)
        {
            if(_dictGUIName.ContainsKey(viewKey))
            {
                string viewName = string.Empty;
                _dictGUIName.TryGetValue(viewKey, out viewName);
                UIView view = GetCache(viewName);
                if(view == null)
                {
                   // GameObject viewObject = Instantiate(GetGUI(viewName));        
                    //view = viewObject.GetComponent<UIView>();
                    view = GetGUI(viewKey);
                    view.UIName = viewName;
                    view.IsLoadingView = isLoadingView;
                }

                if(isLoadingView)
                {
                    _viewLoadingList.Add(view);
                    _uiViewLoadingStack.PushView(view);
                }
                else
                {
                    _viewList.Add(view);
                    _uiViewStack.PushView(view);        
                }
                

                return view;
            }

            return null;
        }

        public void Hide (int guiKey, bool isDestroy = false)
        {
            string guiName = _dictGUIName[guiKey];

            UIView view = GetCache(guiName);

            if(isDestroy)
            {
                _viewList.Remove(view);
            }

            view.Hide(isDestroy);
        }

        public void HideAll ()
        {
            for(int i = 0; i < _viewList.Count; i++)
            {
                UIView view = _viewList[i];

				//_viewList.RemoveAt (i);
                view.DestroyThisView();
				//i--;
            }

            _viewList.Clear();

            // May be the performance is going down here.
            // Need to improve this?
            Resources.UnloadUnusedAssets();
            System.GC.Collect();
        }

        public UIView GetCache(string guiName)
        {
            return _viewList.Find(view => view.UIName == guiName);
        }

        UIView GetGUI(int guiName)
        {
            StringBuilder builder = new StringBuilder();
            string value = builder.Append("GUI/").Append(_dictGUIName[guiName]).ToString();
            //Debug.Log(value);

            //  Change to load from asset bundle in the future.
           // GameObject gObj = Resources.Load(value) as GameObject;
            return _viewFactory.Create(value);
        }
    }
}



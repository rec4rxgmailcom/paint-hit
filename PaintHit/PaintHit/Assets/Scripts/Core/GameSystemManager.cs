﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Core.SystemContext;
namespace Assets.Scripts.Core
{
    public class GameSystemManager : MonoBehaviour
	{
		public List<GameBaseSystem> GameSystems { get; private set; }

		DiContainer _diContainer;

		public void InstallBinding(DiContainer diContainer)
		{
			_diContainer = diContainer;
			GameSystems = new List<GameBaseSystem> (GetComponentsInChildren<GameBaseSystem> ());

			for (int i = 0; i < GameSystems.Count; i++) 
			{
				_diContainer.Bind(GameSystems[i].GetType()).FromInstance(GameSystems[i]);
			}
		}

		public void OnContextLoaded(SystemContext.Context context)
		{
			for (int i = 0; i < GameSystems.Count; i++)
			{
				GameSystems[i].OnLoadContext(context);
			}
		}

		public void OnContextUnloaded(SystemContext.Context context)
		{
			for (int i = 0; i < GameSystems.Count; i++)
			{
				GameSystems[i].OnUnLoadContext(context);
			}
		}
	
		public void RegisterGameSystem(GameBaseSystem gameSystem)
		{
			GameSystems.Add(gameSystem);	
			gameSystem.transform.SetParent(transform);	
			_diContainer.Bind(gameSystem.GetType()).FromInstance(gameSystem);
		}

		public void SetDiContainer(DiContainer diContainer)
		{
			_diContainer = diContainer;
		}
	}
}


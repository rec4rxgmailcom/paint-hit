/*****************************************************************************
 * Spine Asset Pack License
 * Version 1.0
 * 
 * You are granted a perpetual, non-exclusive, non-sublicensable and
 * non-transferable license to use the Asset Pack and derivative works only as
 * incorporated and embedded components of your software applications and to
 * distribute such software applications. Any source code contained in the Asset
 * Pack may not be distributed in source form. You may otherwise not reproduce,
 * distribute, sublicense, rent, lease or lend the Asset Pack. It is emphasized
 * that you are not entitled to distribute or transfer the Asset Pack in any way
 * other way than as integrated components of your software applications.
 * 
 * THIS ASSET PACK IS PROVIDED BY ESOTERIC SOFTWARE "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 * EVENT SHALL ESOTERIC SOFTWARE BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS ASSET PACK, EVEN IF
 * ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *****************************************************************************/
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

namespace Assets.Scripts.Core
{
    public class SoundPalette : MonoBehaviour
    {

        public enum SOUND_EFFECT
        {
            CLICK,
            COIN,
            FINISHCIRCLE,
            HIT1,
            HIT2,
            LEVELUP,
            LEVELUP2,
            NEWCIRCLE
        }

        public static Hashtable soundTable;
        public static Hashtable categoryTable;

        public int maxChannels = 5;
        public List<AudioClip> sounds;
        public AudioSource[] channels;
        public SoundCategory[] categories;
        public float volumeMultiplier;

        public AudioSource bgmSource;
        public AudioSource soundSource;
        public AudioClip[] soundEffect;
        public AudioClip[] bgm;

        public float volumeSoundEffect = 1;

        public bool persistent = false;

        public void Init()
        {
            SetUpSoundSetting();

            soundTable = new Hashtable();
            categoryTable = new Hashtable();

            if (channels.Length == 0)
            {
                channels = new AudioSource[maxChannels];
                for (int i = 0; i < maxChannels; i++)
                {
                    GameObject go = new GameObject("_SoundPalette_Channel_" + i);
                    go.hideFlags = HideFlags.HideAndDontSave;
                    go.transform.parent = transform;
                    channels[i] = (AudioSource)go.AddComponent<AudioSource>();
                    channels[i].dopplerLevel = 0;
                    channels[i].minDistance = 15;
                }
            }
            else
            {
                maxChannels = channels.Length;
            }

            foreach (AudioClip clip in sounds)
            {
                soundTable.Add(clip.name, clip);
            }

            foreach (SoundCategory c in categories)
            {
                categoryTable.Add(c.name, c);
                foreach (AudioClip clip in c.sounds)
                {
                    string nm = clip.name;
                    if (soundTable.ContainsKey(c.name + "/" + nm))
                    {
                        int i = 0;
                        while (soundTable.ContainsKey(c.name + "/" + nm))
                        {
                            i++;
                            nm = clip.name + i;
                        }
                    }
                    soundTable.Add(c.name + "/" + nm, clip);
                }

            }
        }

        public AudioSource PlaySound(string str)
        {
            return PlaySound(str, 1.0f, 1.0f);
        }

        public AudioSource PlaySound(string str, float volume)
        {
            return PlaySound(str, volume, 1.0f);
        }

        public AudioSource PlaySound(string str, float volume, float pitch)
        {
            return PlaySound(str, volume, pitch, Vector3.zero);
        }

        public AudioSource PlaySound(string str, float volume, float pitch, Vector3 position)
        {
            return PlaySound(str, volume, pitch, position, 1);
        }
        public AudioSource PlaySound(string str, float volume, float pitch, Vector3 position, float minDistance)
        {
            if (str == "") return null;

            if (str.Contains("/"))
            {
                string[] chunks = str.Split('/');
                if (categoryTable.ContainsKey(chunks[0]))
                {
                    if (chunks[1] == "Random")
                    {
                        SoundCategory c = (SoundCategory)(categoryTable[chunks[0]]);
                        return PlaySound(c.sounds[(int)Random.Range(0, c.sounds.Count)], volume, pitch, position, minDistance);
                    }
                }
            }

            if (soundTable.ContainsKey(str))
            {
                return PlaySound((AudioClip)soundTable[str], volume, pitch, position, minDistance);
            }

            return null;
        }

        public AudioSource currentAudioSource;
        public AudioSource PlaySound(AudioClip clip, float volume, float pitch, Vector3 position, float minDistance)
        {
            int i = 0;
            foreach (AudioSource src in this.channels)
            {
                if (!src.isPlaying)
                {
                    src.transform.position = position;
                    if (src.transform.position.x == 0 && src.transform.position.y == 0)
                    {
                        src.time = 0;
                    }
                    //src.pitch = Mathf.Lerp(0.4f, 1f, Mathf.InverseLerp(0.5f, 2f, Time.timeScale));
                    src.pitch = pitch;
                    src.clip = clip;
                    src.volume = volume;
                    src.minDistance = minDistance;
                    src.Play();
                    currentAudioSource = src;
                    return src;
                }
                i++;
            }

            return null;
        }

        public bool IsFinishSong(bool isFinish = true)
        {
            if (!isFinish)
            {
                return true;
            }
            return false;
        }

        public void StopAll(float timeFade = 0)
        {
            foreach (AudioSource src in this.channels)
            {
                if (src.isPlaying)
                {
                    src.Stop();
                }
            }
            StopBGM(timeFade);
        }

        public void SetVolumeSoundEffect(float value)
        {
            this.volumeSoundEffect = value;
        }

        public void PlaySoundEffect(SOUND_EFFECT option = SOUND_EFFECT.CLICK)
        {
            this.soundSource.volume = this.volumeSoundEffect;
            this.soundSource.clip = this.soundEffect[(int)option];
            this.soundSource.Play();
        }

        public void StopBGM(float timeFade)
        {
            this.bgmSource.DOFade(0, timeFade)
                .OnComplete(() =>
                {
                    this.bgmSource.Stop();
                })
                .Play();
            this.isPlayBGM = false;
        }

        public void DisableVolumeBGM()
        {
           
            this.bgmSource.DOFade(0, 1f)
                .Play();
        }

        public void EnableVolumeBGM()
        {
            this.bgmSource.DOFade(1, 1f)
                .Play();
        }

        public bool isPlayBGM;
        public void PlayBGM()
        {
            if (this.isPlayBGM == false)
            {
                int random = UnityEngine.Random.Range(0, this.bgm.Length);
                this.bgmSource.volume = this.volumeMultiplier * 1.0f;
                this.bgmSource.clip = this.bgm[random];
                this.bgmSource.loop = true;
                this.bgmSource.Play();
                this.isPlayBGM = true;
            }
        }


        private void SetUpSoundSetting()
        {
            this.volumeSoundEffect = PlayerPrefs.GetInt(GameConst.OPTION_SOUND, 1);
            this.volumeMultiplier = PlayerPrefs.GetInt(GameConst.OPTION_MUSIC, 1);

            GameObject soundSource = new GameObject("soundSource");
            soundSource.hideFlags = HideFlags.HideAndDontSave;
            this.soundSource = (AudioSource)soundSource.AddComponent<AudioSource>();

            GameObject bgmSource = new GameObject("bgmSource");
            bgmSource.hideFlags = HideFlags.HideAndDontSave;
            this.bgmSource = (AudioSource)bgmSource.AddComponent<AudioSource>();
        }

        [System.Serializable]
        public class SoundCategory
        {
            public string name;
            public List<AudioClip> sounds;
        }

        public void AddSounds(AudioClip songName)
        {
            this.sounds.Add(songName);
        }
    }
}
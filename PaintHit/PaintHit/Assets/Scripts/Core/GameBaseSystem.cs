﻿using UnityEngine;
using Zenject;
using Assets.Scripts.Core.SystemContext;

namespace Assets.Scripts.Core
{
	public abstract class GameBaseSystem : MonoBehaviour, IInitializable
	{
        [Inject]
        void IInitializable.Initialize()
        {
            Init();
        }

        public virtual void OnLoadContext(SystemContext.Context context) { }

        public virtual void OnUnLoadContext(SystemContext.Context context) { }

        public virtual void OnDestroyontext(SystemContext.Context context) { }

        public virtual void OnGamePaused() { }

        public virtual void OnGameResumed() { }

        protected virtual void Init() {}
    }
}
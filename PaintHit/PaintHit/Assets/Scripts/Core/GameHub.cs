﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core.SystemContext;
using Assets.Scripts.Core.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

namespace Assets.Scripts.Core
{
	public class GameHub : MonoBehaviour
    {
		[SerializeField] List<ContextData> _listContextData;
        [SerializeField] ContextData _initialContextData;

		[Inject] readonly GameSystemManager gameSystemmanager;
		[Inject] readonly ZenjectSceneLoader _zenjectSceneLoader;

		[Inject] GUISystem guiSystem;

		public ContextData CurrentContextData 
		{
			get { return _currentContextData; }
		}

		public List<ContextData> ListContextData 
		{
			get { return _listContextData; }
		}

		public static class ContextName 
		{
            public const string MainMenuContext = "MainMenu_Context";
            public const string MainGameContext = "MainGame_Context";
		}

		private ContextData _currentContextData; 
		private SystemContext.Context _currentContext;

		#region Public methods section

		public void InitContext(SystemContext.Context context)
		{
			_currentContext = context;
		}

		/// <summary>
		/// Initial all configs
		/// </summary>
		public void Initialization()
		{

		}

		public void StartLoadLocalizationData()
		{
			
			StartCoroutine(LoadLocalizationData());
		}

		public void LoadInitialContext()
		{
			LoadContext (_initialContextData);
		}

		public void LoadAssetBundles(Action onSuccess, Action onError)
		{
			if (true) {
				if (onSuccess != null) {
					onSuccess ();
				}
			}
//			else {
//				if (onError != null) {
//					onError ();
//				}
//			}
		}

		public void UnloadCurrentContext()
		{
			if (_currentContext != null) {
				gameSystemmanager.OnContextUnloaded (_currentContext);
				_currentContext.UnloadContext ();

				if (_currentContextData != null) 
				{
					StartCoroutine(UnloadSceneAtTheEndOfFrame (_currentContextData.SceneName));
				}

				_currentContext = null;
				_currentContextData = null;
			}
		}

		public void LoadContext(string contextName, bool isShowSceneLoading = true)
		{
			ContextData contextData = _listContextData.Find(c => c.ContextName.Equals(contextName));
			LoadContext(contextData, isShowSceneLoading);
		}

		public void LoadContext(ContextData contextData , bool isShowSceneLoading = true)
		{
			UnloadCurrentContext ();

			_currentContextData = contextData;
			if (isShowSceneLoading)
			{
				// TODO: show affect loading before show scene.
			}
			StartCoroutine(LoadSceneAsync(contextData.SceneName));
		}

		#endregion

		#region Private methods section

		private IEnumerator LoadSceneAsync(string sceneName)
		{
			AsyncOperation async = _zenjectSceneLoader.LoadSceneAsync (sceneName, LoadSceneMode.Additive, null, LoadSceneRelationship.Child);
			yield return async;

			_currentContext.OnContextInitialize(ContextLoaded);
		}

		private IEnumerator UnloadSceneAtTheEndOfFrame(string scene)
		{
			//Debug.Log("UnloadSceneAtTheEndOfFrame");
			yield return new WaitForEndOfFrame ();
			SceneManager.UnloadSceneAsync (scene);
		}

		private void ContextLoaded()
		{
			
			_currentContext.LoadContext();
			gameSystemmanager.OnContextLoaded(_currentContext);
		}

		private IEnumerator LoadLocalizationData()
		{
			Debug.Log("LoadLocalizationData");
			return null;
		}

		private IEnumerator UnloadAllContext()
		{
			Debug.Log("UnloadAllContext");
			for(int i = 0; i < _listContextData.Count; i++)
			{				
				if(_listContextData[i].Scene.IsValid())
				{
					AsyncOperation asyncScene = SceneManager.UnloadSceneAsync(_listContextData[i].Scene);
					yield return asyncScene;
				}
			}

			AsyncOperation async = Resources.UnloadUnusedAssets();
			yield return async;
		}


		#endregion
    }
}


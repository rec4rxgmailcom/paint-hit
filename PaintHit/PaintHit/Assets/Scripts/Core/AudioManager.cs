﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Core
{
    public class AudioManager : MonoBehaviour
    {
        AudioSource _musicSource;
        AudioSource _soundSource;

        bool _musicEnable = true;
        bool _soundEnable = true;

        [SerializeField] public AudioClip[] soundEffect;

        [SerializeField] public AudioClip[] SoundBackground;
      
        private void Start()
        {
            _musicSource = gameObject.AddComponent<AudioSource>();
            _soundSource = gameObject.AddComponent<AudioSource>();

            _musicSource.playOnAwake = false;
            _soundSource.playOnAwake = false;

            _soundSource.volume = 0.5f;
        }

        public void PlayBackgroundMusic(AudioClip audioClip , bool loop = true)
        {
            _musicSource.loop = loop;
            _musicSource.clip = audioClip;

            if(_musicEnable)
            {
                _musicSource.Play();
            }
        }

        public void StopBackgroundMusic()
        {
            _musicSource.Stop();
        }

        public void PlaySoundEffect(AudioClip audioClip , float pitch = 1.0f)
        {
            if(_soundEnable)
            {
                _soundSource.pitch = pitch;
                _soundSource.PlayOneShot(audioClip);
            }
        }

        public void StopSoundEffect()
        {
            _soundSource.Stop();
        }

        public void SetMusicEnable(bool enable)
        {
            _musicEnable = enable;

            if(!_musicEnable && _musicSource.isPlaying)
            {
                StopBackgroundMusic();
            }
            else if(_musicEnable && !_musicSource.isPlaying && _musicSource.clip != null)
            {
                _musicSource.Play();
            }
        }

        public void SetSoundEnable(bool enable)
        {
            _soundEnable = enabled;
        }

        public void SetMusicVolum(float volum)
        {
            _musicSource.volume = volum;
        }

        public void SetSoundVolum(float volum)
        {
            _soundSource.volume = volum;
        }

        public void OnAppPaused()
        {
            _musicSource.Pause();
            _soundSource.Pause();
        }

        public void OnAppResumed()
        {
            _musicSource.UnPause();
            _soundSource.UnPause();
        }
    }
}

﻿
namespace Assets.Scripts.Core
{
    public enum EVENT_ID
    {
        NONE = 0,
        CHANGE_CONTEXT,
        CALCULATE_GRID_POSITION
    }

    public struct ControllerEvent
    {
        public const string CHANGE_CONTEXT = "CHANGE_CONTEXT";

    }

    public struct Tag
    {
        public const string NODE = "Node";
    }

    public struct Event 
    {
        public struct Character
        {
            public const string COMPLETE_A_NODE = "COMPLETE_A_NODE";
        }
    }

    public struct Animation
    {
        public struct Player
        {
            public const string IDLE = "Idle";
            public const string WALK = "isWalk";
        }
    }

    public struct PlayeMakerEvent
    {
        public struct Player
        {
            public const string WALK = "WALK";
            public const string IDLE = "IDLE";
        }
    }

    public enum NORMAL { UP = 0, DOWN, LEFT, RIGHT, FORWARD, BACK };

    public enum CUBE_CONFIG_DIRECTION { UP = 0, DOWN, LEFT, RIGHT, FORWARD, BACK };

    public enum PLAYER_STATE { NONE = 0, IDLE, WALK, CLIMB, }

    public enum NODE_TYPE { CUBE = 0, SLOPE, STAIR, LADDER }

    public enum NODE_CAPACITY { NORMAL = 0, EXPLOSION, FIRE, ICE }
    public enum MODE_TYPE{ FLOOR = 0, SLOPE, STAIR }
}




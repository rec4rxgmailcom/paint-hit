﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Assets.Scripts.Core;
using UnityEngine;

public class DataManager : GameBaseSystem 
{
    const string SAVE_FILE_NAME = "/save.dat";

    // [HideInInspector]
    // public Save SaveData;
    
    public Save CreateNewSave()
    {
        Save save = new Save();
        save.LocalNotificationData = new NotificationData();
        save.DailyRewardData = new DailyReward();
        return save;
    }

    public void SaveGame(Save save)
    {
        // Save save = CreateSaveGameObject();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + SAVE_FILE_NAME);
        bf.Serialize(file, save);
        file.Close();
    }

    public Save GetSave()
    {
        // Save save = CreateSaveGameObject();
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + SAVE_FILE_NAME, FileMode.Open);
        return (Save) bf.Deserialize(file);
    }

    public bool IsSaveFileExist()
    {
        return File.Exists(Application.persistentDataPath + SAVE_FILE_NAME);
    }

    public Save LoadGame()
    { 
        Save save = null;
        Debug.Log(Application.persistentDataPath);
        if (File.Exists(Application.persistentDataPath + SAVE_FILE_NAME))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + SAVE_FILE_NAME, FileMode.Open);
            save = (Save) bf.Deserialize(file);
            file.Close();
            if(save != null)
            {
                return save;
            }
            else
            {
                return new Save();
            }
        }
        else
        {
            Debug.Log("No game saved!");
            // Create a new save file for the game.
            // SaveGame();
            return CreateNewSave();
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISerializeData
{
	event Action<ISerializeData> OnSaveData;
	void Save();
}

public class SerializeData : ISerializeData
{
	public event Action<ISerializeData> _onSaveData;

    event Action<ISerializeData> ISerializeData.OnSaveData
    {
        add
        {
            _onSaveData += value;
        }
        remove
        {
            _onSaveData -= value;
        }
    }

    public void Save()
    {
		if(_onSaveData != null)
        	_onSaveData(this);
    }
}
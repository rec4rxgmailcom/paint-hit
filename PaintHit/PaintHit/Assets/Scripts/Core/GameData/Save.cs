﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Save 
{
	public DailyReward DailyRewardData; 

	public NotificationData LocalNotificationData;
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class AbstractData<B>
{
    protected Dictionary<string, object> _prototypeData;

    public void SetPrototypeData(Dictionary<string, object> prototypeData)
    {
        _prototypeData = prototypeData;
    }

    public T GetPrototypeData<T>()
    {
        if (_prototypeData != null)
        {
            string typeName = typeof(T).Name;
            
            if (_prototypeData.ContainsKey(typeName))
                return (T) _prototypeData[typeName];
        }
        
        return default(T);
    }

    public virtual T GetData<T>() where T : B
    {
        return default(T);
    }

    public virtual void ClearDefaultData()
    {
    }
}
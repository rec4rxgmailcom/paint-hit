﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class DailyReward
{
    public long LastDateTimeSaved;
    
    public long NearestDateTimeSaved;

    public int CurrentDayCount;  

    public int[] Rewards;

    public bool[] Claimpeds;
}

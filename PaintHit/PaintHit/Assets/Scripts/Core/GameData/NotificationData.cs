﻿using System;

[Serializable]
public class NotificationData 
{
	public int CurrentId;

	public long LastDateTimeSaved;
}

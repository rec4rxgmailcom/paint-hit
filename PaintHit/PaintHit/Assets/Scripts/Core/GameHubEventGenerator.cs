﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Core
{
	/// <summary>
	/// Manage state of life game circle.
	/// </summary>
	public class GameHubEventGenerator : MonoBehaviour
    {
		public const int ON_GAME_START = 0;
		public const int LOADING_LOCALIZATION = 1;
		public const int LOADING_ASSETBUNDLE = 2;
		public const int LOADING_INITIAL_CONTEXT = 3;
		public const int LOADING_CONTEXT = 4;
		public const int LOADING_MANIFEST = 5;
		public const int ON_GAME_PAUSE = 6;
		public const int ON_GAME_RESUME = 7;

		[Inject] private GameHub gameHub;

		// [Inject]
        // void IInitializable.Initialize()
        // {
        //     ChangeState(ON_GAME_START);
        // }

        public void Start()
		{
			ChangeState(ON_GAME_START);
		}
        
		public void ChangeState(int state)
		{
            //Debug.Log("current state => " + state);
			if (state == ON_GAME_START) {
				
				Application.targetFrameRate = 60;
				
				ChangeState (LOADING_LOCALIZATION);
			}
			else if (state == LOADING_LOCALIZATION) {
				ChangeState (LOADING_ASSETBUNDLE);
			}
			else if (state == LOADING_ASSETBUNDLE) {
                ChangeState(LOADING_INITIAL_CONTEXT);
                //gameHub.LoadAssetBundles (OnLoadAssetBundleSuccess, OnLoadAssetBundleError);
            }
			else if (state == LOADING_INITIAL_CONTEXT) {
                //Debug.Log("LOADING_INITIAL_CONTEXT");
				gameHub.LoadInitialContext ();
			}
		}

		private void OnLoadAssetBundleSuccess()
		{
			ChangeState (LOADING_INITIAL_CONTEXT);
		}

		private void OnLoadAssetBundleError()
		{
			
		}

		/// <summary>
		/// Callback sent to all game objects when the player pauses.
		/// </summary>
		/// <param name="pauseStatus">The pause state of the application.</param>
		void OnApplicationPause(bool pauseStatus)
		{
			if (pauseStatus)
			{
				//Debug.Log("Game Pause");
				ChangeState (ON_GAME_PAUSE);
			}
			else
			{
				//Debug.Log("Game resume");
				ChangeState (ON_GAME_RESUME);
			}
		}

		/// <summary>
		/// Callback sent to all game objects before the application is quit.
		/// </summary>
		void OnApplicationQuit()
		{
			//Debug.Log("Game quit");
			GC.Collect();
		}
    }
}




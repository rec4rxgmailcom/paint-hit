﻿using System;
using UnityEngine;
using Zenject;

namespace Assets.Scripts.Inputs
{
    public class MoblieInputFilter : ITickable
    {
        [Inject] InputManager _inputManager;

        const float TIME_HOLD_DRAG = 0.05f;
        const float MIN_MAGNITUDE_DIRECTION = 10f;
        const float TIME_TICK = 0.01f;
        float _timeHold;
        bool _isTouchDown;
        Vector3 _touchStartPosition;

        public void Tick()
        {
            if (_isTouchDown)
            {
                _timeHold += TIME_TICK;

            }
            if (_timeHold > TIME_HOLD_DRAG)
            {
                Vector3 direction = Input.mousePosition - _touchStartPosition;
                bool isInputMove = direction.sqrMagnitude >= MIN_MAGNITUDE_DIRECTION;

                if (isInputMove)
                {
                    _inputManager.OnUserDrag(Input.mousePosition);
                }
            }
            if (Input.GetMouseButtonDown(0))
            {
                _isTouchDown = true;
                _touchStartPosition = Input.mousePosition;
                _inputManager.OnUserPress(Input.mousePosition);
            }
            if (Input.GetMouseButtonUp(0))
            {
                _timeHold = 0f;
                _inputManager.OnUserRelease(Input.mousePosition);
                _isTouchDown = false;
            }
        }
    }
}



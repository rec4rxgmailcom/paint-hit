using UnityEngine;
using System.Collections;
using System;

namespace Assets.Scripts.Inputs
{
    public class InputManager : IInputManager
    {
        event Action<Vector3> _onInputDown;

        event Action<Vector3> _onInputUp;

        event Action<Vector3> _onInputDrag;

        public void RegisterInputDown(Action<Vector3> callback)
        {
            _onInputDown += callback;
        }

        public void RegisterInputDrag(Action<Vector3> callback)
        {
            _onInputDrag += callback;
        }

        public void RegisterInputUp(Action<Vector3> callback)
        {
            _onInputUp += callback;
        }

        public void UnregisterInputDown(Action<Vector3> callback)
        {
            _onInputDown -= callback;
        }

        public void UnregisterInputDrag(Action<Vector3> callback)
        {
            _onInputDrag -= callback;
        }

        public void UnregisterInputUp(Action<Vector3> callback)
        {
            _onInputUp -= callback;
        }

        public void OnUserPress(Vector3 target)
        {
            if(_onInputDown != null)
            {
                _onInputDown.Invoke(target);
            }
        }

        public void OnUserRelease(Vector3 target)
        {
            if(_onInputUp != null)
            {
                _onInputUp.Invoke(target);
            }
        }

        public void OnUserDrag(Vector3 target)
        {
            if(_onInputDrag != null)
            {
                _onInputDrag.Invoke(target);
            }
        }
    }

    public interface IInputManager
    {
        void RegisterInputDown(Action<Vector3> callback);

        void RegisterInputUp(Action<Vector3> callback);

        void RegisterInputDrag(Action<Vector3> callback);

        void UnregisterInputDown(Action<Vector3> callback);

        void UnregisterInputUp(Action<Vector3> callback);

        void UnregisterInputDrag(Action<Vector3> callback);
    }
}


﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;

public class ShowAdsManager : MonoBehaviour
{
    InterstitialAd interstitial;
    public string BannerId;
    public string InterstitialId;

    private RewardBasedVideoAd rewardBasedVideo;

    // Use this for initialization
    public void Start()
    {
        //Request Ads
        //RequestBanner();
        //RequestInterstitial();
        #if UNITY_ANDROID
        string appId = InterstitialId;
#elif UNITY_IPHONE
        string appId = InterstitialId;
#else
        string appId = InterstitialId;
#endif

        // Initialize the Google Mobile Ads SDK.
        MobileAds.Initialize(appId);

        // Get singleton reward based video ad reference.
        this.rewardBasedVideo = RewardBasedVideoAd.Instance;

        this.RequestInterstitial();
    }

    public void showInterstitialAd()
    {
        //Show Ad
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }

    }

    private void RequestBanner()
    {
#if UNITY_EDITOR
        string adUnitId = "unused";
#elif UNITY_ANDROID
        string adUnitId = BannerId;
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
#else
        string adUnitId = "ca-app-pub-3940256099942544/6300978111";
#endif

        // Create a 320x50 banner at the bottom of the screen.
        BannerView bannerView = new BannerView(adUnitId, AdSize.Banner, AdPosition.Bottom);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

        public void RequestInterstitial()
    {
#if UNITY_ANDROID
        string adUnitId = InterstitialId;
#elif UNITY_IPHONE
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#else
        string adUnitId = "ca-app-pub-3940256099942544/5224354917";
#endif

        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(adUnitId);
        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }


}
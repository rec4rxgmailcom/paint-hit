﻿
using System.Collections;
using System.Collections.Generic;
using Assets.Contexts.BaseScript;
using Assets.Scripts.Core;
using Assets.Scripts.Core.EventsManager;
using Assets.Scripts.InGame.Controllers;
using UnityEngine;
using Zenject;

namespace Assets.Contexts.AppStart_Context.Scripts
{
    public class AppStartController : Controller
    {

        [Inject] GameHub gameHub;
        
        public void OnLoadContext()
        {
            
            gameHub.LoadContext(GameHub.ContextName.MainMenuContext);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Assets.Contexts.BaseScript;
using Assets.Contexts.MainGame.Scripts;
using Assets.Scripts.Core;
using Assets.Scripts.Core.SystemContext;
using Assets.Scripts.Core.UI;
using Assets.Scripts.Inputs;
using UnityEngine;
using Zenject;
using Assets.SimpleAndroidNotifications;

namespace Assets.Contexts.AppStart_Context.Scripts
{
    public class RootContextInstaller : MonoInstaller
    {
        [SerializeField] GameHub gameHub;
        [SerializeField] Camera mainCamera;
        [SerializeField] GameData gameData;
        [SerializeField] AudioManager audioManager;
        [SerializeField] EventGamePlay eventGamePlay;
        [SerializeField] AppStartDelegate appStartDelegate;
        [SerializeField] GameSystemManager gameSystemmanager;
        [SerializeField] ShowAdsManager showAdsManager;
        [SerializeField] SoundPalette soundPalette;
        [SerializeField] PlayerDataService playerDataService;
        [SerializeField] NotificationExample notificationExample;

        public override void InstallBindings()
        {
            Container.Bind<GameHub>().FromInstance(gameHub);
            Container.Bind<UIView.Factory>().AsSingle();
            Container.Bind<Camera>().FromInstance(mainCamera);
            Container.Bind<GameData>().FromInstance(gameData);
            Container.Bind<AudioManager>().FromInstance(audioManager);
            Container.Bind<EventGamePlay>().FromInstance(eventGamePlay);
            Container.BindInterfacesAndSelfTo<InputManager>().AsSingle();
            Container.BindInterfacesAndSelfTo<MoblieInputFilter>().AsSingle();
            Container.Bind<GameSystemManager>().FromInstance(gameSystemmanager);
            Container.Bind<BaseContextDelegate>().FromInstance(appStartDelegate);
            Container.Bind<ShowAdsManager>().FromInstance(showAdsManager);
            Container.Bind<SoundPalette>().FromInstance(soundPalette);
            Container.Bind<PlayerDataService>().FromInstance(playerDataService);
            Container.Bind<NotificationExample>().FromInstance(notificationExample);
            // Binding all in-game systems to root container of zenject.
            gameSystemmanager.InstallBinding(Container);
        }
    }
}



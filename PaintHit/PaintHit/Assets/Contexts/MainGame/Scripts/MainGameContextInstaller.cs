﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core.SystemContext;
using UnityEngine;
using Zenject;

namespace Assets.Contexts.MainGame.Scripts
{
    public class MainGameContextInstaller : MonoInstaller
	{
		[SerializeField] MainGameContextDelegate _contextDelegate;

		public override void InstallBindings()
		{
			Container.Bind<BaseContextDelegate>().FromInstance(_contextDelegate);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class AlphaCutoffController : MonoBehaviour
{
    public Texture AlphaTex;
    public Color MainColor = Color.white;
    public Material _mainMat;

    [Range(0, 1)]
    public float AlphaValue = 0;

    Material _material;
    int _propertyID;
    int _propertyColorID;
    int _propertyAlphaTexID;

    MeshRenderer _mesh;

#if UNITY_EDITOR
    void OnValidate()
    {
        _mesh = GetComponent<MeshRenderer>();
    }
#endif

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        //_mesh = GetComponent<MeshRenderer>();
    }

    public bool IsClone = true;

    void OnEnable()
    {
        _mesh = GetComponent<MeshRenderer>();
        #if UNITY_EDITOR
        if (IsClone)
        {
            // _material = _mainImage.material = Instantiate(_mainImage.material);
            _material = _mesh.material = Instantiate(_mainMat);
        }
        else
        {
            _material = _mesh.material = _mainMat;
        }
        #else
		
		if(IsClone)
		{
            _material = _mesh.material = Instantiate(_mainMat);
		}
		else
		{
            _material = _mesh.material = _mainMat;
		}
        #endif
		
		_propertyID = Shader.PropertyToID("_AlphaCutoff");
		_propertyColorID = Shader.PropertyToID("_SecondColor");
		_propertyAlphaTexID = Shader.PropertyToID("_SecondTex");
        _mesh.material.SetTexture(_propertyAlphaTexID, AlphaTex);
		Update();
	}

	//void Start () 
	//{
 //       _mesh = GetComponent<MeshRenderer>();
	//	#if UNITY_EDITOR
	//	if(EditorApplication.isPlaying  && IsClone)
	//	{
 //           _material = _mesh.materials[0] = Instantiate(_mainMat);
	//	}
	//	else
	//	{
 //           _material = _mesh.materials[0] = _mainMat;
	//	}
 //       #endif

	//	//#else
	//	if(IsClone)
	//	{
 //           _material = _mesh.materials[0] = Instantiate(_mainMat);
	//	}
	//	else
	//	{
 //           _material = _mesh.materials[0] = _mainMat;
	//	}
	//	_propertyID = Shader.PropertyToID("_AlphaCutoff");
	//	_propertyColorID = Shader.PropertyToID("_SecondColor");
	//	_propertyAlphaTexID = Shader.PropertyToID("_SecondTex");
	//	// _material.SetColor(_propertyColorID, MainColor);
	//	_material.SetTexture(_propertyAlphaTexID, AlphaTex);
	//	Update();
	//}

	
	
	void Update () 
	{
        _mesh.material.SetFloat(_propertyID, AlphaValue);
        _mesh.material.SetColor(_propertyColorID, MainColor);

#if UNITY_EDITOR
        _mesh.material.SetTexture(_propertyAlphaTexID, AlphaTex);
#endif
	}

	
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Zenject;

namespace Assets.Contexts.MainGame.Scripts
{
    public class BulletController : MonoBehaviour {

        private RaycastHit hit;

        private Vector3 pointEnd;

      

        private Vector3 lastFrameVelocity;

        [SerializeField] private Rigidbody rb;

        [SerializeField] private BoxCollider boxCollider;

        [SerializeField]
        [Tooltip("Just for debugging, adds some velocity during OnEnable")]
        private Vector3 initialVelocity;

        [SerializeField] private float minVelocity = 10f;



        [SerializeField] private TrailRenderer ballTrailRenderer;

        [SerializeField] LayerMask _layerMask;

        [SerializeField] MeshRenderer meshRenderer;

        [SerializeField] public ParticleSystem enegryParticleSystem;

        [SerializeField] private Transform raycastStart, raycastEnd;

        [Inject] EventGamePlay eventGamePlay;

        [Inject] PlayerDataService playerDataService;

        private List<ColorInfo> listColor;
		public void OnEnable()
		{
            this.enegryParticleSystem = this.gameObject.GetComponentInChildren<ParticleSystem>();
		}
        public void OnShow(EventGamePlay eventGamePlay , PlayerDataService playerDataService)
        {
            this.rb.useGravity = false;
            this.rb.isKinematic = true;
            rb.velocity = initialVelocity;
            this.boxCollider.isTrigger = true;

             this.eventGamePlay = eventGamePlay;

            this.playerDataService = playerDataService;

            this.listColor = this.playerDataService.GetPlayerData().colorDictionary[this.playerDataService.GetPlayerData().IdColorCurrent];

            this.meshRenderer.material.SetColor("_Color", this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);


            this.eventGamePlay.OnEventEnegry += OnEffectEnegry;
            this.eventGamePlay.OnFpsCircleLevel += OffEffectEnegry;

            SetColorTrailRenderer(this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);
            
        }

        void SetColorTrailRenderer(Color color)
        {
            float alpha = 1.0f;
            Gradient gradient = new Gradient();
            gradient.SetKeys(
                new GradientColorKey[] { new GradientColorKey(color, 0.0f), new GradientColorKey(color, 1.0f) },
                new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
                );
            this.ballTrailRenderer.colorGradient = gradient;
        }
       
        private void Update()
        {
            lastFrameVelocity = rb.velocity;
        }
  
        public void UpdateColor()
        {
            this.meshRenderer.material.SetColor("_Color", this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);
        }

        public void OnHide()
        {
            this.eventGamePlay.OnEventEnegry -= OnEffectEnegry;
            this.eventGamePlay.OnFpsCircleLevel -= OffEffectEnegry;
        }

        private void OnCollisionEnter(Collision collision)
        {
            Bounce(collision.contacts[0].normal);
        }

        private void Bounce(Vector3 collisionNormal)
        {
            if (this.rb.useGravity)
                return;
           
            var speed = lastFrameVelocity.magnitude;
            var direction = Vector3.Reflect(lastFrameVelocity.normalized, collisionNormal);

            this.rb.velocity = direction * Mathf.Max(speed, minVelocity);
            this.rb.useGravity = true;

            this.eventGamePlay.TriggerDie();
            //DOTween.Sequence().AppendInterval(2.0f).OnComplete(() => {
            //    this.eventGamePlay.TriggerDie();
            //}).Play();

        }

        // variable
        private float _quantityCurrent;
        private long _quantityRaise;
        private Tweener _tweenCurrencyRaise;

        private void OnTriggerEnter(Collider other)
        {
          
            if (MainGameSceneController.instance.IsCheckFps)
            {
                var alphaCutoffController = other.gameObject.GetComponent<AlphaCutoffController>();
                if (alphaCutoffController.AlphaValue > 0)
                {
                    this.rb.isKinematic = false;
                    this.boxCollider.isTrigger = false;
                    this.eventGamePlay.Resume();
                    this.eventGamePlay.UpdateUi();
                    MainGameSceneController.instance.IsCheckFps = false;
                }
                else
                {
                    MainGameSceneController.instance.countFps++;
                    if (other.transform.GetChild(0).gameObject.activeSelf == true)
                    {
                        this.playerDataService.UpdateDiamond(1);
                        other.transform.GetChild(0).gameObject.SetActive(false);
                    }

                    this.transform.localScale = new Vector3(0, 0, 0);
                    MainGameUI.instance.objBulletEffect.SetActive(true);
                    MainGameUI.instance.objBulletEffect.transform.position = new Vector3(other.gameObject.transform.position.x, 
                                                                                         MainGameUI.instance.objBulletEffect.transform.position.y,
                                                                                         MainGameUI.instance.objBulletEffect.transform.position.z);

                    MainGameUI.instance.bulletParticleSystem_02.Play();

                    //MainGameUI.instance.bulletEffectMaterial_01.SetColor("_TintColor", this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);
                    //MainGameUI.instance.bulletEffectMaterial_02.SetColor("_TintColor", this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);

                    _quantityCurrent = 0.8f;
                    alphaCutoffController.MainColor = this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors;

                    alphaCutoffController.AlphaValue = _quantityCurrent;
                    _tweenCurrencyRaise = DOTween.To(() => _quantityCurrent, v =>
                    {
                        alphaCutoffController.AlphaValue = v;
                    }, 1f, 0.3f)
                        .SetEase(Ease.InFlash)
                        .OnComplete(() =>
                        {
                            MainGameUI.instance.bulletParticleSystem_02.Stop();
                            alphaCutoffController.AlphaValue = 1f;
                        });
                   
                    //MainGameUI.instance.objBulletEffect.SetActive(false);
                    this.eventGamePlay.Resume();
                    this.eventGamePlay.UpdateUi();
                    this.eventGamePlay.FpsSucess();
                    MainGameSceneController.instance.IsCheckFps = false;
                    this.gameObject.SetActive(false);


                  
                }
            }
        }

        private void OnEffectEnegry()
        {
            if (this.enegryParticleSystem == null)
                return;
            
            this.enegryParticleSystem.gameObject.SetActive(true);

            this.enegryParticleSystem.Play();

            Sequence sequence = DOTween.Sequence();
            sequence.AppendInterval(4.0f).OnComplete(() => {
                sequence.Kill();
                this.enegryParticleSystem.gameObject.SetActive(false);
            }).Play();
        }

        private void OffEffectEnegry()
        {
            if (this.enegryParticleSystem == null)
                return;

            if(this.enegryParticleSystem.isPlaying)
            {
                this.enegryParticleSystem.gameObject.SetActive(false);
            }
           
        }
    }
}

﻿using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;
using DG.Tweening;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
namespace Assets.Contexts.MainGame.Scripts
{
    public class MainGameUI : MonoBehaviour
    {

        public static MainGameUI instance;

        public static MainGameUI Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new MainGameUI();
                }
                return instance;
            }
        }


        [Inject] GameHub gameHub;

        [Inject] GameData gameData;

        [Inject] SoundPalette soundPalette;

        [Inject] ShowAdsManager showAdsManager;

        [Inject] EventGamePlay eventGamePlay;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] Text textDiamond;

        [SerializeField] public Text textScore;

        [SerializeField] private Text textEnegryNumber;

        [SerializeField] private Image[] iconHearts;

        [SerializeField] public Transform parentIconBullet;

        [SerializeField] private PopupEndGame popupEndGame;

        [SerializeField] private PopupLose popupLose;

        [SerializeField] private PopupContinue popupContinue;


        [SerializeField] private IconBulletController prefabIconBullet;

        private float counter;

        private float dist;

        private float lineDrawSpeed = 6f;

        private Vector3 pointAlongLine;


        [SerializeField] public Material bulletEffectMaterial_01;

        [SerializeField] public Material bulletEffectMaterial_02;

        [SerializeField] public GameObject objBulletEffect;


        //[SerializeField] public ParticleSystem bulletParticleSystem_01;

        [SerializeField] public ParticleSystem bulletParticleSystem_02;


        public Dictionary<int, IconBulletController> IconBulletDic = new Dictionary<int, IconBulletController>();

        private List<ColorInfo> listColor;

        [SerializeField] private Vector3 positionBeginFly;

        [SerializeField] private Vector3 positionEndFly;

        [SerializeField] private SpriteRenderer backgroundImage;

        [SerializeField] private Sprite[] backgroundGame;

        [SerializeField] ShopController shopController;

        [SerializeField] private RateUsLayerCtrl rateUsLayerCtrl;

        private void Awake()
        {
            this.backgroundImage.sprite = this.backgroundGame[Random.Range(0, this.backgroundGame.Length - 1)];
            // Check is Ipad
#if UNITY_IOS
            UnityEngine.iOS.DeviceGeneration gen = UnityEngine.iOS.Device.generation;

            if (gen == UnityEngine.iOS.DeviceGeneration.iPad1Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPad2Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPad3Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPad4Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadAir1 ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadAir2 ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadMini1Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadMini2Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadMini3Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadMini4Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadPro1Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadPro10Inch1Gen ||
                gen == UnityEngine.iOS.DeviceGeneration.iPadUnknown)
            {
                Screen.autorotateToPortrait = true;
                Screen.autorotateToPortraitUpsideDown = true;
                Screen.orientation = ScreenOrientation.AutoRotation;
            }
#endif
		}
		public void OnEnable()
		{
            instance = this;

            this.eventGamePlay.OnTriggerDie += OnDie;
            this.eventGamePlay.OnUpdateUi += UpdateUi;
            this.eventGamePlay.OnFpsSuccess += OnFpsSuccess;
            this.playerDataService.OnUpdateDiamond += UpdateUi;

            for (var i = 0; i < MainGameSceneController.instance.HeartInLevel; i++)
            {
                this.iconHearts[i].DOFade(1, 0.1f).Play();
            }
            Debug.LogError("==> diamond 1 " + this.playerDataService.GetPlayerData().diamond);
            this.textDiamond.text = string.Format("{0}", this.playerDataService.GetPlayerData().diamond);
        }

		public void OnDisable()
		{
            this.eventGamePlay.OnTriggerDie -= OnDie;
            this.eventGamePlay.OnUpdateUi -= UpdateUi;
            this.eventGamePlay.OnFpsSuccess -= OnFpsSuccess;
            this.playerDataService.OnUpdateDiamond -= UpdateUi;
		}

        public void InitLevelCircle(ItemInfor itemInfor)
        {
            this.IconBulletDic.Clear();
            for (var i = 0; i < itemInfor.EnemLevelBullety;i++)
            {
                GameObject iconBullet = GameObject.Instantiate(this.prefabIconBullet.gameObject, this.parentIconBullet);

                IconBulletController iconBulletController = iconBullet.GetComponent<IconBulletController>();
                iconBulletController.OnActive(true , this.playerDataService);
                this.IconBulletDic.Add(i, iconBulletController);

            }
            this.textEnegryNumber.text = string.Format("{0}", itemInfor.EnemLevelBullety);
            InitEffectBullet();
        }

        public void CompleteLevelCircle()
        {
            this.IconBulletDic.Clear();
            foreach(Transform icon in this.parentIconBullet)
            {
                GameObject.Destroy(icon.gameObject);
            }
        }

        private void OnFpsSuccess()
        {
            foreach(var icon in this.IconBulletDic)
            {
                icon.Value.OnActive(false , this.playerDataService);
                this.IconBulletDic.Remove(icon.Key);
                return;
            }
        }

        public void CompleteLevel()
        {
            this.popupEndGame.gameObject.SetActive(true);

            // check level boss
            if (this.playerDataService.GetPlayerData().level % this.gameData.MainGameData.levelBoss == 0)
            {
                this.popupEndGame.OnShow(true , 2);
            }else{
                this.popupEndGame.OnShow();
            }
        }

        private void InitEffectBullet()
        {

            //var materialRenderer_01 = this.objBulletEffect.GetComponentInChildren<ParticleSystemRenderer>();

            //var materialRenderer_02 = materialRenderer_01.GetComponentInChildren<ParticleSystemRenderer>();

            //this.bulletEffectMaterial_01 = materialRenderer_01.material;

            //this.bulletEffectMaterial_02 = materialRenderer_02.material;

            //var main_01 = this.bulletParticleSystem_01.main;

            //this.listColor = this.playerDataService.GetPlayerData().colorDictionary[this.playerDataService.GetPlayerData().IdColorCurrent];

            //main_01.startColor = new ParticleSystem.MinMaxGradient(this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors , this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);

            var main_02 = this.bulletParticleSystem_02.main;

            this.listColor = this.playerDataService.GetPlayerData().colorDictionary[this.playerDataService.GetPlayerData().IdColorCurrent];


            main_02.startColor = new ParticleSystem.MinMaxGradient(this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors, this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors);

            this.objBulletEffect.SetActive(true);
        }

        public void OnDie()
        {
            MainGameSceneController.instance.HeartInLevel--;
           
            if(MainGameSceneController.instance.HeartInLevel <= 0)
            {
                this.showAdsManager.showInterstitialAd();
                this.popupLose.gameObject.SetActive(true);
                this.popupLose.OnShow();
            }
            else{
                this.iconHearts[MainGameSceneController.instance.HeartInLevel].DOFade(0.25f, 0.3f).Play();
            }
                
           
        }

        public void UpdateUi()
        {
            Debug.LogError("==> diamond 2 " + this.playerDataService.GetPlayerData().diamond);
            this.textScore.text = string.Format("{0} / {1}", MainGameSceneController.instance.RoundCurrent,MainGameSceneController.instance.infoCircle.NumberCircle);    
            this.textDiamond.text = string.Format("{0}", this.playerDataService.GetPlayerData().diamond);
        }

        public void OnShowRateUS()
        {
            this.rateUsLayerCtrl.gameObject.SetActive(true);
        }
#region button on action

		public void OnClickSetting()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            MainGameSceneController.Instance.OnDisableTap();
            this.popupContinue.gameObject.SetActive(true);
            this.popupContinue.OnShow();
        }

        public void OnClickStore()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            //this.shopController.gameObject.SetActive(true);
            //this.shopController.OnShow();
        }

        private Sequence sequence = null;
        public void OnClickEnegry()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            this.eventGamePlay.Enegry();

            MainGameSceneController.Instance.circleCurrent.itemInfor.Speed = ((float)MainGameSceneController.Instance.circleCurrent.itemInfor.Speed / (float)3f);

            if(this.sequence != null)
            {
                this.sequence.Kill();
            }
            this.sequence = DOTween.Sequence();
            this.sequence.AppendInterval(3.0f).OnComplete(()=>{
                this.sequence.Kill();
                MainGameSceneController.Instance.circleCurrent.itemInfor.Speed = ((float)MainGameSceneController.Instance.circleCurrent.itemInfor.Speed * (float)3f);

            }).Play();
        }
#endregion


    }
}
﻿using Assets.Contexts.MainGame.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Contexts.MainGame.Scripts
{
    public class BulletPool : MonoBehaviour
    {
        public List<BulletController> PoolbjectList = new List<BulletController>();
        public void InStance(GameObject obj, int size, Transform parent)
        {
            for (var i = 0; i < size; i++)
            {
                GameObject objectNew = GameObject.Instantiate(obj);

                BulletController bulletController = objectNew.GetComponent<BulletController>();

                if (bulletController == null)
                {
                    bulletController = objectNew.AddComponent<BulletController>();
                }
                bulletController.gameObject.SetActive(false);
                bulletController.transform.SetParent(parent);

                PoolbjectList.Add(bulletController);
            }
        }

        public BulletController GetObject()
        {
            for (var i = 0; i < PoolbjectList.Count; i++)
            {
                if (!PoolbjectList[i].gameObject.activeInHierarchy)
                {
                    PoolbjectList[i].gameObject.SetActive(true);
                    PoolbjectList[i].transform.localScale = new Vector3(0.35f, 0.35f, 0.35f);
                    return PoolbjectList[i];
                }
            }

            return null;
        }

        public List<BulletController> GetBulletControllerList()
        {
            return PoolbjectList;
        }

        public void Clear()
        {
            for (var i = PoolbjectList.Count - 1; i > 0; i--)
            {
                BulletController bulletController = PoolbjectList[i];
                if (bulletController)
                {
#if UNITY_EDITOR
                    GameObject.Destroy(bulletController.gameObject);
#else
                      GameObject.Destroy(bulletController.gameObject);
#endif
                }
            }
            PoolbjectList.Clear();
        }
    }
}

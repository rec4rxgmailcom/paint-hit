﻿using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using Zenject;
using DG.Tweening;
using UnityEngine.UI;

namespace Assets.Contexts.MainGame.Scripts
{
    public class PopupLose : MonoBehaviour
    {


        public static PopupLose instance;

        public static PopupLose Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new PopupLose();
                }
                return instance;
            }
        }

        [Inject] GameHub gameHub;

		private void OnEnable()
		{
            MainGameSceneController.instance.pressGesture.enabled = false;
		}

		private void OnDisable()
		{
            if (MainGameSceneController.instance.pressGesture == null)
                return;
            
            MainGameSceneController.instance.pressGesture.enabled = true;
		}
		public void OnShow()
        {
            instance = this;
        }

        public void OnClickRestart()
        {
            this.gameObject.SetActive(false);
            this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
        }

        public void OnClickHome()
        {
            this.gameHub.LoadContext(GameHub.ContextName.MainMenuContext);
        }
    }
}

﻿
using Zenject;
using UnityEngine;
using DG.Tweening;
using Assets.Scripts.Core;
using Assets.Scripts.InGame.Controllers;
using Assets.Contexts.BaseScript;
using TouchScript.Gestures;
using System.Collections.Generic;
using System;
using System.Collections;


namespace Assets.Contexts.MainGame.Scripts
{
    public class MainGameSceneController : Controller
    {
        [Inject] GameHub gameHub;

        public static MainGameSceneController instance;

        public static MainGameSceneController Instance{

            get{
                if(instance == null)
                {
                    instance = new MainGameSceneController(); 
                }
                return instance;
            }
        }

        [SerializeField] public Transform pointTarget = null;

        [SerializeField] public Transform pointBullet = null;

        [SerializeField] public Transform pointBulletInit = null;

        [SerializeField] GameObject parentBullet = null;

        [SerializeField] GameObject prefabBullet = null;

        [SerializeField] CircleRoot parentCircle = null;

        [SerializeField] GameObject prefabCircle = null;

        [SerializeField] Transform positionCircleInit = null;

        [SerializeField] private ParticleSystem particleSystemVictory = null;

        [SerializeField] public PressGesture pressGesture;


        [SerializeField] private GameObject[] circleModel; 


        public ParticleSystem _ringParticleSystem;

        private BulletPool bulletPool = new BulletPool();

        private CirclePool circlePool = new CirclePool();

        public BulletController bulletCurrent;

        public CircleController circleCurrent;

        private Dictionary< int,InfoCircleDiction> circleCompleteDic = new Dictionary< int,InfoCircleDiction>();


        [Inject] GameData gameData;

        [Inject] SoundPalette soundPalette;

        [Inject] EventGamePlay eventGamePlay;

        [SerializeField] private MainGameUI mainGameUI;
        //test
        public int countFps = 0;

        public int LevelCurrent;

        public int RoundCurrent;

        public int HeartInLevel;

        public bool IsCheckFps = false;

        public ItemInfor ItemInforCurrent;

        public InfoCircle infoCircle;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] GenerationLevel generationLevel;

        [SerializeField] private PopupReward popupReward;


        bool _canTouch;

        public void Awake()
        {
            instance = this;

            this.pressGesture.enabled = false;

            this.HeartInLevel = 3;

            this.LevelCurrent = this.playerDataService.GetPlayerData().level;

            if (this.playerDataService.GetPlayerData().ItemQuestInfo.LevelQuest > 0)
            {
                this.LevelCurrent = this.playerDataService.GetPlayerData().ItemQuestInfo.LevelQuest;
            }



            this.RoundCurrent = 0;

            int idModel = this.playerDataService.GetPlayerData().idCircleSelect;

            this.prefabCircle = this.circleModel[idModel];

            this.bulletPool.InStance(this.prefabBullet, 10, this.parentBullet.transform);
            this.circlePool.InStance(this.prefabCircle, 10, this.parentCircle.transform);


            var speed = gameData.MainGameData.ListLevels[this.LevelCurrent].ItemInfors[this.RoundCurrent].Speed;

            // check level boss
            if (this.playerDataService.GetPlayerData().level % this.gameData.MainGameData.levelBoss == 0)
            {
                this.infoCircle = this.generationLevel.GetLevelBoss(this.LevelCurrent, speed);
            }
            else
            {
                this.infoCircle = this.generationLevel.GetLevelInfo(this.LevelCurrent, speed);
            }

            this.playerDataService.GetPlayerData().totalPlayGame++;

            StartGame();
        }

      
		public void StartGame()
		{
            _canTouch = true;
            this.playerDataService.GetPlayerData().IsPlay = true;
            this.mainGameUI.textScore.text =string.Format("0 / {0}", this.infoCircle.NumberCircle);
            InitLevelCircle(positionCircleInit.localPosition, true, null);
            InitBullet();
           
		}

        public void OnResume()
        {
            StopCoroutine(OnEnableTap(0));
            StartCoroutine(OnEnableTap(0.3f));
            this.playerDataService.GetPlayerData().IsPlay = true;
            this.circleCurrent.OnResume();
        }
		private void OnEnable()
        {
            this.pressGesture.Pressed += OnTapAction;
            this.eventGamePlay.OnTriggerDie += OnDieCircle;
            this.eventGamePlay.OnEventResume += CheckCompleteRound;
        }

        public void OnDisable()
        {
            this.playerDataService.SaveGame(this.playerDataService.GetPlayerData());
            this.pressGesture.Pressed -= OnTapAction;
            this.eventGamePlay.OnTriggerDie -= OnDieCircle;
            this.eventGamePlay.OnEventResume -= CheckCompleteRound;
        }

        private void OnTapAction(object sender, System.EventArgs e)
        {
            

            if (!this.pressGesture.enabled)
                return;

            if (IsCheckFps || this.bulletCurrent == null)
                return;

           
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.HIT1);

            this.bulletCurrent.transform.position = this.pointBullet.position;

            FpsCircleLevel();
           
            this.IsCheckFps = true;
           
            this.eventGamePlay.FpsCircleLevel();
        }


        public void InitLevelCircle(Vector3 targetPos, bool isUseBouce, Action onInitCompleted)
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.NEWCIRCLE);
            this.mainGameUI.textScore.text =string.Format("{0} / {1}", this.RoundCurrent, this.infoCircle.NumberCircle);    
            this.ItemInforCurrent = gameData.MainGameData.ListLevels[this.LevelCurrent].ItemInfors[this.RoundCurrent];
            this.ItemInforCurrent.EnemLevelBullety = this.infoCircle.NumberBullet;
            CircleController circleController = this.circlePool.GetObject();
           

            Vector3 initPos = targetPos;
            initPos.y = 7;
            circleController.transform.localPosition = initPos;
            circleController.MoveTo(targetPos, isUseBouce, onInitCompleted);
            circleController.OnShow(this.ItemInforCurrent, this.infoCircle , this.eventGamePlay , this.gameData , this.playerDataService);
            circleController.OnEventShake = () => {
                if(_canTouch)
                    parentCircle.HandleShake();
            };

            InfoCircleDiction infoCircleDiction = new InfoCircleDiction();
            infoCircleDiction.IsComplete = false;
            infoCircleDiction.GameObject = circleController.gameObject;

            if(!this.circleCompleteDic.ContainsKey(this.RoundCurrent)){
                this.circleCompleteDic.Add(this.RoundCurrent, infoCircleDiction);
            }
          
            this.circleCurrent = circleController;
        
            this.mainGameUI.InitLevelCircle(this.ItemInforCurrent);

            this.pressGesture.enabled = true;
        }
        
        public void InitBullet()
        {
            this.bulletCurrent = this.bulletPool.GetObject();
            this.bulletCurrent.OnShow(this.eventGamePlay , this.playerDataService);
            this.bulletCurrent.gameObject.transform.position = this.pointBulletInit.position;
            this.bulletCurrent.transform.DOMove(this.pointBullet.position, 0.2f).Play();
        }

        public void FpsCircleLevel()
        {
           
            //this.countFps++;

            BulletController bullet = this.bulletCurrent;
            bullet.transform.position = this.pointBullet.position;
            var sequence = DOTween.Sequence();

            bullet.gameObject.transform.DOMove(this.pointTarget.position, 0.2f).Play();
            sequence.AppendInterval(0.2f).OnComplete(()=> {
                sequence.Kill();
                this.circleCurrent.AnimFpsShake();
            }).Play();
        }
   
        private void CheckCompleteRound()
        {
            InitBullet();
            if(this.countFps >= this.infoCircle.NumberBullet)
            {
                this.countFps = 0;
                this.pressGesture.enabled = false;
                var sequence = DOTween.Sequence();

                this.bulletCurrent.gameObject.SetActive(false);
                MainGameUI.instance.parentIconBullet.gameObject.SetActive(false);

                sequence.AppendInterval(0.6f).OnComplete(()=>{
                    sequence.Kill();
                    this.pressGesture.enabled = true;
                    this.bulletCurrent.gameObject.SetActive(true);
                    MainGameUI.instance.parentIconBullet.gameObject.SetActive(true);
                    this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.FINISHCIRCLE);
                    OnCompleteRound();

                }).Play();
               
            }
        }


        public void OnCompleteRound()
        {
            Vector3 previousPos = circleCurrent.transform.localPosition;
            this.ItemInforCurrent.IsComplete = true;
            

                previousPos.y += 0.95f;
                OnCompleteColorCircle();
                MainGameUI.instance.CompleteLevelCircle();
                this.RoundCurrent++;

                //CreateNewRound(previousPos);
          
            // Check complete level
            if (this.RoundCurrent >= this.infoCircle.NumberCircle)
            {
                this.bulletCurrent.gameObject.SetActive(false);
                this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.LEVELUP);
                CompleteLevel();

                // play effect complete level
                return;
            }

            CreateNewRound(previousPos);
        }

        void CreateNewRound(Vector3 previousPos)
        {
            Sequence sequenceDelay = DOTween.Sequence();

            sequenceDelay.AppendInterval(0.3f).OnComplete(()=>{
             
                InitLevelCircle(previousPos, false, OnMoveCircleCompleted);

                // update color bullet in pool
                UpdateColorBullet();

                sequenceDelay.Kill();

               
            }).Play();

        }

        void OnMoveCircleCompleted()
        {
            _ringParticleSystem.Play();
            for (var i = 0; i < this.circleCompleteDic.Count;i++)
            {
                GameObject objCircle = this.circleCompleteDic[i].GameObject;
                float y = objCircle.transform.localPosition.y - 1.0f;
                objCircle.transform.DOLocalMoveY(y, 0.2f);
                this.eventGamePlay.CompleteCircleLevel();
            }
            
            parentCircle.HandleBounce();
        }

        void OnCompleteColorCircle()
        {
            for (var i = 0; i < this.circleCompleteDic.Count;i++)
            {
                GameObject objCircle = this.circleCompleteDic[i].GameObject;
                bool isComplete = objCircle.GetComponent<CircleController>().itemInfor.IsComplete; 
                if(!this.circleCompleteDic[i].IsComplete)
                {
                    objCircle.GetComponent<CircleController>().OnCompleteCircle(); 
                }
               
                this.circleCompleteDic[i].IsComplete = true;
            }

        }

   
        private void UpdateColorBullet()
        {
            foreach(var bullet in this.bulletPool.GetBulletControllerList())
            {
                bullet.UpdateColor();
            }
        }
        public void CompleteLevel()
        {
            _canTouch = false;
            OnCompleteStage();
            MainGameUI.Instance.UpdateUi();
            this.circleCompleteDic.Clear();
            this.particleSystemVictory.Play();

            var sequense = DOTween.Sequence();
            sequense.AppendInterval(3.0f).OnComplete(()=> {
                sequense.Kill();
                this.LevelCurrent++;
                this.playerDataService.GetPlayerData().level = this.LevelCurrent;


                if (this.playerDataService.GetPlayerData().level % 5 == 0)
                {
                    // check reward
                    CheckRewardLevel();
                }
                else{
                    if (this.playerDataService.GetPlayerData().totalPlayGame % 5 == 0)
                    {
                        CheckRateUS();
                    }
                    else
                    {
                        MainGameUI.instance.CompleteLevel();
                    }
                   
                }

               // complete challenge
                if(this.LevelCurrent-1 == this.playerDataService.GetPlayerData().ItemQuestInfo.LevelQuest )
                {
                    this.playerDataService.GetPlayerData().ItemQuestInfo.Status = 1;
                }
              
                this.playerDataService.SaveGame(this.playerDataService.GetPlayerData());
            });
        }

        private void OnCompleteStage()
        {
            if (this.playerDataService.GetPlayerData().challengeList.ContainsKey(this.LevelCurrent))
            {
                this.playerDataService.GetPlayerData().diamond +=
                       this.playerDataService.GetPlayerData().challengeList[this.LevelCurrent].currentChallengeValue;
                this.playerDataService.GetPlayerData().challengeList.Remove(this.LevelCurrent);
            }

            if(this.playerDataService.GetPlayerData().stageUnClockDictionary.ContainsKey(this.LevelCurrent))
            {
                this.playerDataService.GetPlayerData().stageUnClockDictionary[this.LevelCurrent] = true;
            }

        }
        public void OnRestart()
        {
            this.countFps = 0;
            this.RoundCurrent = 0;
            this.bulletCurrent.gameObject.transform.position = this.pointBullet.position;
            this.circleCompleteDic.Clear();
            MainGameUI.instance.CompleteLevelCircle();
            ResetGamePlay();
            StartGame();

            StopCoroutine(OnEnableTap(0));
            StartCoroutine(OnEnableTap(0.3f));
                   
        }

        public IEnumerator OnEnableTap(float time)
        {
            yield return new WaitForSeconds(time);
            this.pressGesture.enabled = true;
        }
        public void OnDieCircle()
        {
            if(this.HeartInLevel <= 0)
            {
                this.pressGesture.enabled = false;
            }
         
          
        }

        public void OnDisableTap()
        {
            this.pressGesture.enabled = false;  
        }
        public void ResetGamePlay()
        {
            _canTouch = true;
            foreach(Transform bullet in this.parentBullet.transform)
            {
                bullet.gameObject.SetActive(false);
            }
            foreach (Transform circle in this.parentCircle.transform)
            {
                circle.gameObject.SetActive(false);
            }
        }

        private void CheckRewardLevel()
        {
                this.pressGesture.enabled = false; 
                this.popupReward.gameObject.SetActive(true);

                TypeReward type = (TypeReward)UnityEngine.Random.Range(0, 3);
                this.popupReward.OnShow(type);
           
        }

        private void CheckRateUS()
        {
            this.pressGesture.enabled = false;
            MainGameUI.instance.OnShowRateUS();
        }
    }


}
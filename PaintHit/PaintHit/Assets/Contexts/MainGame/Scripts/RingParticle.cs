﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Contexts.MainGame.Scripts
{
	public class RingParticle : MonoBehaviour 
	{
		[SerializeField] CircleController _circelController;
		ParticleSystem _particle;
		// CircleControl
		void Awake()
		{
			// _circelController = GetComponent<CircleController>();
			_particle = GetComponent<ParticleSystem>();
		}

		public void OnParticleSystemStopped()
		{
			// _circelController.StartRotate = true;
			_particle.gameObject.SetActive(false);
		}
	}

}


﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.InGame.Controllers;
using Assets.Contexts.MainMenu.Scripts;

namespace Assets.Contexts.MainGame.Scripts
{
    public class RateUsLayerCtrl : MonoBehaviour
    {

        public static RateUsLayerCtrl instance = null;

        public static RateUsLayerCtrl Instance
        {
            get
            {
                return instance;
            }
        }

        [SerializeField] private Image background;


        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
        public void OnEnable()
        {
            this.gameObject.transform.localPosition =
                new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, -3);

        }


        public void OnDisable()
        {
            if (MainGameUI.instance != null)
            {
                MainGameUI.instance.CompleteLevel();
                MainGameSceneController.instance.pressGesture.enabled = true;
            }
           
            if(MainMenuUi.instance != null)
            {
                MainMenuUi.instance.OnClickHome();
            }
        }

        public void OnClickRateNow()
        {
            PlayerPrefs.SetInt("RATEUS", 1);
            PlayerPrefs.Save();
            Application.OpenURL("https://play.google.com/store?hl=en");
            this.gameObject.SetActive(false);

        }

        public void OnClickLater()
        {
            this.gameObject.SetActive(false);
        }
    }
}
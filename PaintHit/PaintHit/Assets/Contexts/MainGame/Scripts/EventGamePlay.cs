﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Contexts.MainGame.Scripts
{
    public class EventGamePlay : MonoBehaviour
    {

        public delegate void EventFpsCircleLevel();

        public event EventFpsCircleLevel OnFpsCircleLevel;

        public delegate void EventCompleteCircleLevel();

        public event EventCompleteCircleLevel OnCompleteCircleLevel;

        public delegate void EventTriggerDieCircle();

        public event EventTriggerDieCircle OnTriggerDie;

        public delegate void EventEnegry();

        public event EventEnegry OnEventEnegry;


        public delegate void EventStop();

        public event EventStop OnEventStop;

        public delegate void EventResume();

        public event EventResume OnEventResume;

        public delegate void EventUpdateUi();

        public event EventUpdateUi OnUpdateUi;

        public delegate void EventFpsSuccess();

        public event EventFpsSuccess OnFpsSuccess;

        public void FpsCircleLevel()
        {
            if(OnFpsCircleLevel !=null)
            {
                OnFpsCircleLevel();
            }
        }

        public void CompleteCircleLevel()
        {
            if (OnCompleteCircleLevel != null)
            {
                OnCompleteCircleLevel();
            }
        }

        public void TriggerDie()
        {
            if(OnTriggerDie != null)
            {
                OnTriggerDie();
            }
        }

        public void Enegry()
        {
            if (OnEventEnegry != null)
            {
                OnEventEnegry();
            }
        }

        public void Stop()
        {
            if (OnEventStop != null)
            {
                OnEventStop();
            }
        }

        public void Resume()
        {
            if (OnEventResume != null)
            {
                OnEventResume();
            }
        }

        public void UpdateUi()
        {
            if(OnUpdateUi != null)
            {
                OnUpdateUi();
            }
        }

        public void FpsSucess()
        {
            if (OnFpsSuccess != null)
            {
                OnFpsSuccess();
            }
        }

    }
}

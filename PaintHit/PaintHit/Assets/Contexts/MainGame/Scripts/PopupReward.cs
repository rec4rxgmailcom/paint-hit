﻿using System.Collections;
using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using Zenject;
using DG.Tweening;
using UnityEngine.UI;

namespace Assets.Contexts.MainGame.Scripts
{
    public class PopupReward : MonoBehaviour
    {
        public static PopupReward instance;

        public static PopupReward Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new PopupReward();
                }
                return instance;
            }
        }

        [SerializeField] private Text txtContent;

        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Animator animator;
        private void OnEnable()
        {
            this.animator.Play("AroundLight");
            MainGameSceneController.instance.pressGesture.enabled = false;
        }

        private void OnDisable()
        {
            if (MainGameSceneController.instance.pressGesture == null)
                return;
            
            MainGameSceneController.instance.pressGesture.enabled = true;
        }

        public void OnShow(TypeReward type)
        {
            instance = this;

            switch(type)
            {
                case TypeReward.Color:
                    RewardColor();
                    break;
                case TypeReward.Circle:
                    RewardCircle();
                    break;
                case TypeReward.Diamond:
                    RewardDiamond();
                    break;
                case TypeReward.SlowMotion:
                    RewardSlowMotion();
                    break;
            }
        }

        private void RewardColor()
        {
            int idColor = 0;
            for (var i = 0; i < this.playerDataService.GetPlayerData().colorUnlock.Count;i++)
            {
                if(!this.playerDataService.GetPlayerData().colorUnlock[i])
                {
                    idColor = i;
                    this.playerDataService.GetPlayerData().colorUnlock[i] = true;
                    break;
                }
            }

            if(idColor != 0)
            {
                this.txtContent.text = string.Format("Color id {0}", idColor);
            }else{
                RewardDiamond();
            }
           
        }

        private void RewardCircle()
        {
            int idCircle = 0;
            for (var i = 0; i < this.playerDataService.GetPlayerData().circleUnlock.Count; i++)
            {
                if (!this.playerDataService.GetPlayerData().circleUnlock[i])
                {
                    idCircle = i;
                    this.playerDataService.GetPlayerData().circleUnlock[i] = true;
                    break;
                }
            }

            if(idCircle != 0)
            {
                this.txtContent.text = string.Format("Circle id {0}", idCircle);
            }
            else{
                RewardDiamond();
            }
        }

        private void RewardDiamond()
        {
            this.playerDataService.UpdateDiamond(100);
            this.txtContent.text = string.Format("{0} Diamond", 100); 
        }

        private void RewardSlowMotion()
        {
            this.playerDataService.GetPlayerData().IsSlowMotion = true;

            this.txtContent.text = string.Format("Slow motion Item");
        }
        public void OnClickGetReward()
        {
            this.gameObject.SetActive(false);
            MainGameUI.instance.CompleteLevel();
                   
        }
    }
}
﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircleRoot : MonoBehaviour
{
    public float shakeMultiplier = 1.0f;
    public float shakeTimeScale = 1.0f;

    public float baseShakeDuration = 1.0f;
    public float baseShakeStrength = 0.1f;
    public int baseShakeVibrato = 10;

    public void HandleShake()
    {
        transform.DOLocalMoveZ(0.3f, 0.1f)
            .SetEase(Ease.OutSine)
            .OnComplete(() => {
                transform.DOLocalMoveZ(0f, 0.1f).SetEase(Ease.InSine)
                            .OnComplete(() => {
                                transform.localPosition = Vector3.zero;
                            });
            });
    }

    public void HandleBounce()
    {
        transform.DOLocalMoveY(-0.3f, 0.15f)
            .SetEase(Ease.OutSine)
            .OnComplete(() => {
                transform.DOLocalMoveY(0f, 0.2f).SetEase(Ease.InSine)
                            .OnComplete(() => {
                                transform.localPosition = Vector3.zero;
                            });
            });

        
    }
}

﻿using Assets.Contexts.BaseScript;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using DG.Tweening;
using System;

namespace Assets.Contexts.MainGame.Scripts
{

    public enum CircleState
    {
        RotateIncrease = 0,
        RotateDecrease,
        Stay
    }

    public class CircleController : MonoBehaviour
    {
        public ItemInfor itemInfor;

        [SerializeField] GameData gameData;

        [SerializeField] GameObject pointRoot = null;

        [SerializeField] public GameObject[] iconDiamond;

        [SerializeField] public AlphaCutoffController[] materialsSegment;

        [SerializeField] CircleData RotateDataConfig;
        [SerializeField] CircleData StayDataConfig;
        // [SerializeField] ParticleSystem _particle;

        CircleState _curState;
       
        private Material materialSegmentCurrent;
        
        [Inject] EventGamePlay eventGamePlay;

        [SerializeField] private ParticleSystem auraCompleteParticle;


        public Action OnEventShake;

        // public bool StartRotate;

        float _maxAngle;
        float _curTime;
        float _executeTime;
        float _deltaAngle;
        float _curAngle;

        private List<ColorInfo> listColor;

        private PlayerDataService playerDataService;

		public void Awake()
		{
            for (var i = 0; i < this.iconDiamond.Length;i++)
            {
                this.materialsSegment[i] = this.iconDiamond[i].GetComponentInParent<AlphaCutoffController>();
            }
		}

		public void OnShow(ItemInfor itemInfor,InfoCircle infoCircle, EventGamePlay eventGamePlay , GameData gameData , PlayerDataService playerDataService)
        {
          
            _curAngle = 0;
            this.itemInfor = itemInfor;

            this.itemInfor.IsComplete = false;

            this.itemInfor.Speed = infoCircle.SpeedRotation;

            this.gameData = gameData;
            this.eventGamePlay = eventGamePlay;

            this.playerDataService = playerDataService;

            this.eventGamePlay.OnEventResume += OnResume;

            for (var i = 0; i < this.materialsSegment.Length; i++)
            {
                //Material material = this.materialsSegment[i].AlphaValue;
                this.materialsSegment[i].MainColor = this.itemInfor.ColorCircle;

                BoxCollider boxCollider = this.materialsSegment[i].GetComponent<BoxCollider>();
                boxCollider.enabled = true;
            }

           
            SetUpCircleData(RotateDataConfig);
            _curState = CircleState.RotateIncrease;


            this.listColor = this.playerDataService.GetPlayerData().colorDictionary[this.playerDataService.GetPlayerData().IdColorCurrent];

            List<int> idRamdon = new List<int> { 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
         
            for (var i = 0; i < infoCircle.blockColor; i++)
            {
                var pos = UnityEngine.Random.Range(0, idRamdon.Count);
             
                this.materialsSegment[idRamdon[pos]].MainColor = this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors;
                this.materialsSegment[idRamdon[pos]].AlphaValue = 1;
                idRamdon.RemoveAt(pos);
            }


            // init diamond bonus
            SetupDiamondBonus();
        }

        public void OnHide()
        {
            this.eventGamePlay.OnEventResume -= OnResume;
        }

        void SetUpCircleData(CircleData stateData)
        {
            _executeTime = UnityEngine.Random.Range(stateData.MinRemainRotateTime, stateData.MaxRemainRotateTime);
            _maxAngle =  UnityEngine.Random.Range(stateData.MinAngle, stateData.MaxAngle);
            _deltaAngle = UnityEngine.Random.Range(stateData.MinDeltaAngle, stateData.MaxDeltaAngle);
            var turning = UnityEngine.Random.Range(-10, 10) > 0 ? 1 : -1;
            _deltaAngle *= turning;
            _maxAngle *= turning;

          
        }

        void SetupDiamondBonus()
        {
            for (var p = 0; p < this.iconDiamond.Length;p++ )
            {
                if (this.iconDiamond[p].activeInHierarchy)
                    this.iconDiamond[p].SetActive(false);
            }
            for (var i = 0; i < this.itemInfor.IsBoDiamondnus;i++)
            {
                var pos = UnityEngine.Random.Range(0, 19);
                this.iconDiamond[pos].SetActive(true);
            }
        }
        public void Update()
        {
            if (this.itemInfor.IsComplete)
            {
                ChangeState(CircleState.Stay);
                OnStop();
                return;
            }
              
            
            if(!this.playerDataService.GetPlayerData().IsPlay)
            {
                OnStop();
                return;
            } 

            
            if (!this.itemInfor.IsComplete)
            {
                switch (_curState)
                {
                    case CircleState.RotateIncrease:
                        if((_deltaAngle < 0 && _maxAngle < _curAngle)
                            || (_deltaAngle > 0 && _curAngle < _maxAngle))
                        {
                            _curAngle += _deltaAngle * Time.deltaTime * 1.5f;
                        }
                        else    // rotate with remain angle until end playing time.
                        {
                            _curTime += Time.deltaTime;
                            if(_curTime > _executeTime)
                            {
                                _curTime = 0;
                                ChangeState(CircleState.RotateDecrease);
                            }
                        }
                        
                        break;

                    case CircleState.RotateDecrease:
                        if((_deltaAngle > 0 && _curAngle > 0) 
                            || (_deltaAngle < 0) && _curAngle < 0)
                        {
                            _curAngle -= _deltaAngle * Time.deltaTime * 1.1f;
                        }
                        else
                        {
                            _curAngle = 0;
                            ChangeState(CircleState.Stay);
                        }
                        break;

                    case CircleState.Stay:
                        _curTime += Time.deltaTime;
                        if(_curTime > _executeTime)
                        {
                            _curTime = 0;
                            SetUpCircleData(RotateDataConfig);
                            ChangeState(CircleState.RotateIncrease);
                        }
                        break;

                    default:
                        break;
                }

                RotationAction();
            }
        }

        public void MoveTo(Vector3 target, bool isUseBounce = false, Action onCompleted = null)
        {
            Vector3 targetTemp = target;

            if(isUseBounce)
            {
                targetTemp.y -= 0.8f;
            }
            transform.DOLocalMove(targetTemp, 0.2f)
                    .SetEase(Ease.OutSine)
                    .OnComplete(() => {
                        if(isUseBounce)
                        {
                            transform.DOLocalMove(target, 0.25f)
                                .SetEase(Ease.InSine)
                                .OnComplete(() => {
                                    transform.localPosition = target;
                                    if(onCompleted != null)
                                        onCompleted();
                                });
                        }
                        else
                        {
                            transform.localPosition = target;
                            // _particle.Play();
                            // _particle.ParticleSystemStopAction
                            if(onCompleted != null)
                                onCompleted();
                        }
                    });
        }

        void ChangeState(CircleState state)
        {
            _curState = state;
        }

        public void RotationAction()
        {
            this.transform.RotateAround(this.pointRoot.transform.position, Vector3.up, _curAngle * this.itemInfor.Speed * Time.deltaTime);
        }

        public void OnStop()
        {
            this.itemInfor.IsComplete = true;
        }

        public void OnResume()
        {
            this.itemInfor.IsComplete = false;
        }

        public void OnCompleteCircle()
        {
            Color color = this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors;

            this.auraCompleteParticle.Play();

            var mainAura = this.auraCompleteParticle.main;

            mainAura.startColor = color;

            this.itemInfor.IsComplete = true;

             for (var i = 0; i < this.materialsSegment.Length; i++)
            {


                var alpha = this.materialsSegment[i].GetComponent<AlphaCutoffController>();
                alpha.MainColor = this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors;

                alpha.AlphaValue = 1;

                BoxCollider boxCollider = this.materialsSegment[i].GetComponent<BoxCollider>();
                boxCollider.enabled = false;

                if (this.iconDiamond[i].activeInHierarchy)
                    this.iconDiamond[i].SetActive(false);
            }

        }

        public void AnimFpsShake()
        {
            //this.animatorShake.CrossFade("CircleAnimtionShake");
            if (OnEventShake != null)
                OnEventShake();
        }

        public void OnCompleteShake()
        {
        }
    }
}

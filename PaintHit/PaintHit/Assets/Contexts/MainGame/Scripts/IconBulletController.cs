﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Contexts.MainGame.Scripts
{
    public class IconBulletController : MonoBehaviour
    {

        [SerializeField] private Image OnObject;

        [SerializeField] private GameObject OffObject;

        private List<ColorInfo> listColor;

        private PlayerDataService playerDataService;

        public void OnActive(bool isActive , PlayerDataService playerDataService)
        {
            this.OnObject.gameObject.SetActive(isActive);
            this.OffObject.SetActive(!isActive);

            this.playerDataService = playerDataService;

            this.listColor = this.playerDataService.GetPlayerData().colorDictionary[this.playerDataService.GetPlayerData().IdColorCurrent];

            this.OnObject.color = this.listColor[MainGameSceneController.instance.ItemInforCurrent.IdCircle].colors;
        }


    }
}
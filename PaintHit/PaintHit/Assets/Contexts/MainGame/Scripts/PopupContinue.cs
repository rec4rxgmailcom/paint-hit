﻿using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using Zenject;
using DG.Tweening;
using UnityEngine.UI;

namespace Assets.Contexts.MainGame.Scripts
{
    public class PopupContinue : MonoBehaviour
    {

        public static PopupContinue instance;

        public static PopupContinue Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new PopupContinue();
                }
                return instance;
            }
        }

        [Inject] GameHub gameHub;

        private long quantityCurrent;
        private long quantityRaise;
        private Tweener tweenCurrencyRaise;

        [SerializeField] private Text levelText;

        [SerializeField] private Image imageCountDown;

        [Inject] ShowAdsManager showAdsManager;

        [Inject] PlayerDataService playerDataService;

        private void OnEnable()
        {
            MainGameSceneController.instance.pressGesture.enabled = false;
        }

        private void OnDisable()
        {
            if (MainGameSceneController.instance.pressGesture == null)
                return;

            MainGameSceneController.instance.pressGesture.enabled = true;
        }

        public void OnShow()
        {
            instance = this;
            this.playerDataService.GetPlayerData().IsPlay = false;
            this.levelText.text = MainGameSceneController.Instance.LevelCurrent.ToString();
            //OnCountDown();
        }

        public void OnCountDown()
        {
            this.quantityCurrent = 0;
            this.imageCountDown.fillAmount = 1;

            this.tweenCurrencyRaise = DOTween.To(() => this.quantityCurrent, v =>
            {
                this.imageCountDown.fillAmount = (3 - v);
            }, 2.99f, 3).OnComplete(() =>
            {
                OnHide();
                this.tweenCurrencyRaise.Kill();
                this.imageCountDown.fillAmount = 0;
                this.playerDataService.GetPlayerData().IsPlay = true;
                this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
            }).Play();
        }


        public void OnClickContinue()
        {
            this.tweenCurrencyRaise.Kill();
            OnHide();
            this.playerDataService.GetPlayerData().IsPlay = true;
            this.showAdsManager.showInterstitialAd();
            MainGameSceneController.instance.OnResume();
        }

        public void OnClickRestart()
        {
            OnHide();
            this.playerDataService.GetPlayerData().IsPlay = true;
            this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
        }

        public void OnClickHome()
        {
            this.gameHub.LoadContext(GameHub.ContextName.MainMenuContext);
        }

        private void OnHide()
        {
            this.gameObject.SetActive(false);
        }
    }
}

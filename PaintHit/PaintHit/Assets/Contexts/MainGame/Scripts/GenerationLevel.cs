﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainGame.Scripts
{
    public class GenerationLevel : MonoBehaviour
    {

        public static GenerationLevel instance;

        public static GenerationLevel Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new GenerationLevel();
                }
                return instance;
            }
        }

        [Inject] GameData gameData;

        private List<int> levelStage = new List<int> { 10, 20, 40, 80, 120, 180,250,300 };

		private void OnEnable()
		{
            instance = this;
		}
	

        public InfoCircle GetLevelInfo(int level , float speed)
        {
            InfoCircle infoCircle = new InfoCircle();

            if(level < levelStage[0])
            {
                infoCircle.blockColor = 0;
                infoCircle.NumberBullet = 4;
                infoCircle.NumberCircle = 3;
                infoCircle.SpeedRotation = speed;
                infoCircle.NumberDiamond = 5;
                infoCircle.RotateIncrease = 1.5f;
                infoCircle.RotateDecrease = 1.1f;
            }
            else if(level < levelStage[1])
            {
                infoCircle.blockColor = 2;
                infoCircle.NumberBullet = 5;
                infoCircle.NumberCircle = 5;
                infoCircle.SpeedRotation = (speed *2);
                infoCircle.NumberDiamond = 4;
                infoCircle.RotateIncrease = 1.4f;
                infoCircle.RotateDecrease = 1.0f;
            }
            else if (level < levelStage[2])
            {
                infoCircle.blockColor = 3;
                infoCircle.NumberBullet = 6;
                infoCircle.NumberCircle = 6;
                infoCircle.SpeedRotation = (speed * 3);
                infoCircle.NumberDiamond = 3;
                infoCircle.RotateIncrease = 1.3f;
                infoCircle.RotateDecrease = 0.9f;
            }
            else if (level < levelStage[3])
            {
                infoCircle.blockColor = 4;
                infoCircle.NumberBullet = 7;
                infoCircle.NumberCircle = 7;
                infoCircle.SpeedRotation = (speed * 3.1f);
                infoCircle.NumberDiamond = 4;
                infoCircle.RotateIncrease = 1.2f;
                infoCircle.RotateDecrease = 0.8f;
            }
            else if (level < levelStage[4])
            {
                infoCircle.blockColor = 5;
                infoCircle.NumberBullet = 7;
                infoCircle.NumberCircle = 8;
                infoCircle.SpeedRotation = (speed * 3.1f);
                infoCircle.NumberDiamond = 4;
                infoCircle.RotateIncrease = 1.1f;
                infoCircle.RotateDecrease = 0.7f;
            }
            else if (level < levelStage[5])
            {
                infoCircle.blockColor = 6;
                infoCircle.NumberBullet = 7;
                infoCircle.NumberCircle = 8;
                infoCircle.SpeedRotation = (speed * 3.3f);
                infoCircle.NumberDiamond = 5;
                infoCircle.RotateIncrease = 1.0f;
                infoCircle.RotateDecrease = 0.6f;
            }
            else if (level < levelStage[6])
            {
                infoCircle.blockColor = 7;
                infoCircle.NumberBullet = 8;
                infoCircle.NumberCircle = 8;
                infoCircle.SpeedRotation = (speed * 3.4f);
                infoCircle.NumberDiamond = 6;
                infoCircle.RotateIncrease = 0.9f;
                infoCircle.RotateDecrease = 0.5f;
            }
            else if (level < levelStage[7])
            {
                infoCircle.blockColor = 7;
                infoCircle.NumberBullet = 8;
                infoCircle.NumberCircle = 9;
                infoCircle.SpeedRotation = (speed * 3.6f);
                infoCircle.NumberDiamond = 6;
                infoCircle.RotateIncrease = 0.9f;
                infoCircle.RotateDecrease = 0.5f;
            }
            else
            {
                infoCircle.blockColor = 8;
                infoCircle.NumberBullet = 9;
                infoCircle.NumberCircle = 9;
                infoCircle.SpeedRotation = (speed * 3.6f);
                infoCircle.NumberDiamond = 6;
                infoCircle.RotateIncrease = 0.9f;
                infoCircle.RotateDecrease = 0.5f;
            }
            return infoCircle;
        }

        public InfoCircle GetLevelBoss(int level, float speed)
        {
            InfoCircle infoCircle = new InfoCircle();

            int randomType = Random.Range(0, 3);

            switch(randomType)
            {
                case 0:
                    infoCircle = LevelBoss_1(speed);
                    break;
                case 1:
                    infoCircle = LevelBoss_2(speed);
                    break;
                case 2:
                    infoCircle = LevelBoss_3(speed);
                    break;
                case 3:
                    infoCircle = LevelBoss_4(speed);
                    break;

            }
            return infoCircle;
        }

        public InfoCircle LevelBoss_1(float speed)
        {
            InfoCircle infoCircle = new InfoCircle();
            infoCircle.blockColor = 19;
            infoCircle.NumberBullet = 1;
            infoCircle.NumberCircle = 5;
            infoCircle.SpeedRotation = (speed *1.5f);
            infoCircle.NumberDiamond = 4;
            infoCircle.RotateIncrease = 1.5f;
            infoCircle.RotateDecrease = 1.1f;
            return infoCircle;
        }

        public InfoCircle LevelBoss_2(float speed)
        {
            InfoCircle infoCircle = new InfoCircle();
            infoCircle.blockColor = 15;
            infoCircle.NumberBullet = 3;
            infoCircle.NumberCircle = 6;
            infoCircle.SpeedRotation = (speed * 1.5f);
            infoCircle.NumberDiamond = 4;
            infoCircle.RotateIncrease = 1.5f;
            infoCircle.RotateDecrease = 1.1f;
            return infoCircle;
        }

        public InfoCircle LevelBoss_3(float speed)
        {
            InfoCircle infoCircle = new InfoCircle();
            infoCircle.blockColor = 16;
            infoCircle.NumberBullet = 3;
            infoCircle.NumberCircle = 7;
            infoCircle.SpeedRotation = (speed * 2f);
            infoCircle.NumberDiamond = 4;
            infoCircle.RotateIncrease = 1.5f;
            infoCircle.RotateDecrease = 1.1f;
            return infoCircle;
        }

        public InfoCircle LevelBoss_4(float speed)
        {
            InfoCircle infoCircle = new InfoCircle();
            infoCircle.blockColor = 13;
            infoCircle.NumberBullet = 6;
            infoCircle.NumberCircle = 6;
            infoCircle.SpeedRotation = (speed * 1.5f);
            infoCircle.NumberDiamond = 4;
            infoCircle.RotateIncrease = 1.0f;
            infoCircle.RotateDecrease = 0.8f;
            return infoCircle;
        }
	}

 

    public class InfoCircle
    {
        public float _curAngle;

        public int NumberCircle;

        public float SpeedRotation;

        public int NumberBullet;

        public int blockColor;

        public int NumberDiamond;

        public float RotateIncrease;

        public float RotateDecrease;


    }
}

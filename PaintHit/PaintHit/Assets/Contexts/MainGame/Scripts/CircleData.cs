﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CircleStateData", menuName = "Circle/State Data")]
public class CircleData : ScriptableObject 
{
	[Header("All these values below are random value")]
	public float MinRemainRotateTime;
	public float MaxRemainRotateTime;
	public int MinAngle;
	public int MaxAngle;
	public float MinDeltaAngle;
	public float MaxDeltaAngle;
}

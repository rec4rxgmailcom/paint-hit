﻿using System.Collections;
using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using Zenject;
using DG.Tweening;
using UnityEngine.UI;
using System;

public class PopupDailyReward : MonoBehaviour 
{
    
    [SerializeField] private Text txtContent;

    [Inject] GameHub gameHub;

    [Inject] PlayerDataService playerDataService;

	public void SetUp()
	{

	}
	
	public void OnGetReward()
	{
		gameObject.SetActive(false);
	}

    private Action onComplete = null;
    public void OnShow(int diamond , Action callBack = null)
    {
        this.onComplete = callBack;
        this.playerDataService.UpdateDiamond(diamond);
        this.txtContent.text = string.Format("{0} Diamond", diamond); 
    }

    public void OnClickGetReward()
    {
        Debug.Log("===> click re " + this.onComplete);
        if (this.onComplete != null)
        {
            this.onComplete();
        }
        this.gameObject.SetActive(false);
      
    }
}

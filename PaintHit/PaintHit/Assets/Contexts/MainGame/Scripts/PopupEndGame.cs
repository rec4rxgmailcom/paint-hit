﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainGame.Scripts
{
    public class PopupEndGame : MonoBehaviour
    {

        public static PopupEndGame instance;

        public static PopupEndGame Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new PopupEndGame();
                }
                return instance;
            }
        }

        [SerializeField] private Text levelText;

        [SerializeField] private Image imageCountDown;

        [SerializeField] private GameObject panelDanger;

        private long quantityCurrent;
        private long quantityRaise;
        private Tweener tweenCurrencyRaise;

        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        private float timeDelay = 1;
        private void OnEnable()
        {
            MainGameSceneController.instance.pressGesture.enabled = false;
        }

        private void OnDisable()
        {
            if (MainGameSceneController.instance.pressGesture == null)
                return;
            
            MainGameSceneController.instance.pressGesture.enabled = true;
        }

        public void OnShow(bool isDanger = false , float timeDelay = 1)
        {
            instance = this;
            this.timeDelay = timeDelay;
            this.panelDanger.SetActive(false);
            if(isDanger)
            {
                this.panelDanger.SetActive(true);
            }

            this.playerDataService.GetPlayerData().totalPlayGame++;
            this.levelText.text = MainGameSceneController.Instance.LevelCurrent.ToString();
            OnCountDown();


        }

        public void OnHide()
        {
            this.gameObject.SetActive(false);
        }

        public void OnCountDown()
        {
            this.quantityCurrent = 0;
            this.imageCountDown.fillAmount = 1;

            this.tweenCurrencyRaise = DOTween.To(() => this.quantityCurrent, v =>
            {
                this.imageCountDown.fillAmount = (1 - v);
            }, this.timeDelay - 0.01f, this.timeDelay).OnComplete(() =>
            {
                this.tweenCurrencyRaise.Kill();
                this.imageCountDown.fillAmount = 0;
                OnHide();
                if(MainGameSceneController.Instance.LevelCurrent == 0)
                {
                    MainGameSceneController.Instance.StartGame();
                }
                else
                {
                    this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
                }
           
            }).Play();
        }

        public void OnClickHome()
        {
            this.gameHub.LoadContext(GameHub.ContextName.MainMenuContext);
        }
    }
}

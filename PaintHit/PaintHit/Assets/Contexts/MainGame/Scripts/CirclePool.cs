﻿using Assets.Contexts.MainGame.Scripts;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Contexts.MainGame.Scripts
{
    public class CirclePool : MonoBehaviour
    {
        public List<CircleController> PoolbjectList = new List<CircleController>();
        public void InStance(GameObject obj, int size, Transform parent)
        {
            for (var i = 0; i < size; i++)
            {
                GameObject objectNew = GameObject.Instantiate(obj);

                CircleController circleController = objectNew.GetComponent<CircleController>();

                if (circleController == null)
                {
                    circleController = objectNew.AddComponent<CircleController>();
                }
                circleController.gameObject.SetActive(false);
                circleController.transform.SetParent(parent);

                PoolbjectList.Add(circleController);
            }
        }

        public CircleController GetObject()
        {
            for (var i = 0; i < PoolbjectList.Count; i++)
            {
                if (!PoolbjectList[i].gameObject.activeInHierarchy)
                {
                    PoolbjectList[i].gameObject.SetActive(true);
                    return PoolbjectList[i];
                }
            }

            return null;
        }

        public void Clear()
        {
            for (var i = PoolbjectList.Count - 1; i > 0; i--)
            {
                CircleController circleController = PoolbjectList[i];
                if (circleController)
                {
#if UNITY_EDITOR
                    GameObject.Destroy(circleController.gameObject);
#else
                    GameObject.Destroy(circleController.gameObject);
#endif
                }
            }
            PoolbjectList.Clear();
        }
    }
}

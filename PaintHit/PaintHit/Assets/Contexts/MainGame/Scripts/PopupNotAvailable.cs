﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
namespace Assets.Contexts.MainGame.Scripts
{
	public class PopupNotAvailable : MonoBehaviour 
	{
        [SerializeField] private TextMeshProUGUI txtContent;

        public void OnShow(string msg)
        {
            this.txtContent.text = string.Format("{0}", msg);

        }
		public void OnOKClicked()
		{
			gameObject.SetActive(false);
		}
	}

}

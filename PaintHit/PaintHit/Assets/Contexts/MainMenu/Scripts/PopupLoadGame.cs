﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class PopupLoadGame : MonoBehaviour
    {

        public static PopupLoadGame instance;

        public static PopupLoadGame Instance
        {

            get
            {
                if (instance == null)
                {
                    instance = new PopupLoadGame();
                }
                return instance;
            }
        }

        [SerializeField] private Text levelText;

        [SerializeField] private Image imageCountDown;

        private long quantityCurrent;
        private long quantityRaise;
        private Tweener tweenCurrencyRaise;

        [Inject] GameHub gameHub;

        [Inject] GameData gameData;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Image backgroundImage;

        [SerializeField] private Sprite[] backgroundGame;


        public void OnShow()
        {
            instance = this;
            this.backgroundImage.sprite = this.backgroundGame[Random.Range(0, this.backgroundGame.Length - 1)];

            this.levelText.text = this.playerDataService.GetPlayerData().level.ToString();
            OnCountDown();
        }

        public void OnHide()
        {
            this.gameObject.SetActive(false);
        }

        public void OnCountDown()
        {
            this.quantityCurrent = 0;
            this.imageCountDown.fillAmount = 1;

            this.tweenCurrencyRaise = DOTween.To(() => this.quantityCurrent, v =>
            {
                this.imageCountDown.fillAmount = (1 - v);
            }, 0.99f, 1).OnComplete(() =>
            {
                this.tweenCurrencyRaise.Kill();
                this.imageCountDown.fillAmount = 0;
                this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);

            }).Play();
        }
    }
}

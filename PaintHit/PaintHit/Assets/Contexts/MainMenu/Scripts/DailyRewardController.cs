﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Zenject;
using System;
using UnityEngine.UI;
using Assets.Contexts.MainGame.Scripts;
using Assets.Contexts.MainMenu.Scripts;
using DG.Tweening;

public class DailyRewardController : MonoBehaviour 
{

	[SerializeField] TMP_Text _hours;
	[SerializeField] TMP_Text _minutes;
	[SerializeField] TMP_Text _seconds;
	[SerializeField] List<TMP_Text> _listCoinText;
	[SerializeField] List<Image> _blackImages;
	[SerializeField] List<TMP_Text> _textClaimeds;
	[SerializeField] List<int> _coinsByDays = new List<int>();
    [SerializeField] private PopupDailyReward popupReward;
    [SerializeField] private PopupNotAvailable popupNotAvailable;
	[Inject] RewardManager _rewardmanager;

    [Inject] PlayerDataService playerDataService;

    [SerializeField] private Transform endPos;

    [SerializeField] private Transform[] beginPos;

    [SerializeField] private Transform iconDiamond;

	bool _countTime;
	long _nextDateTime;

	public void OnShow()
	{
		_countTime = true;
		for (int i = 0; i < _listCoinText.Count; i++)
		{
			_listCoinText[i].text = "x " + _coinsByDays[i].ToString();
		}

		_nextDateTime = RewardManager.SECONDS_PER_DAY * (_rewardmanager.DailyRewardData.CurrentDayCount + 1);

		RefeshUI();
		_rewardmanager.OnUpdateGetNewReward = RefeshUI;
	}

	public void RefeshUI()
	{
		for (int i = 0; i < _rewardmanager.DailyRewardData.Rewards.Length; i++)
		{
			if(_rewardmanager.DailyRewardData.Rewards[i] == 1)
			{
				_blackImages[i].enabled = false;
                if (_rewardmanager.OnUpdateGetNewReward != null)
                    _rewardmanager.OnUpdateGetNewReward();
			}
			else
			{
				_blackImages[i].enabled = true;
			}

			if(_rewardmanager.DailyRewardData.Claimpeds[i])
			{
				_textClaimeds[i].gameObject.SetActive(true);
			}
		}
	}

	public void OnHide()
	{
		_countTime = false;
	}

	void Update()
	{
		if(!_countTime)
			return;

		long curTime = _rewardmanager.GetCurrentTimeStamp();
		long count = curTime - _rewardmanager.DailyRewardData.LastDateTimeSaved;
		long timeRemain = _nextDateTime - count;
		var timeSpan = TimeSpan.FromSeconds(timeRemain);
		_hours.text = timeSpan.Hours >= 10 ? timeSpan.Hours.ToString() : "0" + timeSpan.Hours;
		_minutes.text = timeSpan.Minutes >= 10 ? timeSpan.Minutes.ToString(): "0" + timeSpan.Minutes;
		_seconds.text = timeSpan.Seconds >= 10 ? timeSpan.Seconds.ToString() : "0" + timeSpan.Seconds;
	}

	public void OnDailyRewardClick(int dayIdx)
	{



		if(_rewardmanager.DailyRewardData.Rewards[dayIdx] == 1)
		{


                _rewardmanager.DailyRewardData.Rewards[dayIdx] = 0;
                _rewardmanager.DailyRewardData.Claimpeds[dayIdx] = true;
               
                _textClaimeds[dayIdx].gameObject.SetActive(true);
                popupReward.SetUp();
                popupReward.gameObject.SetActive(true);
            popupReward.OnShow(_coinsByDays[dayIdx], () =>
            {
                this.iconDiamond.gameObject.SetActive(true);
                this.iconDiamond.transform.localPosition = this.beginPos[dayIdx].localPosition;

                Sequence sequence = DOTween.Sequence();
                sequence.Append(this.iconDiamond.DOMove(this.endPos.position, 0.4f)).OnComplete(() =>
                {

                    sequence.Kill();

                    this.iconDiamond.gameObject.SetActive(false);

                    this.playerDataService.UpdateDiamond(_coinsByDays[dayIdx]);

                    MainMenuUi.Instance.ShowOffNotiReward();
                    _rewardmanager.Save();
                    RefeshUI();
                }).Play();
            });
		}
		else
		{
			popupNotAvailable.gameObject.SetActive(true);
            popupNotAvailable.OnShow("Have not available yet");
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using Zenject;

public class IapCountDown :MonoBehaviour{

    public static IapCountDown instance;

    public static IapCountDown Instance
    {
        get
        {
            if(instance == null)
            {
                instance = new IapCountDown();
            }
            return instance;
        }
       

    }
    private Tween coutingTween = null;

    [Inject] PlayerDataService playerDataService;

    public void CheckTimeCoutDown()
    {
        if (isCheckTime())
        {
           
          
        }
        //start counting down
        if (this.coutingTween != null)
        {
            this.coutingTween.Kill();
        }

        TimeSpan diff = TimeSpan.FromHours((7)) - GetTimeFromLastSave();
        double seconds = diff.TotalSeconds;
        this.coutingTween = DOVirtual.Float((float)seconds, 0.0f, (float)seconds, f =>
        {
            TimeSpan dateDifference = TimeSpan.FromSeconds((double)f);

        }).SetEase(Ease.Linear).OnComplete(() => {
            
        }).Play();
    }

    #region time checker


    bool isCheckTime()
    {
        if (IsHaveReward())
        {
            return true;
        }
        return false;
    }


    public TimeSpan GetTimeFromLastSave()
    {
        DateTime advancedTime = DateTime.Now;

        TimeSpan diff = advancedTime - this.playerDataService.GetPlayerData().TimeBeginReward;
        return diff;
    }


    public bool IsHaveReward()
    {
        TimeSpan diff = GetTimeFromLastSave();
        double hour = (double)Math.Abs(diff.TotalHours / 7);

        if (hour >= 7)
        {
            return true;
        }

        return false;
    }

    #endregion




}

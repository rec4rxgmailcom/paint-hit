﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class StageController : MonoBehaviour
    {

        public static StageController instance;

        public static StageController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new StageController();
                }
                return instance;
            }
        }

        [SerializeField] GameObject stagePrefap;

        [SerializeField] GameObject parentStageGameObject;

        [Inject] GameHub gameHub;

        [Inject] GameData gameData;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Image[] iconPage;

        [SerializeField] public Sprite iconCurrentpage;

        [SerializeField] public Sprite iconDisibelPage;

        public void OnShow()
        {
            instance = this;

            for (var i = 0; i < this.iconPage.Length;i++)
            {
                this.iconPage[i].sprite = this.iconDisibelPage;
            }
            this.iconPage[0].sprite = this.iconCurrentpage;

            for (var i = 0; i < this.playerDataService.GetPlayerData().stageUnClockDictionary.Count;i++)
            {
                GameObject stageObj = GameObject.Instantiate(this.stagePrefap, this.parentStageGameObject.transform);
                stageObj.GetComponent<ItemStage>().OnShow(this.gameHub, this.playerDataService, i);
            }


        }

		private void FixedUpdate()
		{
            float posX = this.parentStageGameObject.transform.position.x;

            int page = 0;


            if(posX < 0 && posX  > -2000)
            {
                page = 0;

            }
            else if (posX <= -2000 && posX > -4000)
            {
                page = 1;

            }
            else if (posX <= -4000 && posX > -6000)
            {
                page = 2;

            }
            else if (posX <= -6000 && posX > -8000)
            {
                page = 3;

            }
            else if (posX <= -8000 && posX > -10000)
            {
                page = 4;

            }
            else if (posX <= -10000 && posX > -12000)
            {
                page = 5;

            }
            else if (posX <= -12000 && posX > -14000)
            {
                page = 6;

            }
            else if (posX <= -14000 && posX > -16000)
            {
                page = 7;

            }
            else if (posX <= -16000 && posX > -18000)
            {
                page = 8;

            }

            for (var i = 0; i < this.iconPage.Length; i++)
            {
                this.iconPage[i].sprite = this.iconDisibelPage;
            }
            this.iconPage[page].sprite = this.iconCurrentpage;

		}
		private void OnEnable()
		{
            MainMenuUi.instance.btnSetting.gameObject.SetActive(false);
		}
		private void OnDisable()
        {
            MainMenuUi.instance.btnSetting.gameObject.SetActive(true);
            foreach (Transform icon in this.parentStageGameObject.transform)
            {
                GameObject.Destroy(icon.gameObject);
            }
        }

        #region button click

#endregion
    }
}

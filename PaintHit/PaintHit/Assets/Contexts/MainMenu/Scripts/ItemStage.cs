﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Contexts.BaseScript;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class ItemStage : MonoBehaviour
    {
        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Text txtStage;

        [SerializeField] private GameObject panelLock;

        [SerializeField] private GameObject panelUnclock;

        [SerializeField] private GameObject panelHighlight;

        [SerializeField] private int IdStage;
      
        public void OnShow(GameHub gameHub, PlayerDataService playerDataService, int index)
        {
            this.IdStage = index;

            this.gameHub = gameHub;

            this.playerDataService = playerDataService;

            this.txtStage.text = string.Format("{0}", index+ 1);

            this.panelHighlight.SetActive(false);

            if (this.playerDataService.GetPlayerData().stageUnClockDictionary.ContainsKey(index))
            {
                
                var isUnlock = this.playerDataService.GetPlayerData().stageUnClockDictionary[index];

                if (isUnlock)
                {
                    this.panelLock.gameObject.SetActive(false);
                    this.panelUnclock.gameObject.SetActive(true);
                }
                else
                {
                    this.panelUnclock.gameObject.SetActive(false);
                    this.panelLock.gameObject.SetActive(true);
                }
            }


            if(this.playerDataService.GetPlayerData().level == this.IdStage + 1)
            {
                this.panelHighlight.SetActive(true);
            }


        }
        public void OnClickStage()
        {
            if (!this.panelUnclock.activeInHierarchy)
                return;
            this.playerDataService.GetPlayerData().level = this.IdStage;
            this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class SettingController : MonoBehaviour
    {

        public static SettingController instance;

        public static SettingController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new SettingController();
                }
                return instance;
            }
        }

        public ToggleCustom musicToggle;
        public ToggleCustom soundToggle;

        [Inject] SoundPalette SoundPalette;

        [SerializeField] private GameObject popupCopyRight;

        [SerializeField] private GameObject iconSetting;

        public void OnShow()
        {
            instance = this;
            this.popupCopyRight.SetActive(false);
            this.iconSetting.SetActive(false);
        }

        public void OnHide()
        {
            MainMenuUi.instance.OnClickHome();
            this.gameObject.SetActive(false);
           
        }


        private void OnEnable()
        {

            SetUpUI();

            // listener event
            this.musicToggle.onToggleChange += OnMusicChange;
            this.soundToggle.onToggleChange += OnSoundChange;


        }
        private void OnDisable()
        {
            this.iconSetting.SetActive(true);
            this.musicToggle.onToggleChange -= OnMusicChange;
            this.soundToggle.onToggleChange -= OnSoundChange;

        }

        private void SetUpUI()
        {
            // load setting
            LoadSoundSetting();
            LoadMusicSetting();
        }


        private void SaveSoundSetting(bool isOn)
        {
            PlayerPrefs.SetInt(GameConst.OPTION_SOUND, isOn == true ? 1 : 0);
            PlayerPrefs.Save();
        }

        private void LoadSoundSetting()
        {
            if (PlayerPrefs.GetInt(GameConst.OPTION_SOUND, 1) == 1)
            {
                this.soundToggle.SetToggleStatus(true);
            }
            else
            {
                this.soundToggle.SetToggleStatus(false);
            }

        }

        private void SaveMusicSetting(bool isOn)
        {
            PlayerPrefs.SetInt(GameConst.OPTION_MUSIC, isOn == true ? 1 : 0);
            PlayerPrefs.Save();
        }

        private void LoadMusicSetting()
        {
            if (PlayerPrefs.GetInt(GameConst.OPTION_MUSIC, 1) == 1)
            {
                this.musicToggle.SetToggleStatus(true);
            }
            else
            {
                this.musicToggle.SetToggleStatus(false);
            }
        }


        #region Button Action Clicked
        private void OnMusicChange(bool isOn)
        {
            if (isOn)
            {
                this.SoundPalette.PlayBGM();
                //this.SoundPalette.volumeMultiplier = 1;
                //this.SoundPalette.EnableVolumeBGM();
            }
            else
            {
                this.SoundPalette.StopBGM(0.3f);
                //this.SoundPalette.DisableVolumeBGM();
                //this.SoundPalette.volumeMultiplier = 0; 
            }
            SaveMusicSetting(isOn);
        }

        private void OnSoundChange(bool isOn)
        {
            this.SoundPalette.volumeSoundEffect = isOn ? 1 : 0;
            SaveSoundSetting(isOn);
        }

        public void OnClickOpenCopy()
        {
            this.popupCopyRight.SetActive(true);
        }
        public void OnClickCloseCopyRight()
        {
            this.popupCopyRight.SetActive(false);
        }
#endregion
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core.SystemContext;
using UnityEngine;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class MainMenuContextDelegate : BaseContextDelegate
    {
        public override void OnDestroyContext(Context context)
        {
            Controller.Destroy();
        }

        public override void OnLoadContext(Context context)
        {
            Controller.Load();
        }

        public override void OnUnLoadContext(Context context)
        {
            Controller.Unload();
        }
    }
}



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class LevelController : MonoBehaviour
    {
        public static LevelController instance;

        public static LevelController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new LevelController();
                }
                return instance;
            }
        }

        [SerializeField] GameObject ColorPrefap;

        [SerializeField] GameObject parentColorGameObject;

        [SerializeField] GameObject CirclePrefap;

        [SerializeField] GameObject parentCircleGameObject;




        [Inject] GameHub gameHub;

        [Inject] GameData gameData;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] GameObject panelColor;

        [SerializeField] GameObject panelCircle;

        //[SerializeField] GameObject highlightColor;

        //[SerializeField] GameObject highlightCircle;

        [SerializeField] Button btnColor;

        [SerializeField] Sprite sprColorNormal;

        [SerializeField] Sprite sprColorHighlight;

        [SerializeField] Button btnCircle;

        [SerializeField] Sprite sprCircleNormal;

        [SerializeField] Sprite sprCircleHighlight;

        public delegate void OnEventClickItem();

        public event OnEventClickItem OnClickItem;


		private void OnEnable()
		{
            this.OnClickItem += OnClassClickItem;
		}

        public void EventClickItem()
        {
            if(this.OnClickItem != null)
            {
                this.OnClickItem();
            }
        }
		public void OnShow()
        {
            instance = this;

            this.panelColor.SetActive(true);
            this.panelCircle.SetActive(false);

            this.btnColor.GetComponent<Image>().sprite = this.sprColorHighlight;

            this.btnCircle.GetComponent<Image>().sprite = this.sprCircleNormal;

            //this.highlightColor.SetActive(true);
            //this.highlightCircle.SetActive(false);

            for (var i = 0; i < this.gameData.MainGameData.ListColors.Length; i++)
            {
                GameObject objLevel = GameObject.Instantiate(this.ColorPrefap, this.parentColorGameObject.transform);
                objLevel.GetComponent<ItemColor>().OnShow(this.gameHub, this.playerDataService, i);
            }

            this.parentColorGameObject.transform.position = new Vector3(this.parentColorGameObject.transform.position.x,300,0);
        }

        public void OnHide()
        {

        }


        private void OnDisable()
        {
            foreach (Transform icon in this.parentColorGameObject.transform)
            {
                GameObject.Destroy(icon.gameObject);
            }

            foreach (Transform icon in this.parentCircleGameObject.transform)
            {
                GameObject.Destroy(icon.gameObject);
            }
            this.OnClickItem -= OnClassClickItem;
        }

        private void OnClassClickItem()
        {
            if(this.parentColorGameObject.activeInHierarchy)
            {
                foreach (Transform item in this.parentColorGameObject.transform)
                {
                    item.GetComponent<ItemColor>().OnUnclickItem();
                }
            }

            if (this.parentCircleGameObject.activeInHierarchy)
            {
                foreach (Transform item in this.parentCircleGameObject.transform)
                {
                    item.GetComponent<ItemCircle>().OnUnclickItem();
                }
            }
        }

        #region button click

        public void OnClickColor()
        {
            
            foreach (Transform icon in this.parentCircleGameObject.transform)
            {
                GameObject.Destroy(icon.gameObject);
            }
            this.panelColor.SetActive(true);
            this.panelCircle.SetActive(false);

            //this.highlightColor.SetActive(true);
            //this.highlightCircle.SetActive(false);

            this.btnColor.GetComponent<Image>().sprite = this.sprColorHighlight;

            this.btnCircle.GetComponent<Image>().sprite = this.sprCircleNormal;

            for (var i = 0; i < this.gameData.MainGameData.ListColors.Length; i++)
            {
                GameObject objLevel = GameObject.Instantiate(this.ColorPrefap, this.parentColorGameObject.transform);
                objLevel.GetComponent<ItemColor>().OnShow(this.gameHub, this.playerDataService,i);
            }
            this.parentColorGameObject.transform.position = new Vector3(this.parentColorGameObject.transform.position.x, 300, 0);
        }

        public void OnClickCircle()
        {
            foreach (Transform icon in this.parentColorGameObject.transform)
            {
                GameObject.Destroy(icon.gameObject);
            }

            this.panelColor.SetActive(false);
            this.panelCircle.SetActive(true);

            //this.highlightColor.SetActive(false);
            //this.highlightCircle.SetActive(true);

            this.btnColor.GetComponent<Image>().sprite = this.sprColorNormal;

            this.btnCircle.GetComponent<Image>().sprite = this.sprCircleHighlight;

            for (var i = 0; i < this.gameData.MainGameData.ListCircle.Length; i++)
            {
                GameObject objLevel = GameObject.Instantiate(this.CirclePrefap, this.parentCircleGameObject.transform);
                objLevel.GetComponent<ItemCircle>().OnShow(this.gameHub, this.playerDataService, i);
            }
            this.parentCircleGameObject.transform.position = new Vector3(this.parentCircleGameObject.transform.position.x, 300, 0);
        }

        #endregion
    }
}

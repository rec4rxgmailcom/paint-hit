﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class QuestController : MonoBehaviour
    {

        [SerializeField] private Text txtQuest;

        [SerializeField] private Text txtvalueDiamond;

        [SerializeField] private Text txtClaim;

        [SerializeField] private Image imageFill;

        private int level;

        private int diamond;

        private GameHub gameHub;

        private PlayerDataService playerDataService;

        [SerializeField] public Color OpenColor;

        [SerializeField] public Color ClaimColor;

        [SerializeField] public Color ClaimedColor;

        public enum StatusQuest
        {
            Open,
            Claim,
            Claimed
        }

        public void OnShow(GameHub gameHub, PlayerDataService playerDataService, int level , float diamond)
        {
            this.level = level;

            this.gameHub = gameHub;
            this.playerDataService = playerDataService;


            this.txtClaim.text = string.Format("Open");

            this.imageFill.color = new Color(255,248,0);

            this.txtQuest.text = string.Format("Complete {0}",level);

            this.txtvalueDiamond.text = string.Format("{0}", diamond);
        }

        public void OnClickQuest()
        {
           
            this.playerDataService.GetPlayerData().ItemQuestInfo.LevelQuest = this.level;

            this.gameHub.LoadContext(GameHub.ContextName.MainGameContext);
        }

        public void UpdateChallengeDoing()
        {
            this.txtClaim.text = string.Format("Claim");
            this.imageFill.color = new Color(5,255,0);
        }
    }
}

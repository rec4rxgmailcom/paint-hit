﻿using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;
using DG.Tweening;


namespace Assets.Contexts.MainMenu.Scripts
{
    public class HomeController : MonoBehaviour
    {
        public static HomeController instance;

        public static HomeController Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new HomeController();
                }
                return instance;
            }
        }


        [SerializeField] private Text txtLevel;

        [Inject] GameData gameData;

        [Inject] SoundPalette soundPalette;

        [Inject] PlayerDataService playerDataService;
        
        public void OnShow()
        {
            instance = this;

            this.txtLevel.text = "Level " + this.playerDataService.GetPlayerData().level;
        }


        public void OnHide()
        {

        }

        public void OnClickStage()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            MainMenuUi.instance.OnClickStage();
        }
    }
}
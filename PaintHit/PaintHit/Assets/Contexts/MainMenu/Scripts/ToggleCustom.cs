﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class ToggleCustom : MonoBehaviour
    {

        public GameObject onObject;

        public delegate void EventToggleChange(bool isStatus);

        public event EventToggleChange onToggleChange;


        private bool isOn = true;

        public void OnChangeStatus()
        {
            this.isOn = !this.isOn;

            if (this.isOn)
            {
                this.onObject.SetActive(true);

            }
            else
            {
                this.onObject.SetActive(false);

            }
            if(this.onToggleChange != null)
            {
                this.onToggleChange(isOn);
            }
        }

        public void SetToggleStatus(bool isOnStatus)
        {
            if (isOnStatus)
            {
                this.onObject.SetActive(true);
               ;
            }
            else
            {
                this.onObject.SetActive(false);
               
            }
        }
    }
}
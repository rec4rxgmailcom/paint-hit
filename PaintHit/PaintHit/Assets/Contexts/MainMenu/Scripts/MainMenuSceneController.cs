﻿
using Zenject;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Assets.Scripts.Core;
using UnityEngine.EventSystems;
using Assets.Scripts.Core.EventsManager;
using Assets.Scripts.InGame.Controllers;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class MainMenuSceneController : Controller
    {
        [Inject] GameHub gameHub;

        public void OnPlayGame()
        {
            MainMenuUi.Instance.OnPlayGame();
        }
    }

}
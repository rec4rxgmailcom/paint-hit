﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Contexts.BaseScript;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using UnityEngine.UI;

namespace Assets.Contexts.MainMenu.Scripts
{
public class ItemCircle : MonoBehaviour {

        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private GameObject panelLock;

        [SerializeField] private GameObject panelUnclock;

        [SerializeField] private Sprite[] sprImageCircle;

        [SerializeField] private Image imageCircle;

        [SerializeField] private Image imageCircleLock;

        [SerializeField] private GameObject highligh;

        [SerializeField] private Text price;

        [SerializeField] private int[] priceDefaul;

        private int idCircle;

        public void OnShow(GameHub gameHub, PlayerDataService playerDataService, int index)
        {
            this.idCircle = index;

            this.gameHub = gameHub;

            this.playerDataService = playerDataService;

            this.imageCircle.sprite = this.sprImageCircle[index];

            this.highligh.SetActive(false);

            if(this.playerDataService.GetPlayerData().circleUnlock.ContainsKey(index))
            {
                var isUnlock = this.playerDataService.GetPlayerData().circleUnlock[index];

                if (isUnlock)
                {
                    this.panelLock.gameObject.SetActive(false);
                    this.panelUnclock.gameObject.SetActive(true);
                }
                else
                {
                    //int diamond = this.playerDataService.GetPlayerData().diamond;

                    //if (diamond >= this.priceDefaul[this.idCircle])
                    //{
                    //    this.playerDataService.UpdateDiamond(-this.priceDefaul[this.idCircle]);
                    //    this.playerDataService.GetPlayerData().circleUnlock[this.idCircle] = true;
                    //    this.panelLock.gameObject.SetActive(false);
                    //    this.panelUnclock.gameObject.SetActive(true);
                    //}
                    //else
                    //{
                        this.panelUnclock.gameObject.SetActive(false);
                        this.panelLock.gameObject.SetActive(true);
                        this.price.text = string.Format("{0}", this.priceDefaul[index]);
                        this.imageCircleLock.sprite = this.sprImageCircle[index];
                    //}

                 
                }
            }
           
            if(this.idCircle == this.playerDataService.GetPlayerData().idCircleSelect)
            {
                this.highligh.SetActive(true);
            }

        }
        public void OnClickCircle()
        {
            if (this.playerDataService.GetPlayerData().circleUnlock[this.idCircle])
            {
                LevelController.instance.EventClickItem();
                this.playerDataService.GetPlayerData().idCircleSelect = this.idCircle;
                this.highligh.SetActive(true);
            }
        }

        public void OnUnclickItem()
        {
            this.highligh.SetActive(false);
        }

        public void OnClickBuy()
        {
            int diamond = this.playerDataService.GetPlayerData().diamond;

            if (diamond >= this.priceDefaul[this.idCircle])
            {
                this.playerDataService.UpdateDiamond(-this.priceDefaul[this.idCircle]);
                this.playerDataService.GetPlayerData().circleUnlock[this.idCircle] = true;
                LevelController.instance.EventClickItem();
                this.playerDataService.GetPlayerData().idCircleSelect = this.idCircle;
                this.highligh.SetActive(true);
                this.panelLock.gameObject.SetActive(false);
                this.panelUnclock.gameObject.SetActive(true);
            }
            else{
                MainMenuUi.instance.ShowNotice("you are not enough diamond");
            }
        }
	}
}

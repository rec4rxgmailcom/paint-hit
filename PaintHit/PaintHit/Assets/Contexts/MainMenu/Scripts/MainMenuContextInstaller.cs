﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Core.SystemContext;
using UnityEngine;
using Zenject;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class MainMenuContextInstaller : MonoInstaller
	{
		[SerializeField] MainMenuContextDelegate _contextDelegate;

		public override void InstallBindings()
		{
			Container.Bind<BaseContextDelegate>().FromInstance(_contextDelegate);
		}
	}
}

﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Assets.Scripts.InGame.Controllers;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class PopupVideoAds : MonoBehaviour
    {

        public static PopupVideoAds instance = null;

        public static PopupVideoAds Instance
        {
            get
            {
                return instance;
            }
        }

        [SerializeField] private Image background;

        [Inject] GameData gameData;

        [Inject] ShowAdsManager showAdsManager;

        [Inject] PlayerDataService playerDataService;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
        public void OnEnable()
        {
            this.gameObject.transform.localPosition =
                new Vector3(this.gameObject.transform.localPosition.x, this.gameObject.transform.localPosition.y, -3);

        }


        public void OnDisable()
        {
           
                MainMenuUi.instance.OnClickHome();
        }

        public void OnClickVideoAds()
        {
            this.showAdsManager.showInterstitialAd();
            this.playerDataService.UpdateDiamond(this.gameData.MainGameData.diamondVideoAds);
            this.gameObject.SetActive(false);

        }

        public void OnClickLater()
        {
            this.gameObject.SetActive(false);
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using Zenject;
using UnityEngine.UI;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class ItemColor : MonoBehaviour
    {
        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private GameObject panelLock;

        [SerializeField] private GameObject panelUnclock;

        [SerializeField] private Sprite[] sprImageCircle;

        [SerializeField] private Image imageCircle;

        [SerializeField] private Image imageCircleLock;

        [SerializeField] private GameObject highlight;

        [SerializeField] private Text price;

        [SerializeField] private int[] priceDefaul;

        private int idColor;

        public void OnShow(GameHub gameHub , PlayerDataService playerDataService,  int index){

            this.idColor = index;

            this.gameHub = gameHub;

            this.playerDataService = playerDataService;

            this.imageCircle.sprite = this.sprImageCircle[index];

            this.highlight.SetActive(false);

            if (this.playerDataService.GetPlayerData().colorUnlock.ContainsKey(index))
            {
                var isUnlock = this.playerDataService.GetPlayerData().colorUnlock[index];

                if (isUnlock)
                {
                    this.panelLock.gameObject.SetActive(false);
                    this.panelUnclock.gameObject.SetActive(true);
                }
                else
                {
                    //int diamond = this.playerDataService.GetPlayerData().diamond;

                    //if (diamond >= this.priceDefaul[this.idColor])
                    //{
                    //    this.playerDataService.UpdateDiamond(-this.priceDefaul[this.idColor]);
                    //    this.playerDataService.GetPlayerData().colorUnlock[this.idColor] = true;
                    //    this.panelLock.gameObject.SetActive(false);
                    //    this.panelUnclock.gameObject.SetActive(true);
                    //}
                    //else{
                        this.panelUnclock.gameObject.SetActive(false);
                        this.panelLock.gameObject.SetActive(true);
                        this.price.text = string.Format("{0}", this.priceDefaul[index]);
                        this.imageCircleLock.sprite = this.sprImageCircle[index];
                    //}

                }
            }

            if(this.idColor == this.playerDataService.GetPlayerData().IdColorCurrent)
            {
                this.highlight.SetActive(true);
            }
        }
        public void OnClickPlayGame()
        {
            if(this.playerDataService.GetPlayerData().colorUnlock[this.idColor])
            {
                LevelController.instance.EventClickItem();
                this.playerDataService.GetPlayerData().IdColorCurrent = this.idColor;
                this.highlight.SetActive(true); 
            }
         
        }

        public void OnUnclickItem()
        {
            this.highlight.SetActive(false);
        }

        public void OnClickBuy()
        {
            int diamond = this.playerDataService.GetPlayerData().diamond;

            if (diamond >= this.priceDefaul[this.idColor])
            {
                this.playerDataService.UpdateDiamond(-this.priceDefaul[this.idColor]);
                this.playerDataService.GetPlayerData().colorUnlock[this.idColor] = true;
                LevelController.instance.EventClickItem();
                this.playerDataService.GetPlayerData().IdColorCurrent = this.idColor;
                this.highlight.SetActive(true);
                this.panelLock.gameObject.SetActive(false);
                this.panelUnclock.gameObject.SetActive(true);
            }
            else
            {
                MainMenuUi.instance.ShowNotice("you are not enough diamond");
            }
        }
    }
}
﻿using Assets.Scripts.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using UnityEngine.UI;
using DG.Tweening;
using Assets.Contexts.BaseScript;
using Assets.Contexts.MainGame.Scripts;
using Assets.SimpleAndroidNotifications;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class MainMenuUi : MonoBehaviour
    {
        public static MainMenuUi instance;

        public static MainMenuUi Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new MainMenuUi();
                }
                return instance;
            }
        }

        [Inject] GameHub gameHub;

        [Inject] SoundPalette soundPalette;

        [Inject] private GameData gameData;

        [Inject] ShowAdsManager showAdsManager;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Text txtDiamond;

        [SerializeField] private Button btnChallenge;

        [SerializeField] private GameObject iconBtnChallenge = null;

        [SerializeField] private Button btnCircle;

        [SerializeField] private GameObject iconBtnCircle = null;

        [SerializeField] private Button btnHome;

        [SerializeField] private GameObject iconBtnHome = null;

        [SerializeField] private Button btnGift;

        [SerializeField] private GameObject iconBtnGift = null;

        [SerializeField] private Button btnAnalytics;

        [SerializeField] private GameObject iconBtnAnalytics = null;

        [SerializeField] private Scrollbar scrollbarLoading = null;

        [SerializeField] public Button btnSetting;


        [SerializeField] private PopupLoadGame popupLoadGame;

        [SerializeField] private LevelController levelController = null;

        [SerializeField] private HomeController homeController = null;

        [SerializeField] private SettingController settingController = null;

        [SerializeField] private ChallengeController characterController = null;

        [SerializeField] private DailyRewardController dailyRewardController = null;

        [SerializeField] private ShopController shopController = null;

        [SerializeField] private StageController stageController = null;

        [SerializeField] private Image backgroundImage;

        [SerializeField] private Sprite[] backgroundGame;

        [SerializeField] private RateUsLayerCtrl rateUsLayerCtrl;

        [SerializeField] private PopupVideoAds popupVideoAds;

        [SerializeField] private PopupNotAvailable popupNotAvailable;

        [SerializeField] private Animator notiRewardAnimator;

        [Inject] RewardManager rewardmanager;

        [Inject] NotificationExample notificationExample;

        private void Awake()
        {
            this.backgroundImage.sprite = this.backgroundGame[Random.Range(0, this.backgroundGame.Length - 1)];
        }


        public void Start()
        {
            instance = this;
            this.scrollbarLoading.size = 0;
            this.gameData.LoadGameData(OnLoadContext);
            this.soundPalette.Init();
            this.soundPalette.PlayBGM();

            Debug.LogError("==> diamond " + this.playerDataService.GetPlayerData().diamond);
            this.txtDiamond.text = string.Format("{0}", this.playerDataService.GetPlayerData().diamond);
            this.notificationExample.ScheduleNormal();
        }

       
		public void OnEnable()
        {
            
            this.notiRewardAnimator.gameObject.SetActive(false);
            this.playerDataService.OnUpdateDiamond += UpdateUi;
            this.rewardmanager.OnUpdateGetNewReward += ShowOnNotiReward;
            OnResetButton();
            this.iconBtnHome.gameObject.SetActive(true);
            this.homeController.gameObject.SetActive(true);
            this.homeController.OnShow();

            CheckNotiReward();
        }

		public void OnDisable()
		{
            this.playerDataService.SaveGame(this.playerDataService.GetPlayerData());
            this.rewardmanager.OnUpdateGetNewReward -= ShowOnNotiReward;
            this.playerDataService.OnUpdateDiamond -= UpdateUi;
		}

        public void ShowNotice(string msg)
        {
            popupNotAvailable.gameObject.SetActive(true);
            popupNotAvailable.OnShow(msg);
        }

        public void UpdateUi()
        {
            Debug.LogError("==> diamond " + this.playerDataService.GetPlayerData().diamond);
            this.txtDiamond.text = string.Format("{0}", this.playerDataService.GetPlayerData().diamond);
        }


		public void OnLoadContext(GameData gameData)
        {
            OnClickHome();
        }

        public void OnPlayGame()
        {
            this.playerDataService.GetPlayerData().ItemQuestInfo.LevelQuest = 0;
            this.popupLoadGame.gameObject.SetActive(true);
            this.popupLoadGame.OnShow();
        }

        private void CheckNotiReward()
        {
            bool isNotifaction = false;
            for (int i = 0; i < this.rewardmanager.DailyRewardData.Rewards.Length; i++)
            {
                if (this.rewardmanager.DailyRewardData.Rewards[i] == 1)
                {
                    isNotifaction = true;
                    ShowOnNotiReward();
                }
            }

            if(!isNotifaction)
            {
                ShowOffNotiReward();
            }
        }

        public void ShowOnNotiReward()
        {
            this.notiRewardAnimator.gameObject.SetActive(true);
            this.notiRewardAnimator.Play("Noti");
        }

        public void ShowOffNotiReward()
        {
            this.notiRewardAnimator.gameObject.SetActive(false);
        }
        #region button click

        public void OnClickChallenge()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.iconBtnChallenge.gameObject.SetActive(true);
            this.characterController.gameObject.SetActive(true);
            this.characterController.OnShow();


        }

        public void OnClickLevel()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.levelController.gameObject.SetActive(true);
            this.levelController.OnShow();
            this.iconBtnCircle.gameObject.SetActive(true);
        }

        public void OnClickHome()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.iconBtnHome.gameObject.SetActive(true);
            this.homeController.gameObject.SetActive(true);
            this.homeController.OnShow();
        }

        public void OnClickGift()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.dailyRewardController.gameObject.SetActive(true);
            this.dailyRewardController.OnShow();
            this.iconBtnGift.gameObject.SetActive(true);
        }

        public void OnClickRateUs()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.rateUsLayerCtrl.gameObject.SetActive(true);
            this.iconBtnAnalytics.gameObject.SetActive(true);
        }

        public void OnClickVideoAds()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.popupVideoAds.gameObject.SetActive(true);
        }

        public void OnClickAnalytics()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            this.iconBtnAnalytics.gameObject.SetActive(true);
        }

        public void OnClickSetting()
        {
            this.soundPalette.PlaySoundEffect(SoundPalette.SOUND_EFFECT.CLICK);
            OnResetButton();
            if(this.settingController.gameObject.activeInHierarchy)
            {
                this.settingController.gameObject.SetActive(false);
                OnClickHome();
            }
            else{
               
                this.settingController.gameObject.SetActive(true);
                this.settingController.OnShow();
            }
           
        }

        public void OnClickStore()
        {
            OnClickVideoAds();
        }

        public void OnClickStage()
        {
            OnResetButton();
            this.stageController.gameObject.SetActive(true);
            this.stageController.OnShow();
        }

        public void OnResetButton()
        {
            this.characterController.gameObject.SetActive(false);
            this.homeController.gameObject.SetActive(false);
            this.levelController.gameObject.SetActive(false);
            this.iconBtnChallenge.gameObject.SetActive(false);
            this.iconBtnCircle.gameObject.SetActive(false);
            this.iconBtnHome.gameObject.SetActive(false);
            this.iconBtnGift.gameObject.SetActive(false);
            this.iconBtnAnalytics.gameObject.SetActive(false);
            this.dailyRewardController.gameObject.SetActive(false);
            this.shopController.gameObject.SetActive(false);
            this.stageController.gameObject.SetActive(false);
            this.settingController.gameObject.SetActive(false);
        }
        #endregion
    }
}

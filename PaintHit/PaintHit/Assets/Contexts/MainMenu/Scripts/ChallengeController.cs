﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;
using Assets.Scripts.Core;
using Assets.Contexts.BaseScript;
using System;

namespace Assets.Contexts.MainMenu.Scripts
{
    public class ChallengeController : MonoBehaviour
    {
        public static ChallengeController instance;

        public  static ChallengeController Instance
        {
            get
            {
                if(instance == null)
                {
                    instance = new ChallengeController();
                }
                return instance;
            }
        }

        private const int NumberQuest = 5;

        [SerializeField] Text timeCountDown;

        bool _countTime;
        long _nextDateTime;

        [Inject] GameHub gameHub;

        [Inject] PlayerDataService playerDataService;

        [SerializeField] private Transform parentQuest;

        [SerializeField] private QuestController questController;

       
        public void OnShow()
        {
            instance = this;

            this.playerDataService.GetPlayerData().challengeList.Clear();

            //if (!this.playerDataService.GetPlayerData().isChallengeReward)
            //{
                GenerationChallange();

                ItemQuestInfo levelQuest = this.playerDataService.GetPlayerData().ItemQuestInfo;

                if (levelQuest.Status == 1)
                {
                    this.playerDataService.GetPlayerData().ListQuest[levelQuest.LevelQuest].GetComponent<QuestController>().UpdateChallengeDoing();

                }
            //}

            //if(this.playerDataService.GetPlayerData().isChallengeReward && this.playerDataService.GetPlayerData().TimeActiveChallenge > 0)
            //{
                CheckTimeCoutDown(); 
            //}
          

        }


        public void OnHide()
        {

        }

        private Tween coutingTween = null;

        private void GenerationChallange()
        {
            this.playerDataService.GetPlayerData().ListQuest.Clear();
            for (var i = 0; i < 5; i++)
            {
                
                int level = UnityEngine.Random.Range(100, 300);
                int diamond = 10 * (i + 1);
                GameObject itemQuest = GameObject.Instantiate(this.questController.gameObject, this.parentQuest);
                itemQuest.GetComponent<QuestController>().OnShow(this.gameHub, this.playerDataService, level, diamond);

                if (!this.playerDataService.GetPlayerData().challengeList.ContainsKey(level))
                {
                    ChallengeData challengeData = new ChallengeData();

                    challengeData.currentChallengeId = level;
                    challengeData.currentChallengeValue = diamond;
                    challengeData.currentProcessChallengeCount = 0;
                    challengeData.currentTargetValue = 0;

                    this.playerDataService.GetPlayerData().challengeList.Add(level, challengeData);
                }
                this.playerDataService.GetPlayerData().ListQuest.Add(itemQuest);
            }
            this.playerDataService.GetPlayerData().TimeBeginReward = DateTime.Now;
            this.playerDataService.GetPlayerData().isChallengeReward = true;
            this.playerDataService.GetPlayerData().TimeActiveChallenge = 7;
        }
        public void CheckTimeCoutDown()
        {
            if (isCheckTime())
            {
                this.playerDataService.GetPlayerData().isChallengeReward = false;
                this.playerDataService.GetPlayerData().TimeActiveChallenge = 0;
            }
            //start counting down
            if (this.coutingTween != null)
            {
                this.coutingTween.Kill();
            }

            TimeSpan diff = TimeSpan.FromHours((this.playerDataService.GetPlayerData().TimeActiveChallenge)) - GetTimeFromLastSave();
            double seconds = diff.TotalSeconds;
            this.coutingTween = DOVirtual.Float((float)seconds, 0.0f, (float)seconds, f =>
            {
                TimeSpan timeSpan = TimeSpan.FromSeconds((double)f);

                this.timeCountDown.text = "REFRESH IN "+ timeSpan.Hours.ToString() +":"+ timeSpan.Minutes.ToString()+":"+timeSpan.Seconds.ToString();

            }).SetEase(Ease.Linear).OnComplete(() => {
                this.playerDataService.GetPlayerData().isChallengeReward = false;
            }).Play();
        }

        #region time checker


        bool isCheckTime()
        {
            if (IsHaveReward())
            {
                return true;
            }
            return false;
        }


        public TimeSpan GetTimeFromLastSave()
        {
            DateTime advancedTime = DateTime.Now;

            TimeSpan diff = advancedTime - this.playerDataService.GetPlayerData().TimeBeginReward;
            return diff;
        }


        public bool IsHaveReward()
        {
            TimeSpan diff = GetTimeFromLastSave();
            double hour = (double)Math.Abs(diff.TotalHours / this.playerDataService.GetPlayerData().TimeActiveChallenge);

            if (hour >= this.playerDataService.GetPlayerData().TimeActiveChallenge)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Zenject;

namespace Assets.Contexts.BaseScript
{
    [Serializable]
    public class GameData : MonoBehaviour
    {
        public MainGameData MainGameData = new MainGameData();

        private string gameDataProjectFilePath = "/StreamingAssets/configff002.txt";

        private string gameDataProjectFilePathAndroid = "https://d3exmuvldua4bg.cloudfront.net/config/configff002.txt";

        private const string Version = "0.0.0";

        private const string KEY_VERSION = "version";
 

        public void LoadGameData(Action<GameData> callBack = null)
        {
            MainGameData = InitDataFake();
            //string filePath = Application.dataPath + gameDataProjectFilePath;
            //if (File.Exists(filePath))
            //{
            //    string dataAsJson = File.ReadAllText(filePath);
            //    //var mainGameData = JsonUtility.FromJson<MainGameData>(dataAsJson);
            //    MainGameData = InitDataFake();


            //}

            if (callBack != null)
            {
                callBack(this);
            }
            
        }

        private MainGameData InitDataFake()
        {
            this.MainGameData = new MainGameData();
            this.MainGameData.levelBoss = 10;
            this.MainGameData.diamondVideoAds = 200;
            this.MainGameData.ListLevels = new LevelInfor[300];
            this.MainGameData.ListColors = new ColorInfo[10];
            this.MainGameData.ListCircle = new CircleInfo[10];

            for (var i = 0; i < this.MainGameData.ListLevels.Length; i++)
            {
                this.MainGameData.ListLevels[i] = new LevelInfor();
                this.MainGameData.ListLevels[i].Id = i;
                this.MainGameData.ListLevels[i].Level = 5;
                this.MainGameData.ListLevels[i].ItemInfors = new ItemInfor[5];
                for (var j = 0; j < this.MainGameData.ListLevels[i].ItemInfors.Length; j++)
                {
                    this.MainGameData.ListLevels[i].ItemInfors[j] = new ItemInfor();
                  
                    this.MainGameData.ListLevels[i].ItemInfors[j].ColorCircle = HexToColor("FFFFFF");
                    this.MainGameData.ListLevels[i].ItemInfors[j].IdCircle = j;
                    this.MainGameData.ListLevels[i].ItemInfors[j].IsBoDiamondnus = 3;
                    this.MainGameData.ListLevels[i].ItemInfors[j].IsComplete = false;
                    this.MainGameData.ListLevels[i].ItemInfors[j].EnemLevelBullety = 5;
                    this.MainGameData.ListLevels[i].ItemInfors[j].Rotation = 30;
                    this.MainGameData.ListLevels[i].ItemInfors[j].Speed = 1;
                }
            }
            this.MainGameData.level = 0;
            return this.MainGameData;
        }
        public void SaveGameData(GameData gameData)
        {

            string dataAsJson = JsonUtility.ToJson(gameData.MainGameData);

            string finalURL = gameDataProjectFilePathAndroid;
            string fullPath = "";
            string fileCache = "";
#if UNITY_EDITOR
            fullPath = Application.dataPath + gameDataProjectFilePath;
#elif UNITY_ANDROID
            //fileCache = "/Data/" + System.IO.Path.GetFileName(finalURL);
            //fullPath = FileUtilities.GetWritablePath(fileCache);

            //if (FileUtilities.IsFileExist(fileCache))
            //{
            //    finalURL = "file://" + fullPath;
            //}
#endif
            File.WriteAllText(fullPath, dataAsJson);
        }

        public static Color HexToColor(string hex)
        {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if (hex.Length == 8)
            {
                a = byte.Parse(hex.Substring(6, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }

    }

    [Serializable]
    public class MainGameData
    {
        public int modeGame;

        public int ModeGame
        {
            get { return modeGame; }
            set { modeGame = value; }
        }

        public bool isPlay;

        public bool IsPlay
        {
            get { return isPlay; }
            set { isPlay = value; }
        }

        public int live;
        public int Live
        {
            get { return live; }
            set { live = value; }
        }

        public int liveDefaul;
        public int LiveDefaul
        {
            get { return liveDefaul; }
            set { liveDefaul = value; }
        }

        public float score;
        public float Score
        {
            get { return score; }
            set { score = value; }
        }

        public int currentLevel;

        public int CurrentLevel
        {
            get { return currentLevel; }
            set { currentLevel = value; }
        }

        public int level;

        public int Level
        {
            get { return level; }
            set { level = value; }
        }

        public int stage;

        public int Stage
        {
            get { return stage; }
            set { stage = value; }
        }
        

        public bool isSound;

        public bool IsSound
        {
            get { return isSound; }
            set { isSound = value; }
        }

        public float timeAdsContinue;

        public float TimeAdsContinue
        {
            get { return timeAdsContinue; }
            set { timeAdsContinue = value; }
        }

  
        public string version;
        public string Version
        {
            get { return version; }
            set { version = value; }
        }

        public int diamondVideoAds;

        public int DiamondVideoAds
        {
            get { return diamondVideoAds; }
            set { diamondVideoAds = value; }
        }

        public int levelBoss;

        public int LevelBoss
        {
            get { return levelBoss; }
            set { levelBoss = value; }
        }

        public LevelInfor[] ListLevels;


        public ColorInfo[] ListColors;

        public CircleInfo[] ListCircle;
    }

    [Serializable]
    public class LevelInfor
    {
        public int Id;
        public int Level;
        public ItemInfor[] ItemInfors;

    }


    [Serializable]
    public class ColorInfo
    {
        public int Id;
        public int Level;

    }


    [Serializable]
    public class CircleInfo
    {
        public int Id;
        public int Level;

    }

    [SerializeField]
    public class ItemInfor
    {
        public int IdCircle;
        public Color ColorCircle;
        //public Color ColorBullet;
        public int EnemLevelBullety;
        public int IsBoDiamondnus;
        public float Speed;
        public float Rotation;
        public bool IsComplete;
    }




    public class InfoCircleDiction
    {
        public bool IsComplete;
        public GameObject GameObject;
    }



#if UNITY_EDITOR
    public class GameDataEditor : EditorWindow
    {


        [Inject] private GameData gameData;

        private string gameDataProjectFilePath = "/StreamingAssets/data.json";

        [MenuItem("Manifest/Game Data Editor")]
        static void Init()
        {
            EditorWindow.GetWindow(typeof(GameDataEditor)).Show();
        }

        void OnGUI()
        {
            if (gameData != null)
            {
                SerializedObject serializedObject = new SerializedObject(this);
                SerializedProperty serializedProperty = serializedObject.FindProperty("gameData");
                EditorGUILayout.PropertyField(serializedProperty, true);

                serializedObject.ApplyModifiedProperties();

                if (GUILayout.Button("Save data"))
                {
                    SaveGameData(gameData);
                }
            }

            if (GUILayout.Button("Load data"))
            {
                LoadGameData(LoadComplete);
            }
        }

        public void LoadGameData(Action<GameData> callBack = null)
        {
            string filePath = Application.dataPath + gameDataProjectFilePath;

            if (File.Exists(filePath))
            {
            }
        }

        public void SaveGameData(GameData gameData)
        {
            string dataAsJson = JsonUtility.ToJson(gameData.MainGameData);

            string filePath = Application.dataPath + gameDataProjectFilePath;
            File.WriteAllText(filePath, dataAsJson);

        }

        private void LoadComplete(GameData gameData)
        {

        }
    }
#endif
}






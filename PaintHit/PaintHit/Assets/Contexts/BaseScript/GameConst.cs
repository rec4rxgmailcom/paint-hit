﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Contexts.BaseScript
{
    public class GameConst : MonoBehaviour
    {
        public static string PREFER_BQ_TIME_PLAY = "PREFER_BQ_TIME_PLAY";
        public static string PREFER_BQ_TOTAL_TIME = "PREFER_BQ_TOTAL_TIME";
        public static string PREFER_BQ_REFER_SOURCE = "PREFER_BQ_REFER_SOURCE";
        public static string PREFER_BQ_REFER_CAMPAIGN = "PREFER_BQ_REFER_CAMPAIGN";
        public static string PREFER_BQ_INCREMENTAL_ADS = "PREFER_BQ_INCREMENTAL_ADS";
        public static string PREFER_BQ_FIRST_TIME_FIREBASE = "PREFER_BQ_FIRST_TIME_FIREBASE";
        public static string PREFER_BQ_INCREMENTAL_SESSION = "PREFER_BQ_INCREMENTAL_SESSION";
        public static string PREFER_BQ_INCREMENTAL_REWARDS = "PREFER_BQ_INCREMENTAL_REWARDS";
        public static string PREFER_BQ_INCREMENTAL_SONG_END = "PREFER_BQ_INCREMENTAL_SONG_END";
        public static string PREFER_BQ_INCREMENTAL_SONG_START = "PREFER_BQ_INCREMENTAL_SONG_START";

       
    }
}

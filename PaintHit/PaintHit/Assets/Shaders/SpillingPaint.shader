﻿Shader "Custom/SpillingPaint_2"
{
	Properties
	{
		_MainColor("Main Color", Color) = (1, 1, 1, 1)
		_FirstTex ("Texture", 2D) = "white" {}
		_SecondColor("Second Color", Color) = (0.5, 0.5, 0.5, 0.5)
		_SecondTex ("Second Texture", 2D) = "white" {}

		_AlphaCutoff ("Alpha Cutoff", Range(0, 1)) = 0.5
		_Smoothness ("Smoothness", Range(0, 1)) = 0.5
		_Metallic ("Metallic", Range(0, 1)) = 0
		_Diffuse ("Diffuse", Range(0, 1)) = 0.5
		_Specular ("Specular", Range(0, 1)) = 0
	}
	SubShader
	{
		Tags { 
			"Queue"="Transparent" 
			"RenderType"="Transparent" 
			"IgnoreProjector"="True" 
			"LightMode" = "ForwardBase"
		}

		Pass
		{
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// #pragma shader_feature _RENDERING_CUTOUT 
			

			// #include "UnityCG.cginc"
			// #include "UnityStandardBRDF.cginc"
			#include "UnityPBSLighting.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float2 uv2 : TEXCOORD;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float3 normal : TEXCOORD2;
				float3 worldPos : TEXCOORD3;
				float4 vertex : SV_POSITION;
			};

			sampler2D _FirstTex;
			sampler2D _SecondTex;
			float4 _FirstTex_ST;
			float4 _SecondTex_ST;
			half4 _MainColor;
			half4 _SecondColor;
			half _AlphaCutoff;
			half _Diffuse;
			half _Specular;


			half _Smoothness;
			half _Metallic;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _FirstTex);
				o.uv2 = TRANSFORM_TEX(v.uv2, _SecondTex);
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
				o.normal = UnityObjectToWorldNormal(v.normal);
				return o;
			}
			
			fixed4 frag (v2f i) : COLOR
			{
				// sample the texture
				// float alpha = GetAlpha(i);
				// clip(alpha - _AlphaCutoff);


				fixed4 col = (tex2D(_FirstTex, i.uv) * _MainColor);
				fixed4 color2 = (tex2D(_SecondTex, i.uv2)) * _SecondColor;
				half alpha = 1 - color2.a;
				alpha = floor(_AlphaCutoff - alpha + 1);
				// fixed4 finalColor = fixed4(((col * (1 - alpha)) + (color2 * alpha)).rgb, col.a);
				fixed3 finalColor2 = fixed3(((col * (1 - alpha)) + (color2 * alpha)).rgb);

				// fixed3 lightDir = _WorldSpaceLightPos0.xyz;
				// fixed3 lightColor = _LightColor0.rgb;
				// i.normal = abs(normalize(i.normal));
				// fixed3 diffuse = finalColor * lightColor * abs(dot(lightDir, i.normal));
				// return fixed4(diffuse, 1);


				i.normal = normalize(i.normal);
				float3 lightDir = _WorldSpaceLightPos0.xyz;
				float3 viewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
				

				float3 lightColor = _LightColor0.rgb;
				// float3 albedo = tex2D(_MainTex, i.uv).rgb * _Tint.rgb;

				float3 specularTint;
				float oneMinusReflectivity;
				finalColor2 = DiffuseAndSpecularFromMetallic(
					finalColor2, _Metallic, specularTint, oneMinusReflectivity
				);
				
				UnityLight light;
				light.color = lightColor;
				light.dir = lightDir;
				light.ndotl = DotClamped(i.normal, lightDir);
				UnityIndirect indirectLight;
				indirectLight.diffuse = _Diffuse;
				indirectLight.specular = _Specular;

				float4 lightingSrc = UNITY_BRDF_PBS(
					finalColor2, specularTint,
					oneMinusReflectivity, _Smoothness,
					i.normal, viewDir,
					light, indirectLight
				);

				return lightingSrc;

				// If you need to use an effect like unlock item, so that you can use the line code below
				// return fixed4(((col * alpha) + (color2 * (1 - alpha))).rgb, col.a);
			}
			ENDCG
		}
	}
}
﻿Shader "Unlit/CombineTex"
{
	Properties
	{
		_MainColor("Main Color", Color) = (1, 1, 1, 1)
		_FirstTex ("Texture", 2D) = "white" {}
		_SecondColor("Second Color", Color) = (0.5, 0.5, 0.5, 0.5)
		_SecondTex ("Second Texture", 2D) = "white" {}

		_AlphaCutoff ("Alpha Cutoff", Range(0, 1)) = 0.5
	}
	SubShader
	{
		Tags { 
			"RenderType"="Transparent" 
			"Queue" = "Transparent"
			"IgnoreProjector"="True"
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// #pragma shader_feature _RENDERING_CUTOUT 

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			sampler2D _FirstTex;
			sampler2D _SecondTex;
			float4 _FirstTex_ST;
			float4 _SecondTex_ST;
			half4 _MainColor;
			half4 _SecondColor;
			half _AlphaCutoff;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _FirstTex);
				// o.uv2 = TRANSFORM_TEX(v.uv, _SecondTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				// float alpha = GetAlpha(i);
				// clip(alpha - _AlphaCutoff);
				fixed4 col = (tex2D(_FirstTex, i.uv) * _MainColor);
				// fixed4 color2 = (tex2D(_SecondTex, i.uv2)) * _SecondColor;
				// half alpha = _AlphaCutoff * color2.a;
				// // clip(alpha - _AlphaCutoff);
				// return fixed4((col * (1 - alpha) + (color2 * alpha)).rgb, col.a);

				// half alpha = color2.a;



				// half alpha = (tex2D(_SecondTex, i.uv2) ;
				// clip(alpha - _AlphaCutoff);
				// if(_AlphaCutoff > 0.05)
				// {
				// 	col.rgb *= color2.rgb;
				// }
				// 
				// 
				// col.a *= alpha;
				
				return col;
			}
			ENDCG
		}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// #pragma shader_feature _RENDERING_CUTOUT 

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
				float4 vertex : SV_POSITION;
			};

			sampler2D _FirstTex;
			sampler2D _SecondTex;
			float4 _FirstTex_ST;
			float4 _SecondTex_ST;
			half4 _MainColor;
			half4 _SecondColor;
			half _AlphaCutoff;
			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				// o.uv = TRANSFORM_TEX(v.uv, _FirstTex);
				o.uv2 = TRANSFORM_TEX(v.uv, _SecondTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 col2 = tex2D(_SecondTex, i.uv2) * _SecondColor;
				half alpha = col2.a;
				clip(alpha - _AlphaCutoff);
				
				return col2;
			}
			ENDCG
		}
	}
}
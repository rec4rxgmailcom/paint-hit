{
  "modeGame": 1,
  "isPlay": false,
  "live": 0,
  "liveDefaul": 1,
  "score": 0,
  "currentLevel": 0,
  "level": 0,
  "stage": 0,
  "isSound": false,
  "timeAdsContinue": 15,
  "version": "1.0.0",
  "ListLevels": [
    {
      "Id": 0,
      "Level": 0,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 1,
      "Level": 1,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 2,
      "Level": 2,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 3,
      "Level": 3,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 4,
      "Level": 4,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 5,
      "Level": 5,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 6,
      "Level": 6,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 7,
      "Level": 7,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 8,
      "Level": 8,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    },
    {
      "Id": 9,
      "Level": 9,
      "ItemInfors": [
        {
          "IdCircle": 0,
          "Color": 0,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 1,
          "Color": 1,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 2,
          "Color": 2,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 3,
          "Color": 3,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        },
        {
          "IdCircle": 4,
          "Color": 4,
          "EnemLevelBullety": 5,
          "IsBoDiamondnus": 4,
          "Speed": 2,
          "Rotation": 2,
          "IsComplete": false
        }
      ]
    }
  ]
}